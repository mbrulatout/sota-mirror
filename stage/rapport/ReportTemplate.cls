\NeedsTeXFormat{LaTeX2e}[1996/12/01]
\ProvidesClass{ReportTemplate}
\def\baseclass{book}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{\baseclass}}

\def\@checkoptions#1#2{
  \edef\@curroptions{\@ptionlist{\@currname.\@currext}}
  \@tempswafalse
  \@tfor\@this:=#2\do{
    \@expandtwoargs\in@{,\@this,}{,\@curroptions,}
    \ifin@ \@tempswatrue \@break@tfor 
    \fi
    }
  \let\@this\@empty
  \if@tempswa \else \PassOptionsToClass{#1}{\baseclass}
  \fi
}

\@checkoptions{11pt}{{10pt}{11pt}{12pt}}
\PassOptionsToClass{a4paper}{\baseclass}
\ProcessOptions\relax

\LoadClass{\baseclass}

%----------------------------------------
%	PACKAGES
%----------------------------------------

\usepackage[utf8]{inputenc}
\usepackage{setspace}
\usepackage{vmargin}
\usepackage{fancyhdr}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{xspace}
\usepackage{float}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{booktabs}
\usepackage{geometry}
\usepackage{pdflscape}
%\usepackage{rotating}
\usepackage[final]{pdfpages}
%\usepackage{listings}
%\usepackage{lstpatch}
\usepackage[centerlast,small,sc,
  justification=centering]{caption}
%\usepackage{subcaption}
\usepackage[pdfpagemode={UseOutlines},
  bookmarks=true,bookmarksopen=true,
  bookmarksopenlevel=0,bookmarksnumbered=true,
  hypertexnames=false,colorlinks,linkcolor={blue},
  citecolor={red},urlcolor={blue},pdfstartview={FitV},
  unicode,breaklinks=true]{hyperref}
        
\pdfstringdefDisableCommands{
   \let\\\space
}
\def\cleardoublepage{\clearpage\if@twoside \ifodd\c@page\else
\hbox{}
\thispagestyle{empty}
\newpage
\if@twocolumn\hbox{}\newpage\fi\fi\fi}



%----------------------------------------
%	PAGE STYLE
%----------------------------------------
\pagestyle{fancy}
\lhead[\rm\thepage]{\fancyplain{}{\sl{\rightmark}}}
\chead{}
\rhead[\fancyplain{}{\sl{\leftmark}}]{\rm\thepage}
\rfoot{Mathias Brulatout}
\cfoot{}
\lfoot{Amélioration du transport dans les réseaux ad hoc}

\setmarginsrb  { 1.5in}  % left margin
               { 0.6in}  % top margin
               { 1.0in}  % right margin
               { 0.8in}  % bottom margin
               {  20pt}  % head height
               {0.25in}  % head sep
               {   9pt}  % foot height
               { 0.3in}  % foot sep
%----------------------------------------

%% \usepackage{silence}
%% \WarningFilter[test]{pdftex}{pdflatex}
%% \WarningFilter[test]{frenchb.ldf}{0T1 encoding}
%% \ActivateWarningFilters[test]


%pdftex 1.40.15 only
% Removes a VERY annoying pdftex warning
%\pdfsuppresswarningpagegroup=1

\raggedbottom
\setlength{\topskip}{1\topskip \@plus 5\p@}
\doublehyphendemerits=10000       % No consecutive line hyphens.
\brokenpenalty=10000              % No broken words across columns/pages.
\widowpenalty=9999                % Almost no widows at bottom of page.
\clubpenalty=9999                 % Almost no orphans at top of page.
\interfootnotelinepenalty=9999    % Almost never break footnotes.

\newcommand\bhrule{\typeout{------------------------------------------------------------------------------}}
\newcommand\btypeout[1]{\bhrule\typeout{\space #1}\bhrule}
\onehalfspacing
\setlength{\parindent}{0pt}
\setlength{\parskip}{2.0ex plus0.5ex minus0.2ex}

\newcommand{\lfn}{Liste des Figures et Algorithmes}

\renewcommand{\chaptermark}[1]{\btypeout{\thechapter\space #1}\markboth{\@chapapp\ \thechapter\ #1}{\@chapapp\ \thechapter\ #1}}
\renewcommand{\sectionmark}[1]{}
\renewcommand{\subsectionmark}[1]{}

\setlength{\captionmargin}{20pt}
\newcommand{\fref}[1]{Figure~\ref{#1}}
\newcommand{\tref}[1]{Tableau~\ref{#1}}
\newcommand{\eref}[1]{\'Equation~\ref{#1}}
\newcommand{\cref}[1]{Chapitre~\ref{#1}}
\newcommand{\sref}[1]{Section~\ref{#1}}
\newcommand{\aref}[1]{Annexe~\ref{#1}}
\renewcommand{\topfraction}{0.85}
\renewcommand{\bottomfraction}{.85}
\renewcommand{\textfraction}{0.1}
\renewcommand{\dbltopfraction}{.85}
\renewcommand{\floatpagefraction}{0.75}
\renewcommand{\dblfloatpagefraction}{.75}
\setcounter{topnumber}{9}
\setcounter{bottomnumber}{9}
\setcounter{totalnumber}{20}
\setcounter{dbltopnumber}{9}

%----------------------------------------
%	VARIABLES
%----------------------------------------
\newcommand*{\supervisor}[1]{\def\supname{#1}}
\newcommand*{\thesistitle}[1]{\def\ttitle{#1}}
\newcommand*{\degree}[1]{\def\degreename{#1}}
\newcommand*{\authors}[1]{\def\authornames{#1}}
\newcommand*{\university}[1]{\def\univname{#1}}
\newcommand*{\department}[1]{\def\deptname{#1}}
\newcommand*{\faculty}[1]{\def\facname{#1}}

\thesistitle{Amélioration du transport dans les réseaux ad hoc} 
\supervisor{Jérémie \textsc{Leguay}}
\degree{Ingénieur informatique}
\authors{Mathias \textsc{Brulatout}}
\university{\texorpdfstring{\href{http://www.enseirb-matmeca.fr/}{ENSEIRB-MATMECA}}{ENSEIRB-MATMECA}}
\department{\texorpdfstring{\href{http://www.enseirb-matmeca.fr/accueil/formations/ingenieur-en-informatique}{Département Informatique}}{Département Informatique}}

%----------------------------------------
%	ABSTRACT PAGE DESIGN
%----------------------------------------
\newenvironment{abstract}
{
  \btypeout{Abstract Page}
  \thispagestyle{empty}
  \null\vfil
  \begin{center}
    \setlength{\parskip}{0pt}
    \bigskip
    {\huge{\textit{Abstract}} \par}
    \bigskip
    {\normalsize \univname \par}
    {\normalsize \deptname \par}
    \bigskip
    {\normalsize Pour l'obtention du diplôme d'\degreename\par}
    \bigskip
    {\normalsize\bf \@title \par} 
    \medskip
    {\normalsize par \authornames \par}
    \bigskip
  \end{center}
}
%----------------------------------------
{
  \vfil\vfil\vfil\null
  \cleardoublepage
}
\addtocounter{secnumdepth}{1}
\setcounter{tocdepth}{6}
\newcounter{dummy}
\newcommand\addtotoc[1]{
\refstepcounter{dummy}
\addcontentsline{toc}{chapter}{#1}}
%----------------------------------------
\renewcommand\tableofcontents{
\btypeout{Table des Mati\`eres}
\addtotoc{Table des Mati\`eres}
\begin{spacing}{1}{
    \setlength{\parskip}{1pt}
    \if@twocolumn
      \@restonecoltrue\onecolumn
    \else
      \@restonecolfalse
    \fi
    \chapter*{\contentsname
        \@mkboth{
           \MakeUppercase\contentsname}{\MakeUppercase\contentsname}}
    \@starttoc{toc}
    \if@restonecol\twocolumn\fi
   \cleardoublepage
}\end{spacing}
}
%----------------------------------------

\renewcommand\listoffigures{
\btypeout{Liste des Figures et Algorithmes}
\addtotoc{Liste des Figures et Algorithmes}
\begin{spacing}{1}{
    \setlength{\parskip}{1pt}
    \if@twocolumn
      \@restonecoltrue\onecolumn
    \else
      \@restonecolfalse
    \fi
    \chapter*{\lfn
      \@mkboth{\MakeUppercase\lfn}
              {\MakeUppercase\lfn}}
    \@starttoc{lof}
    \if@restonecol\twocolumn\fi
    \cleardoublepage
}\end{spacing}
}
%----------------------------------------
\renewcommand\listoftables{
\btypeout{Liste des Tableaux}
\addtotoc{Liste des Tableaux}
\begin{spacing}{1}{
    \setlength{\parskip}{1pt}
    \if@twocolumn
      \@restonecoltrue\onecolumn
    \else
      \@restonecolfalse
    \fi
    
    \chapter*{\listtablename
      \@mkboth{
        \MakeUppercase\listtablename abcde}{\MakeUppercase\listtablename abcde}}
    \@starttoc{lot}
    \if@restonecol\twocolumn\fi
    \cleardoublepage
}\end{spacing}
}
%----------------------------------------
\newcommand\listsymbolname{Abr\'eviations}
\usepackage{longtable}
\newcommand\listofsymbols[2]{
\btypeout{\listsymbolname}
\addtotoc{\listsymbolname}
    \chapter*{\listsymbolname
      \@mkboth{
          \MakeUppercase\listsymbolname}{\MakeUppercase\listsymbolname}}
\begin{longtable}[c]{#1}#2\end{longtable}\par
    \cleardoublepage
}
%----------------------------------------
\newcommand\remerciements[1]{
\btypeout{Remerciements}
\addtotoc{Remerciements}
\thispagestyle{plain}
\begin{center}{\huge{\textit{Remerciements}} \par}\end{center}
{\normalsize #1}
\vfil\vfil\null
}
%----------------------------------------
\renewcommand\backmatter{
  \if@openright
    \cleardoublepage
  \else
    \clearpage
  \fi
  \addtotoc{\bibname}
  \btypeout{\bibname}
  \@mainmatterfalse}
\endinput
%----------------------------------------
