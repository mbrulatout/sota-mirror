\chapter{R\'esultats} % Main chapter title

\label{chapter4} 
Ce chapitre présente les nombreux résultats obtenus et les différentes observations qui ont été faite grâce à ces derniers. Le but est de mettre en valeur un cas d'utilisation où les mécanismes de \REST~ont un intérêt réel pour une ou plusieurs applications.
 
Le \tref{table:notations} regroupe toutes les notations utilisées par la suite dans la description des résultats. Le \tref{table:parameters} présente les paramètres fixés lors des différentes simulations dont les résultats sont présentés dans ce chapitre.
\begin{table}
\renewcommand{\arraystretch}{1.3}
\centering
%\footnotesize
\begin{tabular}{|c||p{10cm}|}
\hline
\bfseries Paramètre & \bfseries Signification\\
\hline
$Px$ & Paquet de donnée en clair d'indice $x$.\\
\hline
$R(x,y)$ & Paquet de redondance codant de $Px$ à $Py$.\\
\hline
$R$ & Ratio de redondance inline de l'émetteur\\
\hline
$p$ & Packet Error Rate (PER) du lien.\\
\hline
$k$ & Nombre de paquets de données en clair entre deux paquets de redondance.\\
\hline
$W$ & Taille maximale de la fenêtre d'encodage de l'émetteur.\\
\hline
$f_{ack}$ & Fréquence d'acquittement du récepteur.\\
\hline
$\alpha$ & Coefficient de lissage de l'EWMA du récepteur.\\
\hline
$W_{EWMA}$ & Taille de la fenêtre de calcul de l'EWMA du récepteur.\\
\hline
\end{tabular}
\caption{Notations}
\label{table:notations}
\end{table}

\begin{table}[!t]
\renewcommand{\arraystretch}{1.3}
\centering
%\footnotesize
\begin{tabular}{|c||c|}
\hline
\bfseries Paramètre & \bfseries Valeur\\
\hline
Débit du lien & 10Mbps\\
\hline
RTT & 300ms\\
\hline
Taille du payload \REST & 450bytes\\
\hline
Fréquence du CBR & 0.008s\\
\hline
Fréquence d'ACK & 0.2s\\
\hline
$\alpha$ & 0.8\\
\hline
$W_{EWMA}$ & 3s\\
\hline
\end{tabular}
\caption{Paramètres des simulations}
\label{table:parameters}
\end{table}

\section{Fiabilité totale}
\label{sec:res1}
Puisque cette version de \REST~demande une redondance fixée, dès le départ, des simulations ont été faites avec des taux d'erreur et de redondance différents passés en paramètre de chaque simulation. Pour obtenir des statistiques avec une certaine justesse, 10 runs sont effectués par paire de taux d'erreur et de redondance.

Trois cas de figure se présentent avec un taux de pertes uniforme (voir \fref{fig:timedecoding}): 
\begin{description}
\item[ 1 - Pas assez de redondance inline :] Le décodage ne peut pas avoir lieu en plein milieu du flux, puisque trop peu de paquet de redondance ont été reçu. Il faut donc attendre la fin pour récupérer tous les paquets lors d'un unique et conséquent décodage (voir \fref{subfig:rest1timedecoding1}).
\item[ 2 - Juste assez de redondance inline :] Le débit applicatif est fluide. Les paquets perdus sont souvent récupérés très vite via des décodages de petite taille (voir \fref{subfig:rest1timedecoding2}).
\item[ 3 - Trop de redondance inline :] Le débit applicatif est totalement fluide. Tous les paquets sont récupérés très vite (décodage de matrices de taille 1) mais le flux dure plus longtemps à cause de paquets de redondance inutile envoyés. (voir \fref{subfig:rest1timedecoding3}).
\end{description}

Le cas 2 est très certainement le meilleur cas possible lors des bons runs (voir \fref{fig:rest1timevsseqno} pour certains runs qui se passent moins bien comme le 9 par exemple). Cependant, il est nécessaire de connaître le taux de pertes dès le départ (qui de plus doit être uniforme tout au long du transfert) pour pouvoir fixer le taux de redondance inline. Ces conditions ne reflètent pas du tout un cas pratique, où les pertes sont souvent sous forme de bursts.
\begin{figure}
\centering
\subfigure[10\% de redondance inline]{\label{subfig:rest1timedecoding1}\includegraphics[width=0.49\textwidth]{Figures/Results/REST1/timedecoding1.pdf}}
\subfigure[20\% de redondance inline]{\label{subfig:rest1timedecoding2}\includegraphics[width=0.49\textwidth]{Figures/Results/REST1/timedecoding2.pdf}}
\\
\subfigure[30\% de redondance inline]{\label{subfig:rest1timedecoding3}\includegraphics[width=0.5\textwidth]{Figures/Results/REST1/timedecoding3.pdf}}
\rule{35em}{0.5pt}
\caption{\label{fig:timedecoding}Numéros de séquence acquittés (vert) et taille des matrices décodées (rouge) pour 15\% d'erreur et 10\subref{subfig:rest1timedecoding1}, 20\subref{subfig:rest1timedecoding2} et 30\%\subref{subfig:rest1timedecoding3} de redondance inline}
\end{figure}

\begin{figure} 
  \centering
  \includegraphics[width=0.6\textwidth]{Figures/Results/REST1/timevsseqno.pdf}
  \rule{35em}{0.5pt}
  \caption{Numéros de séquences acquittés pour les différents runs avec 15\% d'erreur et 20\% de redondance inline}
  \label{fig:rest1timevsseqno}
\end{figure}


La \fref{fig:rest1packetdecoding} représente pour chaque taux d'erreur en fonction du taux de redondance inline le nombre moyen d'opérations de décodage effectuées(voir \fref{subfig:rest1decoding}) et le nombre de paquets reçus(voir \fref{subfig:rest1packet}) par le récepteur.

On définit le nombre d'opérations de décodage effectuées comme le carré de la taille de la matrice décodée. Ainsi, pour une matrice de taille $3 \times 3$, 9 opérations sont nécessaires. L'idée est d'avoir une croissance exponentielle du nombre d'opération lorsque la taille de la matrice augmente linéairement. 

La \fref{subfig:rest1decoding} montre qu'envoyer une redondance supérieur au taux d'erreur fait grandement chuter le nombre d'opérations (l'échelle de l'axe vertical est logarithmique) quel que soit le taux d'erreur. Par contre, une trop grande différence entre la redondance inline et le taux de perte augmente le nombre de paquets reçus (voir \fref{subfig:rest1packet}). Ceci est dû à la réception de paquets codés inutiles (n'apportant pas de nouvelles informations pour le décodage). L'exemple le plus flagrant est pour un taux de pertes de 20\%. L'envoi de 25 \% de redondance (ratio parfait) au lieu de 20\% permet de diviser par 10 la complexité de décodage, sans augmenter le nombre de paquets reçus. Au dessus, soit 30\% de redondance divise la complexité par 500, au prix d'un overhead non négligeable. 

\begin{figure} 
\centering
  \subfigure[Nombre d'opérations de décodage]{\label{subfig:rest1decoding}\includegraphics[width=0.49\textwidth]{Figures/Results/REST1/decoding.pdf}}
  \subfigure[Nombre de paquets reçus]{\label{subfig:rest1packet}\includegraphics[width=0.49\textwidth]{Figures/Results/REST1/packet.pdf}}
  \rule{35em}{0.5pt}
  \caption{Nombre d'opérations de décodage \subref{subfig:rest1decoding} et nombre de paquets reçus \subref{subfig:rest1packet} pour les différents taux d'erreur en fonction du taux de redondance inline}
  \label{fig:rest1packetdecoding}
\end{figure}


La \fref{fig:rest1redvstime15} illustre ce phénomène. On représente pour un taux d'erreur fixé à 15\%, et différents taux de redondance, la durée du flux. Les boîtes à moustaches représentent un intervalle de confiance basé sur les dix runs pour la même simulation. Les résultats sont sensiblement les même quand le taux de redondance inline est inférieur ou égal à 15\%. Au dessus de ce seuil, la durée moyenne du flux augmente (et l'intervalle de confiance diminue énormément), signe que de plus en plus de paquets inutiles ont été envoyés par l'émetteur et reçus.

\begin{figure} 
  \centering
  \includegraphics[width=0.6\textwidth]{Figures/Results/REST1/redvstime15.pdf}
  \rule{35em}{0.5pt}
  \caption{Durée du flux (moyenne en bleu et intervalle de confiance en rouge) pour 15\% d'erreur et différents taux de redondance inline}
  \label{fig:rest1redvstime15}
\end{figure}

La \fref{fig:rest1rederror} et ses sous-figures reprennent toutes les combinaisons de simulations effectuées (sans intervalle de confiance pour ne pas trop surcharge les figures). On observe sur la \fref{subfig:rest1errorvstime} qu'il est impossible de faire mieux que de ne pas envoyer de redondance inline. Quand on en transmet trop, l'overhead est forcément présent, et la durée du flux en pâti. On ne peut seulement tendre vers la durée observée lorsque aucune redondance inline n'est injectée, et que le mécanisme de fiabilité se charge de tout rattraper à la fin.

La \fref{subfig:rest1redvstime} nous permet de déduire que l'envoi de redondance en trop, quel que soit le taux d'erreur a exactement le même résultat.

\begin{figure} 
  \centering
  \subfigure[Durée pour tout taux d'erreur]{\label{subfig:rest1errorvstime}\includegraphics[width=0.49\textwidth]{Figures/Results/REST1/errorvstime.pdf}}
  \subfigure[Durée pour toute redondance]{\label{subfig:rest1redvstime}\includegraphics[width=0.49\textwidth]{Figures/Results/REST1/redvstime.pdf}}
  \rule{35em}{0.5pt}
  \caption{Durée pour tous les taux d'erreurs en fonction du taux de redondance\subref{subfig:rest1errorvstime} et durée pour tous les taux de redondance en fonction du taux d'erreur \subref{subfig:rest1redvstime}}
  \label{fig:rest1rederror}
\end{figure}


Cette version de \REST~permet dans tous les cas une fiabilité totale, atteinte avec ou sans overhead selon les conditions. L'ajout de redondance inline n'a aucun effet mélioratif sur la durée du transfert. Par contre elle permet de simplifier et d'accélérer le décodage.


\begin{figure} 
  \centering
  \includegraphics[width=0.7\textwidth]{Figures/Results/REST1/timedecoding-sinus.pdf}
  \rule{35em}{0.5pt}
  \caption{Performances de \REST~sur un taux d'erreur en sinus}
  \label{fig:rest1sinus}
\end{figure}

Pour montrer la faiblesse de cette solution, des simulations avec des pertes en burst ou sinusoïdales ont été faites. La \fref{fig:rest1sinus} montre un assez mauvais débit applicatif sur un taux de pertes en sinus. Cependant, sur toute la simulation, trop de redondance est envoyée (on a un overhead d'environ 5\% en envoyant 15\% de redondance inline).

\begin{table}[!t]
\renewcommand{\arraystretch}{1.3}
\centering
%\footnotesize
\begin{tabular}{|c|c|p{3.5cm}|p{3.5cm}|}
\hline
\bfseries Redondance & \bfseries Durée du flux & \bfseries Délai de livraison (moyen) & \bfseries Délai de livraison (écart-type)  \\
\hline
0\% & 21.96s & 2.00s & 4.91s \\
\hline
15\% & 21.96s & 0.89s & 2.12s \\
\hline
20\% & 22.53s & 0.23s & 0.32s \\
\hline
30\% & 25.01s & 0.16s & 0.06s \\
\hline
\end{tabular}
\caption{Délai de livraison applicatif}
\label{table:delay}
\end{table}

Le \tref{table:delay} introduit la notion de délai de livraison à l'application. On définit le délai de livraison d'un paquet comme le temps entre sa réception et son envoi. Dans le cas d'un paquet non perdu, cela équivaut au délai du lien. Si le paquet est perdu, cela équivaut au délai du lien plus le temps pour décoder ce paquet. Pour uniformiser un débit applicatif, il faut donc de faibles taux de livraison (moyenne des délais de chaque paquet) et un faible écart-type, comme le tableau précédemment cité le montre. Même si les deux premiers cas ne permettent pas un service de type streaming, on remarque une nette amélioration lors de l'introduction de 15\% de redondance. Pour 20 et 30\% les statistiques ne sont que mieux. On remarque que le délai moyen minimal théorique est quasiment atteint par la solution à 30\% (0.15s théorique soit le délai du lien) et un écart type quasi nul.

Le manque d'adaptabilité de cette solution induit un overhead conséquent, ou des délais et complexités de décodage bien supérieurs à ce qu'une application de streaming peut tolérer. Cette conclusion nous a motivé a fournir une version améliorer de \REST~utilisant une redondance adaptative comme présenté \sref{sec:restv2}.






\section{Redondance adaptative}
\label{sec:res2}

En utilisant le mécanisme d'adaptation de la redondance présenté en \sref{sec:restv2}, le même type de simulation a été réalisé pour observer la différence de comportements entre les deux solutions. 
Différents taux de perte sont utilisés pour les simulations à savoir :
\begin{description}
\item[Taux de perte uniforme :] 15\% d'erreur (comme en \sref{sec:res1}).
\item[Taux de perte sinusoïdal :] varie entre 0\% et 20\% selon une sinusoïdal.
\item[Taux de perte en burst :] 1\% de chance d'obtenir une erreur qui déclenche aléatoi\-rement un burst ou non de taille entre 1 et 20 paquets.
\end{description}

\begin{figure}
\centering
\subfigure[Uniforme]{\centering\label{subfig:rest2timedecoding1}\includegraphics[width=0.49\textwidth]{Figures/Results/REST2/timedecoding-uniform.pdf}}
\subfigure[Sinusoïdal]{\centering\label{subfig:rest2timedecoding2}\includegraphics[width=0.49\textwidth]{Figures/Results/REST2/timedecoding-sinus.pdf}}
\subfigure[Bursts]{\centering\label{subfig:rest2timedecoding3}\includegraphics[width=0.5\textwidth]{Figures/Results/REST2/timedecoding-burst.pdf}}
\rule{35em}{0.5pt}
\caption{\label{fig:rest2timedecoding}
Numéros de séquence acquittés (vert) et taille des matrices décodés (rouge) pour différentes distributions de pertes : uniforme\subref{subfig:rest2timedecoding1}, sinusoïdal\subref{subfig:rest2timedecoding2} et en bursts\subref{subfig:rest2timedecoding3}}
\end{figure}

\begin{figure}
\centering
\subfigure[Uniforme]{\label{subfig:rest2errorestimation1}\includegraphics[width=0.49\textwidth]{Figures/Results/REST2/errorestimation-uniform.pdf}}
\subfigure[Sinusoïdale]{\label{subfig:rest2errorestimation2}\includegraphics[width=0.49\textwidth]{Figures/Results/REST2/errorestimation-sinus.pdf}}
\subfigure[Burst]{\label{subfig:rest2errorestimation3}\includegraphics[width=0.5\textwidth]{Figures/Results/REST2/errorestimation-burst.pdf}}
\rule{35em}{0.5pt}
\caption{\label{fig:rest2errorestimation}
Taux d'erreur (bleu) et de redondance (rouge) calculés et valeur de $k$ (vert) pour différentes distributions de pertes : uniforme\subref{subfig:rest2errorestimation1}, sinusoïdale\subref{subfig:rest2errorestimation2} et en bursts\subref{subfig:rest2errorestimation3}}
\end{figure}

La \fref{fig:rest2timedecoding} présente les numéros de séquences acquittés et les tailles de matrices décodées pour ces différentes distributions de pertes alors que la \fref{fig:rest2errorestimation} présente l'évolution du taux d'erreur estimé côté récepteur, la redondance ajoutée côté émetteur et l'adaptation du paramètre $k$ au fil du temps pour les même distributions de pertes.

La \fref{subfig:rest2timedecoding1} montre un très bon débit application, quasiment uniforme. Cela est dû à une très bonne approximation du taux de perte lorsque celui-ci est uniforme, comme l'indique la \fref{subfig:rest2errorestimation1}. En plus d'offrir un bon débit applicatif, la solution ne fournit quasiment pas d'overhead (moins d'1\% de paquets inutiles).

Puisque les réseaux subissant des pertes ont des comportements bien différents, nous avons choisi d'utiliser plusieurs modèles de pertes afin de tester la robustesse des différents mécanismes. Un modèle de perte uniforme n'est sûrement pas un modèle très réaliste, et même si des pertes uniformes ne sont pas forcément représentatives, on trouve des résultats plutôt intéressants. Par contre, le modèle en burst est sûrement l'un des plus représentatif à l'heure actuelle. Pour des soucis d'implémentation lors des simulations avec des pertes en sinus ou en burst, nous avons fixé un seuil maximum pour $k$ égal à 100.

Pour les pertes sinusoïdales, représentées Figures \ref{subfig:rest2timedecoding2} et \ref{subfig:rest2errorestimation2}, on remarque, malgré une estimation des pertes qui semble coller au modèle, deux décodages de grandes matrices. Ces deux décodages de tailles équivalentes se produisent à un moment précis, lors de la phase descendante de la courbe sinusoïdale. Le même phénomène est remarquable un peu moins facilement sur les courbes des Figures \ref{subfig:rest2timedecoding3} et \ref{subfig:rest2errorestimation3}, où le déclenchement d'un burst fait brutalement augmenter le taux de perte estimé mais le décodage ne peut se faire que lors d'un retour à la normale de la mesure.

De manière générale, on arrive donc à récupérer les erreurs précédentes et donc décoder, seulement lorsque les pertes sont en train de diminuer.


La redondance adaptative permet d'obtenir de très bons résultats sur des distributions de perte uniformes mais n'arrive pas à fournir un débit applicatif assez intéressant pour des applications de streaming fiable sur des distributions de perte plus représentatives. Une idée serait encore une fois d'introduire une marge, un $\epsilon$ dans la formule du calcul de la redondance à injecter pour rajouter un overhead plus ou moins conséquent et uniformiser le débit applicatif. Cette solution n'est pas vraiment acceptable compte tenu du rajout volontaire d'overhead.

Alors que les matrices décodées sur la distribution uniforme sont plutôt limitées (50 au maximum dans cet exemple), elles explosent sur les distributions en sinus et en burst, et sont potentiellement infinies.  Pour des n\oe uds avec des capacités de calcul limités, cela s'avère être un problème. Cette problématique de contrôle des tailles de matrices nous a motivé dans l'élaboration de notre troisième et dernière version de \REST implémentation une fenêtre de codage mobile au lieu de la fenêtre glissante. Cette fenêtre mobile est présentée \sref{sec:restv3} et une analyse approfondie de ses performances est faite \sref{sec:res3}.

\section{Fenêtre mobile}
\label{sec:res3}


\begin{figure}
\centering
\subfigure[\'Evolution du transfert]{\label{subfig:rest3timedecoding-sinus}\includegraphics[width=0.49\textwidth]{Figures/Results/REST3/timedecoding-sinus.pdf}}
\subfigure[\'Evolution des pertes]{\label{subfig:rest3errorestimation-sinus}\includegraphics[width=0.49\textwidth]{Figures/Results/REST3/errorestimation-sinus.pdf}}
\rule{35em}{0.5pt}
\caption{\label{fig:rest3sinus}\subref{subfig:rest3timedecoding-sinus} : Numéros de séquence acquittés (vert) et taille des matrices décodées (rouge) .\subref{subfig:rest3errorestimation-sinus} : Taux d'erreur (bleu) et de redondance (rouge) calculés et valeur de $k$ (vert).}
\end{figure}

\begin{figure}
\centering
\subfigure[\'Evolution du transfert]{\label{subfig:rest3timedecoding-burst}\includegraphics[width=0.49\textwidth]{Figures/Results/REST3/timedecoding-burst.pdf}}
\subfigure[\'Evolution des pertes]{\label{subfig:rest3errorestimation-burst}\includegraphics[width=0.49\textwidth]{Figures/Results/REST3/errorestimation-burst.pdf}}
\rule{35em}{0.5pt}
\caption{\label{fig:rest3burst}Numéros de séquence acquittés (vert) et taille des matrices décodées (rouge) \subref{subfig:rest3timedecoding-burst}. Taux d'erreur (bleu) et de redondance (rouge) calculés et valeur de $k$ (vert)\subref{subfig:rest3errorestimation-burst}}
\end{figure}

La \fref{fig:rest3sinus} présente les résultats obtenus avec une fenêtre mobile sur une distribution de pertes sinusoïdale. La \fref{fig:rest3burst} présente les résultats pour la même version du protocole sur une distribution en bursts. La taille maximale de la fenêtre d'encodage est $W = 100$. En théorie, la taille des matrices décodées est limitée à  la taille maximale de la fenêtre soit 100 paquets (atteignable dans le cas où 100 paquets d'affilée sont perdus). En pratique, elle évolue autour de $W \times p$ où $p$ est le taux de pertes moyen depuis le dernier décodage. On remarque de cette façon que les tailles des matrices décodées suivent l'évolution du taux de pertes sur la courbe des sinus.

Cette solution arrive à de biens meilleurs résultats au niveau du débit applicatif que les autres sur des conditions moins favorables, à savoir des pertes en sinus, et le modèle de burst, très certainement le plus représentatif des pertes sur un lien. Il y a cependant un overhead non négligeable. ceci est dû au délai entre le décodage côté récepteur et la réception de l'ACK par l'émetteur.  Pendant ce temps, ce dernier envoie de la redondance (quand il doit le faire) sur la fenêtre de codage déjà reçue entièrement car décodée par le récepteur. Ces paquets sont donc ensuite ignorés à la réception. Cet overhead dépend donc de la fenêtre d'encodage, de la fréquence d'acquittement et du RTT (les différents tradeoffs sont étudiés en \sref{sec:res4}).

 Même avec un fort RTT, et un PER en burst, \REST~ délivre les données à l'application de façon uniforme avec un overhead raisonnable quand la fenêtre d'encodage est bien choisie. En effet, une petite fenêtre d'encodage avec un grand RTT aurait tendance à lisser le débit applicatif tout en augmentant l'overhead de plus en plus important. De nombreux paquets inutiles seront envoyés sur le réseau dans l'attente d'un ACK, qui prend trop de temps à arriver à cause d'un grand RTT.


Pour résumer, cette solution est bien meilleure dans des conditions plus réalistes (taux d'erreurs très variables au fil du temps), et permet selon la configuration de la taille de la fenêtre d'ajuster le lissage du débit applicatif selon les contraintes de l'application. Pour fournir une étude plus précise et poussée des performances du protocole, il est nécessaire d'étudier les différents tradeoffs et limites de \REST, ce qui est fait \sref{sec:res4}.


\section{Dépendance aux ACK}
\label{sec:res4}


\begin{figure}
\centering
\subfigure[Overhead]{\label{subfig:overhead1}\includegraphics[width=0.49\textwidth]{Figures/Results/TRADEOFF/overhead.pdf}}
\subfigure[Délai de livraison]{\label{subfig:decoding1}\includegraphics[width=0.49\textwidth]{Figures/Results/TRADEOFF/decoding.pdf}}
\rule{35em}{0.5pt}
\caption{\label{fig:tradeoff1}Overhead\subref{subfig:overhead1} et délai moyen de livraison\subref{subfig:decoding1} en fonction de la fenêtre d'encodage et du RTT.}
\end{figure}

\begin{figure}
\centering
\subfigure[Overhead]{\label{subfig:overhead2}\includegraphics[width=0.49\textwidth]{Figures/Results/TRADEOFF/overhead2.pdf}}
\subfigure[Délai de livraison]{\label{subfig:decoding2}\includegraphics[width=0.49\textwidth]{Figures/Results/TRADEOFF/decoding2.pdf}}
\rule{35em}{0.5pt}
\caption{\label{fig:tradeoff2}Overhead\subref{subfig:overhead2} et délai moyen de livraison\subref{subfig:decoding2} en fonction de la fenêtre d'encodage et de la fréquence d'ACK.}
\end{figure}


\begin{figure}
\centering
\subfigure[Overhead]{\label{subfig:overhead3}\includegraphics[width=0.49\textwidth]{Figures/Results/TRADEOFF/overhead3.pdf}}
\subfigure[Délai de livraison]{\label{subfig:decoding3}\includegraphics[width=0.49\textwidth]{Figures/Results/TRADEOFF/decoding3.pdf}}
\rule{35em}{0.5pt}
\caption{\label{fig:tradeoff3}Overhead\subref{subfig:overhead3} et délai moyen de livraison\subref{subfig:decoding3} en fonction de la fenêtre d'encodage et de la taille des bursts.}
\end{figure}


En utilisant plusieurs combinaisons de paramètres réseau, on étudie l'effet du RTT sur la fenêtre d'encodage et l'overhead \fref{fig:tradeoff1}. La \fref{subfig:overhead1} montre qu'un faible RTT et une grande fenêtre d'encodage limite l'overhead. Un seuil peut être vu au delà duquel, une petite fenêtre d'encodage couplée avec un plus grand RTT causent de plus en plus d'overhead, jusqu'à dépasser les 20\%. Ceci est dû au fait que beaucoup de paquets de redondance codent le même ensemble de paquet (fenêtre souvent pleine) et que l'ACK du récepteur met trop de temps à atteindre l'émetteur.

La \fref{subfig:decoding1} représente le délai de livraison moyen des paquets en fonction du RTT et de la taille maximale de la fenêtre d'encodage. Pour un RTT fixe, disons 500ms, elle montre un délai de livraison optimal pour une fenêtre suffisamment large, 150 dans ce cas, puisque la fenêtre d'encodage n'est jamais remplie totalement.


Avec un RTT fixé de nouveau à 300ms, il est intéressant d'étudier l'impact de la fréquence d'ACK et de la fenêtre d'encodage sur l'overhead et le délai de livraison des paquets. La \fref{subfig:overhead2} montre l'overhead moyen en fonction de l'intervalle entre deux acquittements et de la taille de la fenêtre d'encodage. Nous utilisons des intervalles entre acquittement (plus facile à utiliser que des fréquences) entre 100ms et 1900ms par pas de 200ms. Alors que la fréquence d'acquittement n'a pas d'impact sur la fiabilité de notre protocole, il y a un tradeoff entre la réduction de la complexité de décodage (et donc acquitter plus fréquemment) et un overhead de transmission.
En effet n une grande fréquence d'acquittement introduit un assez grand overhead (Les ACK sont de l'overhead), environ 10\%, quelle que soit la taille maximale de la fenêtre. Certains ACK ne réduisent même pas le fenêtre d'encodage de l'émetteur. Dans ce cas de figure, le récepteur créé bien plus d'overhead que l'émetteur. Comme remarqué auparavant, une petite fenêtre d'encodage génère aussi un overhead grandissant.
Il semblerait qu'il y ait une fréquence d'acquittement optimale dépendant des conditions du réseau.

les \fref{subfig:overhead1} et \ref{subfig:overhead2} montrent qu'il existe un tradeoff entre la fréquence d'acquittement, le RTT et la fenêtre d'encodage. Dans nos simulations, envoyer un ACK tous les trois RTT dans ce cas (donc 900ms) semble être un bon compromis.

La \fref{subfig:decoding2} représente le délai de livraison moyen à l'application des paquets en fonction de la fréquence d'acquittement et de la taille de la fenêtre d'encodage. Les résultats montrent que réduire la fréquence d'acquittement tout en ciblant un délai moyen de livraison peut être réalisé en augmentant considérablement la taille de la fenêtre d'encodage, et donc une croissance de la consommation des ressources.


La \fref{subfig:overhead3} montre l'overhead moyen en fonction des tailles de bursts maximales et de la taille de la fenêtre d'encodage. La probabilité d'apparition des bursts est toujours la même mais ont des tailles différentes, d'où un PER moyen croissant lorsque la taille maximale des bursts augmente. La même tendance est révélée que sur les \fref{subfig:overhead1} et \ref{subfig:overhead2}. Un fenêtre plus petite augmente l'overhead (encore plus lorsque les bursts grandissent). Il est possible de remarquer qu'une fenêtre plus grande par définition permet d'encaisser des bursts plus grand tout en augmentant le délai de livraison. 

La \fref{subfig:decoding3} représente le délai de livraison moyen des paquets en fonction toujours des bursts et de la taille de la fenêtre d'encodage. Elle montre un délai minimum lorsque des petits bursts sont absorbés par des fenêtres de tailles quelconques puisqu'elles ne sont quasiment jamais remplies. De la façon inverse, de grands bursts et une petite fenêtre d'encodage augmente considérablement le délai de livraison puisque la fenêtre d'encodage est constamment remplie.

Pour résumer ces résultats, deux options sont possible selon les besoins du systèmes et les conditions du réseau.
Il est soit possible de fixer un seuil de délai maximum de livraison des données à l'application (dépendant de l'application elle-même) et  d'en déduire la taille de la fenêtre minimale (la puissance de calcul minimale) pour satisfaire cette contrainte. Il est aussi possible de raisonner à l'inverse et de limiter la fenêtre d'encodage (et donc la puissance de calcul) et donc d'estimer le délai de livraison moyen qui sera assuré par \REST.
