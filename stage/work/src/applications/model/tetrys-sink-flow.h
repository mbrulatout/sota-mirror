#ifndef TETRYS_SINK_FLOW_H
#define	TETRYS_SINK_FLOW_H
/*!
 * \file tetrys-flow.h
 * \brief Abstract representation of a Tetrys flow
 * \author Mathias Brulatout
 * \date May 2014
 * \version 1
 * Contact : mathias.brulatout@gmail.com
 * \copyright Copyright (c) 2014 Thales Inc. All Rights Reserved.
 */
#include <stdio.h>
#include "math.h"
#include "time.h"

#include "ns3/packet.h"
#include "ns3/scheduler.h"
#include "ns3/simulator.h"
#include "ns3/ipv4-raw-socket-factory.h"
#include "ns3/nstime.h"
#include "ns3/socket.h"
#include "ns3/ptr.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-header.h"
#include "ns3/ipv4-l3-protocol.h"

#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/node.h"
#include "ns3/tetrys-hdr.h"
#include "ns3/tetrys-utils.h"
#include "ns3/tetrys-flow.h"


using std::pair;
using std::make_pair;
using std::list;

/*! \namespace ns3
 */
namespace ns3 {


#define FEC_MAX_BLOCK 500
#define FEC_MAX_BLOCK_SIZE 50

    /*! \struct FECBLOCK
     * \brief FEC block structure
     */
    struct fecBlock {
        int k; /*!<  */
        int n; /*!<  */
        int redCount; /*!< Indicates the number of repair pkts received in that block */
        int dataCount; /*!< Indicates the number of data pkts received in that block */
        int maxSeq; /*!<  */
        int minSeq; /*!<  */
        int blockId; /*!< Indicates the block ID */
    };

    /*! \class TetrysSinkFlow
     * \brief Tetrys sink representation
     */
    class TetrysSinkFlow : public TetrysFlow {
    public:
        /*!
         *  \brief
         *
         *
         */
      TetrysSinkFlow(Ptr<Node> src_node, Ptr<Node> dst_node, uint16_t flowID, uint32_t flow_size, Time start, Time ACKFreq);
        /*!
         *  \brief
         *
         *
         */
        ~TetrysSinkFlow();
        /*!
         *  \brief
         *
         *
         */
        virtual void receive(Ptr<Packet>);

        /*!
         *  \brief
         *
         *
         */
        Ptr<Node> GetSrcNode() const {
            return srcNode;
        }

        /*!
         *  \brief
         *
         *
         */
        Ptr<Node> GetDstNode() const {
            return dstNode;
        };

        /*!
         *  \brief
         *
         *
         */
        uint16_t GetFlow() const {
            return flowId;
        };

        /*!
         *  \brief
         *
         *
         */
        bool IsFinished() const {
            return flowFinished;
        };

        TetrysSinkFlow& operator=(const TetrysSinkFlow& src);
        TetrysSinkFlow(const TetrysSinkFlow& src);
    protected:
        /*!
         *  \brief
         *
         *
         */
        virtual void send();
    private:

        //NS3 EXCLUSIVE

        Time ackInterval; /*!<  */

        //NS2

        int numPacketReceived; /*!<  */

        int maxSeqnumNoLoss; /*!<  */
        int maxDataSeen; /**< The highest seq_num seen for a data pkt */ /*!<  */

        int nbLost; //Indicate the number of lost packets /*!<  */

        int repairListItems; /*!<  */

        int nbRepairReceived; /*!<  */
        int nbUsefulRepairReceived; //count the number of useful repair pkts received for decoding /*!<  */
        int lastDataReceived; //Last DATA packet received (order or out of order whatever) /*!<  */

        int* repairList; //List of data pkts which have not been arrived or can not be decoded/*!<  */
        int* dataList; //List of data received with index from the max_seq_num_no_loss /*!<  */

        uint8_t* buffer;

        int dataListSize; //data_list size /*!<  */
        int repairListSize; //repair_list size /*!<  */


        int packetsReceived;
        int lastSeqno;
        int sentAck;
        int totalLost;
        int repairUseless;

        float meanError;
        float alphaError;
        list<pair<double, int> > packetListError;
	int lastFlowPacket;

        //tmp window
        int maxRepairSeen;


        //FEC RELATED
        int* recoverListFEC; /*!<  */
        int blockIndexFEC; //Index to query /*!<  */
        int lastBlockFEC; /*!<  */
        struct fecBlock fecBlock[FEC_MAX_BLOCK]; /*!<  */

        void printLists(void);


        int elementsInBlockList(int *list, int size, int block_id);


        void computeErrorRate();

        /*!
         *  \brief
         *
         *
         */
        void enlargeDataList();
        /*!
         *  \brief
         *
         *
         */
        void refreshData();
        /*!
         *  \brief
         *
         *
         */
//        void sortArray(int* list, int size);
        /*!
         *  \brief
         *
         *
         */
        int elementsInList(int* list, int size);
        /*!
         *  \brief
         *
         *
         */
        void addToDataList(int seq_num);
        /*!
         *  \brief
         *
         *
         */
        void addToRepairList(int seq_num);


        /*!
         *  \brief
         *
         */
        void updateMaxSeqnumNoLoss();
        /*!
         *  \brief
         *
         * \param
         * \param
         * \param
         * \param
         * \return
         */
        bool isPacketInList(int* pt, int size, int seq_num, int action);
        /*!
         *  \brief
         *
         * \param
         * \param
         * \return
         */
        bool isRepairUseful(uint16_t* src_list);

        /*!
         *  \brief
         *
         * \param
         * \param
         */
        void processTetrysHeader(uint16_t* hdr);

        ////////////////////////////////////////////////
        // FEC
        ////////////////////////////////////////////////
        /*!
         *  \brief remove some items
         *
         * Remove items from the data_list
         * \param amount The number of items to remove
         */
        void removeItemsDataListFEC(int amount);
        /*!
         *  \brief
         *
         * \param
         */
        void pocessBlockFEC(Ptr<Packet>pkt);
        /*!
         *  \brief
         *
         * \param
         */
        void tryDecodeFEC(int index);
        /*!
         *  \brief
         *
         *
         */
        void recoverPacketsInBlockFEC(int min_seq, int max_seq);
        /*!
         *  \brief
         *
         * \param
         * \param
         * \return
         */
        int checkDataListFEC(int min_seq, int max_seq);

        ////////////////////////////////////////////////
        // UTILITIES
        ////////////////////////////////////////////////
        /*!
         *  \brief
         *
         * \param
         * \param
         */
//        void quickSort(int *numbers, int array_size);
        /*!
         *  \brief Typical quick sort
         *
         * \param numbers The array to sort
         * \param left The lower bound
         * \param right The upper bound
         */
//        void quickSort(int *numbers, int left, int right);
    };

}
#endif	/* TETRYS_SINK_FLOW_H */

