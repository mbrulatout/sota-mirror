/*!
 * \file tetrys-flow.cc
 * \brief Abstract representation of a Tetrys flow
 * \author Mathias Brulatout
 * \date May 2014
 * \version 1
 * Contact : mathias.brulatout@gmail.com
 * \copyright Copyright (c) 2014 Thales Inc. All Rights Reserved.
 */

#include "tetrys-flow.h"

namespace ns3 {

  TetrysFlow::TetrysFlow() : srcNode(0), dstNode(0), flowId(0), flowSize(0), flowSocket(0)
			   , timeEnd(), timeStart(), flowFinishing(false), flowFinished(0), sendEvent() {
  }

  TetrysFlow::~TetrysFlow() {
  }

} /* namespace ns3 */
