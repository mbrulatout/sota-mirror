#ifndef TETRYS_UTILS_H
#    define	TETRYS_UTILS_H
/*!
 * \file tetrys-flow.h
 * \brief Abstract representation of a Tetrys flow
 * \author Mathias Brulatout
 * \date May 2014
 * \version 1
 * Contact : mathias.brulatout@gmail.com
 * \copyright Copyright (c) 2014 Thales Inc. All Rights Reserved.
 */
#include<stdint.h>
/*! \namespace ns3
 */
namespace ns3 {

    /* \defgroup Tetrys Tetrys protocol implementation files
     *
     *  \brief Implementation of the different versions of Tetrys protocol.
     */

#    define STARTING_TIME 5.0

    //Error estimation
#    define ERROR_WINDOW_TIME 3 // in seconds
#    define ALPHA_VALUE 0.8

    //Traffic
  //#    define ACK_FREQ 0.2 /*!<  */
#    define CBR_FREQ 0.008 /*!<  */
#    define PACKET_SIZE 450

    //Decoding window for tetrys with mobiel window
#    define WINDOW_SIZE 100

    // Tetrys versions
#    define TETRYS_DEFAULT 0
#    define TETRYS_ADAPTATIVE 1
#    define TETRYS_MOBILE 2
#    define TETRYS_MOBILE_ADAPTATIVE 3

#define TETRYS_TYPE TETRYS_DEFAULT
  //#define TETRYS_TYPE TETRYS_ADAPTATIVE
  //#define TETRYS_TYPE TETRYS_MOBILE
  //#define TETRYS_TYPE TETRYS_MOBILE_ADAPTATIVE
  
#define TETRYS_NAME "Tetrys default"
  //#define TETRYS_NAME "Tetrys with Adaptative Redundancy"
  //#define TETRYS_NAME "Tetrys with Mobile Window"
  //#define TETRYS_NAME "Tetrys with Adaptative Redundancy and Mobile Window"

#define MAX_SIMULATION_TIME 2000
#    define TRYTHREADS false /*!<  */
#    define FEC_ACTIVE 0 /*!<  */

#    define TETRYS_DATA 1 /*!<  */
#    define TETRYS_REPAIR 2 /*!<  */
#    define TETRYS_ACK 3 /*!<  */
#    define FEC_DATA 4 /*!<  */
#    define FEC_REPAIR 5 /*!<  */

#    define MAX_DATA 20 /*!<  */
#    define MAX_REPAIR 50 /*!<  */
    //#define MAX_TEMP 1000 /*!<  */
#    define ADJUST_SIZE 50 /*!< Enlarge by this constant the receiver data list size*/
#    define RED_INFO_SIZE 3 /*!<  */
    //#define MAX_RED_ARRAY 400

    //Convenience macros
#    define MAX(a,b) ((a) > (b) ? a : b)
#    define MIN(a,b) ((a) < (b) ? a : b)

    /*! \struct TetrysFlow
     * \brief Tetrys FLow representation
     */
    struct REDUNDANCY {
        double redRatio_; /*!<  */
        int enableRatio_; /*!<  */ //= 1

        int pktCount_; /*!<  */

        //Tetrys mode
        int tetrys_min_red; /*!<  */
        int tetrys_max_red; /*!<  */

        //FEC mode
        int fecMode_; //1: Reed-Solomon MDS , 0: Tetrys /*!<  */ // FEC_ACTIVE
        int fec_n_; /*!<  */ // 20
        int fec_k_; /*!<  */ // 18
        int fec_min_; /*!<  */
        int fec_max_; /*!<  */
        int block_seq_; /*!<  */
    };

    /*! \class TetrysUtils
     * \brief Tetrys Utils representation
     */
    

    class TetrysUtils {
    public:
        static const uint8_t PROT_NUMBER = 151; /*!<  */
        static int dico(int* data, int begin, int end, int value) ;
        static void sortArray(int* list, int size);
        static void quickSort(int *numbers, int array_size);
        static void quickSort(int* array, int start, int end) ;
    };

}

#endif	/* TETRYS_UTILS_H */

