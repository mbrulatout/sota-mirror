//============================================================================
// Name        : tetrys-utils.cc
// Author      : Mathias Brulatout
// Date        : April 2014
// Version     : v0.9
// Copyright   : Copyright (c) 2014 Thales Inc. All Rights Reserved.
// Description :
//============================================================================

#include "ns3/tetrys-utils.h"
#include "ns3/fatal-error.h"
#include<stdlib.h>
#include<stdint.h>
#include <string.h>
#include "ns3/log.h"
namespace ns3 {

int TetrysUtils::dico(int* data, int begin, int end, int value) {
    int middle = (begin + end) / 2;
    if (data[middle] == value)
        return 1;
    if (begin == end)
        return 0;
    if (data[middle] > value || data[middle] == 0)
        dico(data, begin, middle, value);
    else // if (data[middle] < value)
        dico(data, middle + 1, end, value);
}

void TetrysUtils::sortArray(int* list, int size) {

    int index = 0;
    int temp_list_sort_size = size;
    int *temp_list_sort = NULL, *array = NULL;
    temp_list_sort = (int*) new (std::nothrow) int[temp_list_sort_size];
    //    NS_LOG_UNCOND("sortsize : " << temp_list_sort_size);
    //    temp_list_sort = (int*) malloc(sizeof (int) * temp_list_sort_size);
    if (NULL == temp_list_sort)
        NS_FATAL_ERROR("TetrysSinkFlow::sort_array FAIL ALLOC");
    memset(temp_list_sort, '\0', sizeof (int) *temp_list_sort_size);

    for (int i = 0; i < size; i++) { //Count non-zero elements
        if (list[i] != 0)
            temp_list_sort[index++] = list[i];
    }
    array = (int*) new (std::nothrow) int[index];
    //    NS_LOG_UNCOND("sortsize : " << temp_list_sort_size << " " << index);
    //    array = (int*) malloc(sizeof (int) * index);
    if (NULL == array)
        NS_FATAL_ERROR("TetrysSinkFlow::sort_array FAIL ALLOC 2");

    //Copy and sort array filled with non-zero elements
    memcpy(array, temp_list_sort, sizeof (int) * index);
    quickSort(array, index);
    //Copy sorted array
    memset(list, '\0', sizeof (int) * size);
    memcpy(list, array, sizeof (int) * index);

    delete[] array;
    delete[] temp_list_sort;
    //    free(temp_list_sort);
    //    free(array);
    //    array = nullptr;
    //    temp_list_sort = nullptr;
}

void TetrysUtils::quickSort(int *numbers, int array_size) {

    quickSort(numbers, 0, array_size - 1);
}
#define swap(a,b,t) ((t)=(a),(a)=(b),(b)=(t))

void TetrysUtils::quickSort(int* array, int start, int end) {
    int t;
    if (start < end) {
        int l = start + 1, r = end, p = array[start];
        while (l < r) {
            if (array[l] <= p)
                l++;
            else if (array[r] >= p)
                r--;
            else
                swap(array[l], array[r], t);
        }
        if (array[l] < p) {
            swap(array[l], array[start], t);
            l--;
        } else {

            l--;
            swap(array[l], array[start], t);
        }
        quickSort(array, start, l);
        quickSort(array, r, end);
    }
}
#undef swap

}