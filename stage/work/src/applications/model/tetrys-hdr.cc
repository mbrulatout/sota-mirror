/*!
 * \file tetrys-hdr.cc
 * \brief Tetrys header class
 * \author Mathias Brulatout
 * \date May 2014
 * \version 1
 * Contact : mathias.brulatout@gmail.com
 * \copyright Copyright (c) 2014 Thales Inc. All Rights Reserved.
 */

#include "ns3/tetrys-hdr.h"
#include "math.h"
namespace ns3 {

  TypeId TetrysHdr::GetTypeId(void) {
    static TypeId tid = TypeId("ns3::TetrysHdr").SetParent<Header > ().AddConstructor<TetrysHdr > ();
    return tid;
  }

  TypeId TetrysHdr::GetInstanceTypeId(void) const {
    return GetTypeId();
  }

  TetrysHdr::TetrysHdr() : num_(0), seqnum_(0), tettype_(0), flow_(0), flags(0) {
    this->red_array[0] = 0;
    this->red_array[1] = 0;
    this->red_array[2] = 0;
  }

  TetrysHdr::~TetrysHdr() {
  }

  void TetrysHdr::createDataHeader(uint16_t num, uint16_t seqnum, uint16_t flow,uint16_t FLAG) {
    this->num_ = num;
    this->seqnum_ = seqnum;
    this->tettype_ = TETRYS_DATA;
    this->flow_ = flow;
    this->red_array[0] = 0;
    this->red_array[1] = 0;
    this->flags = FLAG;
  }

  void TetrysHdr::createAckHeader(uint16_t num, uint16_t flow, float error,uint16_t FLAG) {
    this->num_ = num;
    this->seqnum_ = 0;
    this->tettype_ = TETRYS_ACK;
    this->flow_ = flow;
    this->red_array[0] = 0;
    this->red_array[1] = 0;
    this->red_array[2] = (uint16_t) round(error * 1000);
    this->flags = FLAG;
  }


  //Overloading Header methods

  void TetrysHdr::Serialize(Buffer::Iterator start) const {
    uint16_t * header;
    Buffer::Iterator i = start;
    header = new uint16_t[headerLength / sizeof (uint16_t)];

    header[0] = (uint16_t) this->num_;
    header[1] = (uint16_t) this->seqnum_;
    header[2] = (uint16_t) this->tettype_;
    header[3] = (uint16_t) this->flow_;
    header[4] = (uint16_t) this->red_array[0];
    header[5] = (uint16_t) this->red_array[1];
    header[6] = (uint16_t) this->red_array[2];
    header[7] = (uint16_t) this->flags;
    for (int j = 0; j < GetSerializedSize() / sizeof (uint16_t); j++) {
      i.WriteHtonU16(header[j]);
    }
    delete[] header;
  }

  uint32_t TetrysHdr::Deserialize(Buffer::Iterator start) {
    Buffer::Iterator i = start;

    this->num_ = (uint16_t) i.ReadNtohU16();
    this->seqnum_ = (uint16_t) i.ReadNtohU16();
    this->tettype_ = (uint16_t) i.ReadNtohU16();
    this->flow_ = (uint16_t) i.ReadNtohU16();
    this->red_array[0] = (uint16_t) i.ReadNtohU16();
    this->red_array[1] = (uint16_t) i.ReadNtohU16();
    this->red_array[2] = (uint16_t) i.ReadNtohU16();
    this->flags = (uint16_t) i.ReadNtohU16();
    
    return GetSerializedSize();
  }

  uint32_t TetrysHdr::GetSerializedSize(void) const {
    return this->headerLength;
  }

  std::string TetrysHdr::PrintPacket() const {
    std::stringstream os;
    os << " seqnum " << this->seqnum_ << " num " << this->num_ <<
      " type " << this->tettype_ << " flow ID " << this->flow_ << " flags : " << 
      this->flags << " redinfo (" << this->red_array[0] << ", " << 
      this->red_array[1] << ", " << this->red_array[2] << ")";
    return os.str();
  }

  void TetrysHdr::Print() const {
    NS_LOG_UNCOND(PrintPacket());
  }

  void TetrysHdr::Print(std::ostream &os) const {
    os << PrintPacket();
  }
}
