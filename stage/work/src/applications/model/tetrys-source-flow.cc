//============================================================================
// Name        : tetrys-source-flow.cc
// Author      : Mathias Brulatout
// Date        : April 2014
// Version     : v0.9
// Copyright   : Copyright (c) 2014 Thales Inc. All Rights Reserved.
// Description :
//============================================================================

#include "ns3/tetrys-source-flow.h"
#include "ns3/tetrys-utils.h"

namespace ns3 {

TetrysSourceFlow::TetrysSourceFlow() {

}

TetrysSourceFlow::TetrysSourceFlow(Ptr<Node> src_node, Ptr<Node> dst_node,
                                   uint16_t flowID, uint32_t size, Time start,
                                   string filename, int RedRatio, int WindowSize) {
    //NS2
    this->seqno = 0;
    this->minSeqno = 0;
    this->repairSeqno = 0;
    this->sequence = 0;
    this->redInfo.pktCount_ = 0;
    if (TETRYS_TYPE == TETRYS_ADAPTATIVE || TETRYS_TYPE == TETRYS_MOBILE_ADAPTATIVE)
        this->redInfo.redRatio_ = 0;
    else
        this->redInfo.redRatio_ = RedRatio;
    this->redInfo.enableRatio_ = 1;

    this->redInfo.fecMode_ = FEC_ACTIVE;
    this->redInfo.fec_k_ = 18; //CHANGED
    this->redInfo.fec_n_ = 20; //CHANGED
    this->redInfo.fec_min_ = 1;
    this->redInfo.fec_max_ = 1;
    this->redInfo.block_seq_ = 1;
    this->redInfo.tetrys_min_red = 1;
    this->redInfo.tetrys_max_red = 1;

    //NS3
    this->srcNode = src_node;
    this->dstNode = dst_node;
    this->flowId = flowID;
    this->sentBytes = 0;
    this->flowFinished = false;
    this->timeStart = start;
    this->timeEnd = start;
    this->intervalCBR = Seconds(CBR_FREQ);
    this->flowFinishing = false;
    this->errorEstimation = 0;
    this->windowSize = WindowSize;
    //FILE
    FILE* file = fopen(filename.c_str(), "r");
    if (NULL == file)
        NS_FATAL_ERROR("Error opening file");
    this->flowSize = size;
    this->buffer = new (std::nothrow) uint8_t [this->flowSize];
    memset(this->buffer, '\0', this->flowSize);
    if (NULL == this->buffer)
        NS_FATAL_ERROR("Can't malloc file");
    if (fread(this->buffer, 1, this->flowSize, file) != this->flowSize)
        NS_FATAL_ERROR("Fail reading from file.");
    fclose(file);

    //SCHEDULING
    this->sendEvent = Simulator::Schedule(start, &TetrysSourceFlow::send, this);

    //SOCKET
    this->flowSocket = Socket::CreateSocket(this->GetSrcNode(), TypeId::LookupByName("ns3::Ipv4RawSocketFactory"));
    if (flowSocket->Bind() == -1)
        NS_FATAL_ERROR("TetrysSourceFlow::TetrysSourceFlow Cannot bind.");
    this->flowSocket->SetAttribute("IpHeaderInclude", BooleanValue(true));
}

TetrysSourceFlow::~TetrysSourceFlow() {
    delete[] buffer;
    this->flowSocket = NULL;
}

TetrysSourceFlow& TetrysSourceFlow::operator=(const TetrysSourceFlow& src) {
    //NS2
    this->seqno = src.seqno;
    this->minSeqno = src.minSeqno;
    this->repairSeqno = src.repairSeqno;
    this->sequence = src.sequence;
    this->redInfo.pktCount_ = src.redInfo.pktCount_;
    this->redInfo.redRatio_ = src.redInfo.redRatio_;
    this->redInfo.enableRatio_ = src.redInfo.enableRatio_;
    this->redInfo.fecMode_ = src.redInfo.fecMode_;
    this->redInfo.fec_k_ = src.redInfo.fec_k_;
    this->redInfo.fec_n_ = src.redInfo.fec_n_;
    this->redInfo.fec_min_ = src.redInfo.fec_min_;
    this->redInfo.fec_max_ = src.redInfo.fec_max_;
    this->redInfo.block_seq_ = src.redInfo.block_seq_;
    this->redInfo.tetrys_min_red = src.redInfo.tetrys_min_red;
    this->redInfo.tetrys_max_red = src.redInfo.tetrys_max_red;

    //NS3
    this->srcNode = src.srcNode;
    this->dstNode = src.dstNode;
    this->flowSocket = src.flowSocket;
    this->flowId = src.flowId;
    this->intervalCBR = src.intervalCBR;
    this->timeStart = src.timeStart;
    this->timeEnd = src.timeEnd;
    this->sentBytes = src.sentBytes;
    this->flowFinished = src.flowFinished;
    this->errorEstimation = src.errorEstimation;
    this->windowSize = src.windowSize;
    //FILE
    this->flowSize = src.flowSize;
    this->buffer = new (std::nothrow) uint8_t [this->flowSize];
    if (NULL == this->buffer)
        NS_FATAL_ERROR("TetrysSourceFlow::operator= FAIL ALLOC.");
    memcpy(this->buffer, src.buffer, this->flowSize);

    //SCHEDULING
    Simulator::Cancel(src.sendEvent);
    this->sendEvent = Simulator::Schedule(timeStart, &TetrysSourceFlow::send, this);

    //SOCKET
    this->flowSocket = Socket::CreateSocket(this->GetSrcNode(), TypeId::LookupByName("ns3::Ipv4RawSocketFactory"));
    if (flowSocket->Bind() == -1)
        NS_FATAL_ERROR("TetrysSourceFlow::operator= Cannot bind.");
    this->flowSocket->SetAttribute("IpHeaderInclude", BooleanValue(true));

    return *this;
}

TetrysSourceFlow::TetrysSourceFlow(const TetrysSourceFlow& src) {
    //NS2
    this->seqno = src.seqno;
    this->minSeqno = src.minSeqno;
    this->redInfo.pktCount_ = src.redInfo.pktCount_;
    this->redInfo.redRatio_ = src.redInfo.redRatio_;
    this->redInfo.enableRatio_ = src.redInfo.enableRatio_;
    this->redInfo.fecMode_ = src.redInfo.fecMode_;
    this->redInfo.fec_k_ = src.redInfo.fec_k_;
    this->redInfo.fec_n_ = src.redInfo.fec_n_;
    this->redInfo.fec_min_ = src.redInfo.fec_min_;
    this->redInfo.fec_max_ = src.redInfo.fec_max_;
    this->redInfo.block_seq_ = src.redInfo.block_seq_;
    this->redInfo.tetrys_min_red = src.redInfo.tetrys_min_red;
    this->redInfo.tetrys_max_red = src.redInfo.tetrys_max_red;
    this->repairSeqno = src.repairSeqno;
    this->sequence = src.sequence;

    //NS3
    this->srcNode = src.srcNode;
    this->dstNode = src.dstNode;
    this->flowSocket = src.flowSocket;
    this->flowId = src.flowId;
    this->intervalCBR = src.intervalCBR;
    this->timeStart = src.timeStart;
    this->timeEnd = src.timeEnd;
    this->sentBytes = src.sentBytes;
    this->flowFinished = src.flowFinished;
    this->errorEstimation = src.errorEstimation;
    this->windowSize = src.windowSize;
    //FILE
    this->flowSize = src.flowSize;
    this->buffer = new (std::nothrow) uint8_t [this->flowSize];
    if (NULL == this->buffer)
        NS_FATAL_ERROR("TetrysSourceFlow::TetrysSourceFlow(const&) FAIL ALLOC.");
    memcpy(this->buffer, src.buffer, this->flowSize);

    //SCHEDULING
    Simulator::Cancel(src.sendEvent);
    this->sendEvent = Simulator::Schedule(timeStart, &TetrysSourceFlow::send, this);

    //SOCKET
    this->flowSocket = Socket::CreateSocket(this->GetSrcNode(), TypeId::LookupByName("ns3::Ipv4RawSocketFactory"));
    if (flowSocket->Bind() == -1)
        NS_FATAL_ERROR("TetrysSourceFlow::TetrysSourceFlow(const&) Cannot bind.");
    this->flowSocket->SetAttribute("IpHeaderInclude", BooleanValue(true));
}

void TetrysSourceFlow::send() {
    NS_ASSERT(sendEvent.IsExpired());
    //Send data if the entire file hasn't been processed, repair otherwise
    if (!isFinishing()) {
        uint32_t payload_size = (this->flowSize - sentBytes > PACKET_SIZE) ? PACKET_SIZE : this->flowSize - sentBytes;
        if (sentBytes + payload_size >= this->flowSize)
            this->flowFinishing = true;

        uint8_t* buff = new (std::nothrow) uint8_t[payload_size];
        if (NULL == buff)
            NS_FATAL_ERROR("TetrysSourceFlow::send FAIL ALLOC");
        memset(buff, '\0', payload_size);

        for (int i = 0; i < payload_size; i++)
            buff[i] = this->buffer[sentBytes + i];
        //memcpy is more efficient ?
        //memcpy(buff, buffer + sentBytes, payload_size)
        this->sentBytes += payload_size;

        //        NS_LOG_UNCOND(Simulator::Now().GetSeconds()
        //                      << "\t >>>>>>>>>>>>>>> "
        //                      << " Source " << this->GetSrcNode()->GetId()
        //                      << " Flow " << this->flow_id
        //                      << " DATA " << this->seqno + 1
        //                      << " with size " << payload_size
        //                      << ". " << sentBytes << "/ " << flow_size << " " << this);

	
        //Create appropriate packet and send it
        Ptr<Packet>p = Create<Packet>(buff, payload_size);
        setDataHeader(p, payload_size);
	NS_LOG_UNCOND(Simulator::Now().GetSeconds() << "\t : LOG SEND PACKET : " << seqno);
        if (flowSocket->Send(p, 0) < 0)
            NS_FATAL_ERROR(Simulator::Now().GetSeconds() << " Failed to send DATA Raw packet (error 1)");
        delete[] buff;
    }
    if (!this->IsFinished())
        scheduler();
}

void TetrysSourceFlow::scheduler() {
    if (redInfo.fecMode_ == 1) {//FEC mode
        if (isFinishing()) {
            sendEvent = Simulator::Schedule(intervalCBR, &TetrysSourceFlow::sendRepair, this);
            sendEvent = Simulator::Schedule(intervalCBR, &TetrysSourceFlow::send, this);
        } else if (redInfo.pktCount_ == redInfo.fec_k_) {
            for (int j = 0; j < redInfo.fec_n_ - redInfo.fec_k_; j++)
                sendEvent = Simulator::Schedule(Seconds(intervalCBR.GetSeconds() * (j + 1)),
                                                &TetrysSourceFlow::sendRepair, this);
            redInfo.block_seq_++;
            redInfo.fec_max_ = redInfo.fec_max_ + redInfo.fec_k_;
            redInfo.fec_min_ = redInfo.fec_min_ + + redInfo.fec_k_;
            redInfo.pktCount_ = 0;
            sendEvent = Simulator::Schedule(Seconds(intervalCBR.GetSeconds() * (redInfo.fec_n_ - redInfo.fec_k_ + 1)),
                                            &TetrysSourceFlow::send, this);
        } else
            sendEvent = Simulator::Schedule(intervalCBR, &TetrysSourceFlow::send, this);
    } else { //Tetrys mode
        //NS_LOG_UNCOND(redInfo.pktCount_ << " out of " << floor(100.0 / redInfo.redRatio_));
        if (redInfo.redRatio_ == 0.0) {//To avoid division by 0
            redInfo.pktCount_ = 0;
            if (isFinishing())
                sendEvent = Simulator::Schedule(intervalCBR, &TetrysSourceFlow::sendRepair, this);
            sendEvent = Simulator::Schedule(intervalCBR, &TetrysSourceFlow::send, this);
        } else {//Redundancy case
            if (isFinishing()) //Ending session : REPAIR needed until ACK
            {
                sendEvent = Simulator::Schedule(intervalCBR, & TetrysSourceFlow::sendRepair, this);
                sendEvent = Simulator::Schedule(intervalCBR, &TetrysSourceFlow::send, this);
            } else {
                if ((redInfo.pktCount_ >= floor(100.0 / redInfo.redRatio_))) { // REPAIR needed
                    sendEvent = Simulator::Schedule(intervalCBR, &TetrysSourceFlow::sendRepair, this);
                    sendEvent = Simulator::Schedule(intervalCBR + intervalCBR, &TetrysSourceFlow::send, this);
                    redInfo.pktCount_ = 0;
                } else //DATA needed
                    sendEvent = Simulator::Schedule(intervalCBR, &TetrysSourceFlow::send, this);
            }
        }
    }
}

void TetrysSourceFlow::sendRepair() {

    //RANDOM REPAIR PACKET
    uint8_t* buff = new uint8_t [PACKET_SIZE];
    for (int i = 0; i < PACKET_SIZE; i++)
        buff[i] = (uint8_t) (rand() % 256);

    sequence++;
    repairSeqno++;

    Ptr<Packet> p = Create<Packet>(buff, PACKET_SIZE);
    TetrysHdr tetheader;
    //Type is not determined yet
    tetheader.createDataHeader(repairSeqno, sequence, flowId,flowFinishing);
    tetheader.setType(TETRYS_REPAIR);
    tetheader.setRedInfo(0, redInfo.tetrys_min_red + 1);
    if (TETRYS_TYPE == TETRYS_MOBILE || TETRYS_TYPE == TETRYS_MOBILE_ADAPTATIVE) // FIXED SIZE WINDOW
    {
        // from last_acked packet until seqno or last_acked + window is it's too large
        //1-2 ... 1-20 ... 1-50 ... 1-50 ... DECODE ... 66 - 80 ... 66 - 100 ... DECODE 80 - 100 ... 80 129
        tetheader.setRedInfo(1, (seqno < redInfo.tetrys_min_red + this->windowSize) ? seqno : redInfo.tetrys_min_red + this->windowSize - 1);

	tetheader.setFlag((seqno < redInfo.tetrys_min_red + windowSize && isFinishing())? 1 : 0);
        //        NS_LOG_UNCOND("Setting repair packet from " << tetheader.getRedInfo(0) << "  until " << tetheader.getRedInfo(1));
    } else
        tetheader.setRedInfo(1, seqno);

    //tetheader.Print();
    p->AddHeader(tetheader);
    Ipv4Header ipHeader;
    ipHeader.SetProtocol(TetrysUtils::PROT_NUMBER);
    ipHeader.SetSource(this->GetSrcNode()->GetObject<Ipv4L3Protocol > ()->GetAddress(1, 0).GetLocal());
    ipHeader.SetDestination(this->GetDstNode()->GetObject<Ipv4L3Protocol > ()->GetAddress(1, 0).GetLocal());
    ipHeader.SetTtl(32);
    ipHeader.SetPayloadSize(PACKET_SIZE + tetheader.GetSerializedSize());
    p->AddHeader(ipHeader);

    if (flowSocket->Send(p, 0) < 0)
        NS_FATAL_ERROR(Simulator::Now().GetSeconds() << " Failed to send REPAIR Raw packet (error 1)");
    delete[] buff;
}

void TetrysSourceFlow::setDataHeader(Ptr<Packet>pkt, int payload_size) {
    sequence++;
    seqno++;
    redInfo.pktCount_++;

    TetrysHdr tetheader;
    tetheader.createDataHeader(seqno, sequence, this->flowId,this->flowFinishing);
    if ((seqno == 1) && redInfo.fecMode_) {
        redInfo.fec_min_ = 1;
        redInfo.fec_max_ = redInfo.fec_min_ + redInfo.fec_k_ - 1;
    }

    pkt->AddHeader(tetheader);
    Ipv4Header ipHeader;
    ipHeader.SetProtocol(TetrysUtils::PROT_NUMBER);
    ipHeader.SetSource(this->GetSrcNode()->GetObject<Ipv4L3Protocol > ()->GetAddress(1, 0).GetLocal());
    ipHeader.SetDestination(this->GetDstNode()->GetObject<Ipv4L3Protocol > ()->GetAddress(1, 0).GetLocal());
    ipHeader.SetPayloadSize(payload_size + tetheader.GetSerializedSize());
    ipHeader.SetTtl(32);
    pkt->AddHeader(ipHeader);
}

void TetrysSourceFlow::receive(Ptr<Packet> pkt) {
    Ipv4Header ipHeader;
    pkt->RemoveHeader(ipHeader);
    TetrysHdr tetheader;
    pkt->RemoveHeader(tetheader);

    //    NS_LOG_UNCOND(Simulator::Now().GetSeconds()
    //                  << " Source " << this->GetSrcNode()->GetId()
    //                  << " Flow " << this->flow_id
    //                  << " ACK RECEIVED");
    minSeqno = tetheader.getNum();
    redInfo.tetrys_min_red = MAX(redInfo.tetrys_min_red, tetheader.getNum());
    this->errorEstimation = tetheader.getErrorRate() / 1000.0;
    NS_LOG_UNCOND((Simulator::Now() - this->timeStart).GetSeconds() <<
                  "\t ACK :" << minSeqno << " : " << this->errorEstimation);

    //    NS_LOG_UNCOND((Simulator::Now() - this->timeStart).GetSeconds() << "\t  ACK : " << minSeqno
    //                  << ", redInfo.tetrys_min_red " << redInfo.tetrys_min_red
    //                  << " total sent : " << sentBytes << " received : " << (minSeqno * PACKET_SIZE));
    int old_red_ratio = redInfo.redRatio_;

    if (TETRYS_TYPE == TETRYS_ADAPTATIVE || TETRYS_TYPE == TETRYS_MOBILE_ADAPTATIVE) {
        float R = ceil(100 * (1.0 / (1.0 - this->errorEstimation) - 1)) / 100;
        if (R > 1.0)
            R = 1.0; // More than 50% of loss implies 100% redundancy
        this->redInfo.redRatio_ = R * 100;
        NS_LOG_UNCOND(Simulator::Now().GetSeconds() - this->timeStart.GetSeconds() << " \t REDUNDANCY : " << this->redInfo.redRatio_ <<
                      " : \t" << ((this->redInfo.redRatio_ == 0) ? 100 : (floor(100.0 / redInfo.redRatio_))));
    }
    //    NS_LOG_UNCOND("Updating redundancy ratio : " << old_red_ratio <<
    //                  " R >= " << R << " real R >= " << R2 <<
    //                  " k= " << k << " real k = " << k2 <<
    //                  "NEW RED RATIO " << redInfo.redRatio_);

    if (minSeqno * PACKET_SIZE >= this->flowSize) {
        this->timeEnd = Simulator::Now();
        NS_LOG_UNCOND("Source has finished ! Execution time :"
                      << this->timeEnd.GetSeconds() - this->timeStart.GetSeconds());
        NS_LOG_UNCOND("");
        this->flowFinished = true;
    }
}

}
