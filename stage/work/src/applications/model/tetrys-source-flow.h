#ifndef TETRYS_SOURCE_FLOW_H
#define	TETRYS_SOURCE_FLOW_H
/*!
 * \file tetrys-flow.h
 * \brief Abstract representation of a Tetrys flow
 * \author Mathias Brulatout
 * \date May 2014
 * \version 1
 * Contact : mathias.brulatout@gmail.com
 * \copyright Copyright (c) 2014 Thales Inc. All Rights Reserved.
 */

#include <stdio.h>
#include <random>
#include <stdint.h>
#include <string>

#include "ns3/simulator.h"
#include "ns3/boolean.h"
#include "ns3/nstime.h"
#include "ns3/ptr.h"
#include "ns3/scheduler.h"
#include "ns3/node.h"
#include "ns3/packet.h"

#include "ns3/socket.h"
#include "ns3/ipv4-raw-socket-factory.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/ipv4-address.h"

#include "ns3/tetrys-hdr.h"
#include "ns3/tetrys-utils.h"
#include "ns3/tetrys-flow.h"
/*! \namespace ns3
 */
using std::ifstream;
using std::string;
namespace ns3 {

    /*! \class TetrysFlow
     * \brief Tetrys FLow representation
     */
    class TetrysSourceFlow : public TetrysFlow {
    public:
        /*!
         *  \brief
         *
         *
         */
        TetrysSourceFlow(Ptr<Node> src_node, Ptr<Node> dst_node, uint16_t flowID,
                uint32_t size, Time start, string filename, int RedRatio, int WindowSize);
        /*!
         *  \brief
         *
         *
         */
        ~TetrysSourceFlow();
        /*!
         *  \brief
         *
         *
         */
        virtual void receive(Ptr<Packet> p);
        /*!
         *  \brief
         *
         *
         */
        TetrysSourceFlow();

        Ptr<Node> GetSrcNode() const {
            return this->srcNode;
        }

        Ptr<Node> GetDstNode() const {
            return this->dstNode;
        }

        uint16_t GetFlow() const {
            return this->flowId;
        }

        bool IsFinished() const {
            return flowFinished;
        }



        TetrysSourceFlow& operator=(const TetrysSourceFlow& src);
        TetrysSourceFlow(const TetrysSourceFlow& src);
    protected:
        /*!
         *  \brief
         *
         *
         */
        virtual void send();
    private:

        //NS3

        Time intervalCBR; /*!<  */



        uint8_t* buffer; /*!<  */
        uint32_t sentBytes; /*!<  */
        float errorEstimation;
        int windowSize;

        //NS2
        struct REDUNDANCY redInfo; /*!<  */
        int seqno; /*!<  */
        int *repairArray; /*!<  */
        int minSeqno; /*!<  */
        int repairSeqno; /*!<  */

        int sequence; /*!<  */

        /*!
         *  \brief
         *
         *
         */
        bool isFinishing() const {
            return flowFinishing;
        }
        /*!
         *  \brief
         *
         *
         */
        void setDataHeader(Ptr<Packet>pkt, int packet_size);
        /*!
         *  \brief
         *
         *
         */
        void sendRepair();
        /*!
         *  \brief
         *
         *
         */
        void scheduler();
        /*!
         *  \brief
         *
         *
         */
        void dataProcessing(Ptr<Packet>pkt);
    };
}

#endif	/* TETRYS_SOURCE_FLOW_H */

