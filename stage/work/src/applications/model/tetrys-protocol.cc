/*!
 * \file tetrys-protocol.cc
 * \brief Tetrys protocol (deamon implementation)
 * \author Mathias Brulatout
 * \date May 2014
 * \version 1
 * Contact : mathias.brulatout@gmail.com
 * \copyright Copyright (c) 2014 Thales Inc. All Rights Reserved.
 */
#include "ns3/tetrys-protocol.h"

namespace ns3 {

  using std::string;

  NS_LOG_COMPONENT_DEFINE("TetrysProtocol");
  NS_OBJECT_ENSURE_REGISTERED(TetrysProtocol);

  TypeId TetrysProtocol::GetTypeId() {
    static TypeId tid = TypeId("ns3::TetrysProtocol").SetParent<Application > ().AddConstructor<TetrysProtocol > ();
    return tid;
  }

  TetrysProtocol::TetrysProtocol() : appIsStopped(false), forwardSocket(0) {
#if TRYTHREADS == false
    this->threadStop = false;
    this->readThread = false;
#else
    this->ipThread = 0;
#endif
  }

  TetrysProtocol::~TetrysProtocol() {
  }

  void TetrysProtocol::StartApplication(void) {
#if TRYTHREADS == true
    /* start processing packets */
    this->ipThread = createInterceptPacketThread();
    NS_LOG_UNCOND("THREAD");
#else
    readThread = true;
    Simulator::ScheduleNow(&TetrysProtocol::processIpQueue, this);
#endif
    //Every app/node has its own socket to forward packets which aren't theirs.
    this->forwardSocket = Socket::CreateSocket(this->GetNode(), TypeId::LookupByName("ns3::Ipv4RawSocketFactory"));
    this->forwardSocket->SetAttribute("IpHeaderInclude", BooleanValue(true));
    if (this->forwardSocket->Bind() == -1) {
      NS_LOG_UNCOND("FAIL : Cannot bind forward socket.");
      return;
    }
  }

  void TetrysProtocol::installTetrysSourceFlow(Ptr<Node> src, Ptr<Node> dst, uint16_t flowID, uint32_t size, Time start, string filename, int RedRatio, int WindowSize) {
    TetrysSourceFlow* a = new TetrysSourceFlow(src, dst, flowID, size, start, filename, RedRatio, WindowSize);
    this->sourceVector.push_back(*a);
    delete a;
  }

  void TetrysProtocol::installTetrysSinkFlow(Ptr<Node> src, Ptr<Node> dst, uint16_t flowID, uint32_t size, Time start, Time ACKFreq) {
    TetrysSinkFlow* a = new TetrysSinkFlow(src, dst, flowID, size, start, ACKFreq);
    this->sinkVector.push_back(*a);
    delete a;
  }

  void TetrysProtocol::StopApplication(void) {
    NS_LOG_FUNCTION(this);
    NS_LOG_UNCOND("Stopping Application ...");
    
    if (this->forwardSocket != NULL)
      this->forwardSocket->Close();
    this->appIsStopped = true;

#if TRYTHREADS == false
    {
      CriticalSection cs(pendingReadMutex);
      this->threadStop = true;
    }
#else
    //if (pthread_cancel(ipThread) == 0) {
#endif
  }
  
  void TetrysProtocol::forwardPacket(Ptr<Packet> p) {
    Ipv4Header ipheader;
    p->RemoveHeader(ipheader);
    ipheader.SetTtl(ipheader.GetTtl() - 1);
    if (ipheader.GetTtl() <= 0)
      dropPacket(p, "FAIL TTL");
    else {
      p->AddHeader(ipheader);
      //Send packet if TTL > 0
      if (this->forwardSocket->GetTxAvailable() >= p->GetSize())
	if (this->forwardSocket->Send(p, 0) < 0)
	  NS_FATAL_ERROR(Simulator::Now().GetSeconds() << " On node: " << this->GetNode()->GetId() << " Failed to send Raw packet (error 1)");
      
    }
  }

  void TetrysProtocol::processIpQueue() {
#if TRYTHREADS == true
    while (true) {
      pthread_mutex_lock(&this->countMutex);
      pthread_cond_wait(&this->conditionVar, &this->countMutex);
#else
      while (!ipQueue.empty()) {
#endif
	//NS_LOG_UNCOND(this->GetNode()->GetId() << " : Processing IP Queue");
	Ptr<Packet> p;
	Ptr<Packet> packet;
#if TRYTHREADS == false
	{
	  CriticalSection cs(this->pendingReadMutex);
#endif
	  p = this->ipQueue.front();
	  this->ipQueue.pop();
	  //Always work on a copy
	  packet = p->Copy();
	  
#if TRYTHREADS == false
	}
#endif
	Ipv4Header ipHeader;
	packet->RemoveHeader(ipHeader);
	Ipv4Address ipDest = ipHeader.GetDestination();
	
	/* don't intercept packet if not unicast */
	if (ipDest.IsBroadcast() || ipDest.IsMulticast())
	  forwardPacket(p);

	//if IP header encapsulates Tetrys protocol
	if (ipHeader.GetProtocol() == TetrysUtils::PROT_NUMBER) {
	  TetrysHdr tetrysHeader;
	  packet->RemoveHeader(tetrysHeader);

	  if (this->GetNode()->GetObject<Ipv4L3Protocol > ()->IsDestinationAddress(ipHeader.GetDestination(), 0)) {
	    // Schedule if I'm the destination
	    if (tetrysHeader.getType() == TETRYS_ACK) {
	      for (std::vector<TetrysSourceFlow>::iterator it = this->sourceVector.begin(); it != this->sourceVector.end(); ++it) {
		if (tetrysHeader.getFlow() == it->GetFlow()) {
		  // Source scheduling
		  Simulator::ScheduleNow(&TetrysSourceFlow::receive, &(this->sourceVector[it - this->sourceVector.begin()]), p);
		}
	      }
	    } else {
	      for (std::vector<TetrysSinkFlow>::iterator it = this->sinkVector.begin(); it != this->sinkVector.end(); ++it) {
		if (tetrysHeader.getFlow() == it->GetFlow()) {
		  // Sink scheduling
		  Simulator::ScheduleNow(&TetrysSinkFlow::receive, &(this->sinkVector[it - this->sinkVector.begin()]), p);
		}
		
	      }

	    }
	  } else {
	    //Forward packet if I'm not the destination
	    forwardPacket(p);
	  }
	} else {
	  NS_LOG_UNCOND("!!!!!!!!!!!! GOT NON-TETRYS PACKET");
	}
#if TRYTHREADS == true
	pthread_mutex_unlock(&this->countMutex);
#else
	if (threadStop == true) {
	  {
	    CriticalSection cs(pendingReadMutex);
	    readThread = false;
	  }
	}
#endif
      }
    }
    
    void TetrysProtocol::dropPacket(Ptr<Packet> p, std::string errorMsg) {
      
      NS_LOG_UNCOND("!!!!!! ** On node: " << this->GetNode()->GetId() << " packet dropped. Weird... Error message: " << (errorMsg.compare("") == 0 ? "Not specified" : errorMsg) << " ** !! **");
      return;
    }

    void TetrysProtocol::interceptPacketTetrys(Ptr<Packet> p, Ipv4Header ipHeader) {
      interceptPacket(p, ipHeader);
    }
    
    void TetrysProtocol::interceptPacket(Ptr<Packet> p, Ipv4Header ipHeader) {
#if TRYTHREADS == false
      {
	CriticalSection cs(this->pendingReadMutex);
#else
	pthread_mutex_lock(&countMutex);
#endif
	//Add ip header and push it for processing
	p->AddHeader(ipHeader);
	this->ipQueue.push(p);
	Simulator::ScheduleNow(&TetrysProtocol::processIpQueue, this);
#if TRYTHREADS == false
      }
#else
      pthread_cond_signal(&conditionVar);
      pthread_mutex_unlock(&countMutex);
#endif
    }

    /**********************************************************************
     **********************************************************************
     **********************************************************************
     ********************** THREAD RELATED FUNCTIONS **********************
     **********************************************************************
     **********************************************************************
     **********************************************************************/


#if TRYTHREADS == true

    pthread_t TetrysProtocol::createInterceptPacketThread() {
      pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
      this->countMutex = mutex;
      pthread_cond_t condition = PTHREAD_COND_INITIALIZER;
      this->conditionVar = condition;
      pthread_t processQueueThread;
      void* (*f)(void*) = &TetrysProtocol::threadEntryPoint;

      pthread_create(&processQueueThread, NULL, f, this);

      return processQueueThread;
    }

    void* TetrysProtocol::threadEntryPoint(void *object) {
      static_cast<TetrysProtocol*> (object)->processIpQueue();

      return NULL;
    }

#endif

  } /* namespace ns3 */
