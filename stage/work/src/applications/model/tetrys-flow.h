#ifndef TETRYS_FLOW_H_
#define TETRYS_FLOW_H_
/*!
 * \file tetrys-flow.h
 * \brief Abstract representation of a Tetrys flow
 * \author Mathias Brulatout
 * \date May 2014
 * \version 1
 * Contact : mathias.brulatout@gmail.com
 * \copyright Copyright (c) 2014 Thales Inc. All Rights Reserved.
 */
#include <stdint.h>
#include "ns3/ptr.h"
#include "ns3/nstime.h"
#include "ns3/scheduler.h"
#include "ns3/log.h"
#include "ns3/node.h"
#include "ns3/socket.h"
#include "ns3/event-id.h"
#include "ns3/tetrys-utils.h"
/*! \namespace ns3
 */
namespace ns3 {

  /*! \class TetrysFlow
   * \ingroup Tetrys
   * \brief Tetrys FLow representation
   */
  class TetrysFlow {
  protected:
    Ptr<Node> srcNode; /*!< Flow source node*/
    Ptr<Node> dstNode; /*!< Flow sink node*/
    uint16_t flowId; /*!< Flow identifier*/
    uint32_t flowSize; /*!< Flow size (amount of bytes to transmit)*/
    Ptr<Socket> flowSocket; /*!< Socket used for transmission/reception*/

    Time timeStart; /*!< Flow starting time*/
    Time timeEnd; /*!< Flow ending time*/

    bool flowFinished; /*!< true if flow is finished, false otherwise*/
    bool flowFinishing; /*!< true if flow is finishing (i.e. source has sent the entire flow, false otherwise*/

    EventId sendEvent; /*!< Event for scheduling purpose*/

    /*!
     *  \brief sending virtual function
     *
     *  Send data through the flow socket. Must be implemented in subclasses
     */
    virtual void send() = 0;

  public:
    /*!
     *  \brief receiving virtual function
     *
     *  Process a received packet
     * \param p The received packet
     */
    virtual void receive(Ptr<Packet> p) = 0;

    /*!
     *  \brief Constructor
     *
     *  TetrysFlow constructor
     */
    TetrysFlow();

    /*!
     *  \brief Destructor
     *
     *  TetrysFlow destructor
     */
    virtual ~TetrysFlow();

  };

}

#endif /* TETRYS_FLOW_H_ */
