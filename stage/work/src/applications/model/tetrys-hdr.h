#ifndef TETRYS_HDR_H_
#define	TETRYS_HDR_H_
/*!
 * \file tetrys-hdr.h
 * \brief Tetrys header class
 * \author Mathias Brulatout
 * \date May 2014
 * \version 1
 * Contact : mathias.brulatout@gmail.com
 * \copyright Copyright (c) 2014 Thales Inc. All Rights Reserved.
 */

#include <utility>
#include <list>

#include "ns3/header.h"
#include "ns3/log.h"
#include "ns3/tetrys-utils.h"
/*! \namespace ns3
 */
namespace ns3 {

  /*! \class TetrysHdr
   * \brief Tetrys header serialization and deserialization
   *
   * Must implement the following methods inherited from abstract class Header :
   *   - static TypeId GetTypeId (void);
   *   - virtual ~Header ();
   *   - virtual uint32_t GetSerializedSize (void) const = 0;
   *   - virtual void Serialize (Buffer::Iterator start) const = 0;
   *   - virtual uint32_t Deserialize (Buffer::Iterator start) = 0;
   *   - virtual void Print (std::ostream &os) const = 0;
   */
  class TetrysHdr : public Header {
    uint16_t num_; /*!< packet type-specific sequence number  */
    uint16_t tettype_; /*!< Tetrys packet type (data = 1, repair = 2, ack = 3) */

    uint16_t seqnum_; /*!< For both data and repair packets. Nice for error rate implementation*/

    uint16_t red_array[RED_INFO_SIZE]; /*!<  */
    uint16_t flow_; /*!< Packet flow identifer  */
    uint16_t flags;
    static const uint16_t headerLength = 16; /*!< Header size in bytes*/
	
  public:
    /*!
     *  \brief Constructor
     *
     * Tetrys header constructor
     */
    TetrysHdr();
    /*!
     *  \brief Destructor
     *
     * Tetrys header destructor
     */
    virtual ~TetrysHdr();
    /*!
     *  \brief initialize data header
     *
     * Initialize a header for any data packet
     * \param num data sequence number
     * \param seqnum global sequence number
     * \param flow flow identifier from which the packet belongs to
     * \param FLAG packet flag (can be extended)
     */
    void createDataHeader(uint16_t num, uint16_t seqnum, uint16_t flow, uint16_t FLAG);
    /*!
     *  \brief initialize an ack header
     *
     * initialize an ack header
     * \param num ack sequence number
     * \param flow flow identifier from which the packet belongs to
     * \param error Error rate estimation
     * \param FLAG packet flag (can be extended)
     */
    void createAckHeader(uint16_t num, uint16_t flow, float error ,uint16_t FLAG);

    /*!
     *  \brief Set the packet type
     *
     * Set the packet type (data = 1, repair = 2, ack = 3)
     * \param type The packet type
     */
    void setType(uint16_t type) {
      this->tettype_ = type;
    }
    void setFlag(uint16_t flag){
      this->flags = flag;
    }
    /*!
     *  \brief Set a specific redundancy field
     *
     * Set a specific redundancy field using the index and its value
     * \param i the field's index
     * \param value the field's value
     */
    void setRedInfo(uint16_t i, uint16_t value) {
      this->red_array[i] = value;
    }
    /*!
     *  \brief Set the packet number
     *
     * \param num The packet number
     */
    void setNum(uint16_t num) {
      this->num_ = num;
    }
    /*!
     *  \brief Return header length
     */
    static const uint16_t getSize(void) {
      return headerLength;
    }
    /*!
     *  \brief Return packet type
     */
    const uint16_t getType(void) const {
      return this->tettype_;
    }
    /*!
     *  \brief Return packet number
     */
    const uint16_t getNum(void) const {
      return this->num_;
    }
    /*!
     *  \brief Return packet flow ID
     */
    const uint16_t getFlow(void) const {
      return this->flow_;
    }
    /*!
     *  \brief Return packet sequence number
     */
    const uint16_t getSeqNum(void) const {
      return this->seqnum_;
    }
    /*!
     *  \brief Return packet flags
     */
    const uint16_t getFlag(void) const {
      return this->flags;
    }
    /*!
     *  \brief Return packet redundancy specific field
     *
     * \param i Field's index
     */
    const uint16_t getRedInfo(uint16_t i) const {
      return this->red_array[i];
    }
    /*!
     *  \brief Return packet redundancy info
     */
    uint16_t* getRedArray() {
      return this->red_array;
    }
    /*!
     *  \brief Return error rate
     */
    const uint16_t getErrorRate() const {
      return this->red_array[2];
    }
    
    /*!
     *  \brief
     *
     *
     */
    static TypeId GetTypeId(void);
    /*!
     *  \brief
     *
     *
     */
    virtual TypeId GetInstanceTypeId(void) const;
    /*!
     *  \brief
     *
     *
     */
    virtual void Print(std::ostream &os) const;
    /*!
     *  \brief
     *
     *
     */
    virtual void Serialize(Buffer::Iterator start) const;
    /*!
     *  \brief
     *
     *
     */
    virtual uint32_t Deserialize(Buffer::Iterator start);
    /*!
     *  \brief
     *
     *
     */
    virtual uint32_t GetSerializedSize(void) const;
    /*!
     *  \brief
     *
     *
     */
    std::string PrintPacket() const;
    /*!
     *  \brief
     *
     *
     */
    void Print() const;
  };

} //namespace ns3

#endif	/* TETRYS_HDR_H_ */

