#ifndef TETRYS_PROTOCOL_H_
#define TETRYS_PROTOCOL_H_
/*!
 * \file tetrys-protocol.h
 * \brief Tetrys protocol (deamon implementation)
 * \author Mathias Brulatout
 * \date May 2014
 * \version 1
 * Contact : mathias.brulatout@gmail.com
 * \copyright Copyright (c) 2014 Thales Inc. All Rights Reserved.
 */
#include <vector>
#include <queue>
#include <stdint.h>
#include <string>

#include "ns3/system-mutex.h"
#include "ns3/boolean.h"

#include "ns3/application.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/node.h"
#include "ns3/node-list.h"
#include "ns3/packet.h"

#include "ns3/ipv4-header.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/socket.h"

#include "ns3/tetrys-hdr.h"
#include "ns3/tetrys-source-flow.h"
#include "ns3/tetrys-sink-flow.h"
#include "ns3/tetrys-utils.h"
/*! \namespace ns3
 */
namespace ns3 {

  /*! \class TetrysFlow
   * \brief Tetrys FLow representation
   */
  class TetrysProtocol : public Application {
  public:

    /*!
     * \fn bool TetrysProtocol::~TetrysProtocol()
     * \brief Constructor
     */
    TetrysProtocol();
    /*!
     * \fn bool TetrysProtocol::~TetrysProtocol()
     * \brief Destructor
     */
    virtual ~TetrysProtocol();
    /*!
     * \fn bool TetrysProtocol::~TetrysProtocol()
     * \brief Get the type ID
     *
     * \return tid The TypeID
     */
    static TypeId GetTypeId(void);
    /*!
     * \fn void TetrysProtocol::Install_Tetrys_Source_Flow(Ptr<Node> src, Ptr<Node> dst, uint16_t flowID, uint16_t size, Time start)
     * \brief Create a source for a flow.
     *
     * \param src The flow source node.
     * \param dst The flow destination node.
     * \param flowID the flow identifier.
     * \param size The flow size.
     * \param start The flow starting time.
     * \param filename The file to send
     * \param RedRatio Inline redundancy ratio parameter
     * \param WidowSize Encoding window maximum size
     */
    void installTetrysSourceFlow(Ptr<Node> src, Ptr<Node> dst, uint16_t flowID, uint32_t flow_size, Time start, string filename, int RedRatio, int WindowSize);
    /*!
     * \fn void TetrysProtocol::Install_Tetrys_Sink_Flow(Ptr<Node> src, Ptr<Node> dst, uint16_t flowID, uint16_t size, Time start)
     * \brief Create a sink for a flow.
     *
     * \param src The flow source node.
     * \param dst The flow destination node.
     * \param flowID the flow identifier.
     * \param size The flow size.
     * \param start The flow starting time.
     * \param ACKFreq Interval between two ACKs
     */
    void installTetrysSinkFlow(Ptr<Node> src, Ptr<Node> dst, uint16_t flowID, uint32_t flow_size, Time start, Time ACKFreq);

    /*!
     * \fn bool TetrysProtocol::IsApplicationStopped()
     *  \brief app_stopped getter
     *
     * \return Returns 1 if the app is stopped, 0 otherwise.
     */
    bool isApplicationStopped() {
      return appIsStopped;
    }

    /*!
     * \fn bool TetrysProtocol::IsFinished()
     * \brief Check whether the application is finished or not.
     *
     * \return true if it's finished, 0 otherwise.
     */
    bool isFinished() {
      appIsStopped = true;
      for (std::vector<TetrysSourceFlow>::iterator it = this->sourceVector.begin(); it != this->sourceVector.end(); ++it)
	if (!it->IsFinished())
	  appIsStopped = false;
      return appIsStopped;
    }
    /*!
     * \fn void TetrysProtocol::StartApplication(void)
     * \brief Start the application (Schedule the IP queue processing and create the socket).
     */
    void StartApplication(void);
    /*!
     * \fn void TetrysProtocol::StopApplication(void)
     * \brief Close the application socket.
     */
    void StopApplication(void);
    /*!
     * \fn void TetrysProtocol::InterceptPacketTetrys(Ptr<Packet> p, Ipv4Header ipHeader)
     * \brief Specific processing for intercepting a tetrys packet. Just calls the InterceptPacket function.
     *
     * \param p The intercepted packet from the hook.
     * \param ipHeader IP Header taken out from the packet.
     */
    void interceptPacketTetrys(Ptr<Packet>, Ipv4Header);


  private:
    bool appIsStopped; /*!<  True if all flows have ended */

    std::vector<TetrysSourceFlow> sourceVector; /*!< Source agent container */
    std::vector<TetrysSinkFlow> sinkVector; /*!< Sink agent container */

    Ptr<Socket> forwardSocket; /*!< Forwarding socket */
    std::queue<Ptr<Packet> > ipQueue; /*!< Queue of IP packets to process */

    /*!
     * \fn void TetrysProtocol::ProcessIPqueue()
     * \brief process the queue that contains all IP packets intercepted by netfilter on this node.
     */
    void processIpQueue();

    /*!
     * \fn void TetrysProtocol::InterceptPacket(Ptr<Packet> p, Ipv4Header ipHeader)
     * \brief General function for sending an intercepted packet to the process queue.
     *
     * \param p The intercepted packet from the hook
     * \pram ipHeader IP Header taken out from the packet.
     */
    void interceptPacket(Ptr<Packet>, Ipv4Header);
    /*!
     * \fn void TetrysProtocol::ForwardPacket(Ptr<Packet> p)
     * \brief Forward a packet through a raw socket.
     *
     * \param p The packet to send.
     */
    void forwardPacket(Ptr<Packet> p);
    /*!
     * \fn void TetrysProtocol::dropPacket(Ptr<Packet> p, std::string errorMsg)
     * \brief Drops the packet given as an argument and prints an error message.
     *
     * \param p The packet to drop.
     * \param errorMsg The error message explaining the reason of the drop.
     */
    void dropPacket(Ptr<Packet> p, std::string errorMsg);

    //TO KEEP THREADS
#if TRYTHREADS == true
    pthread_t ipThread; /*!<  */
    pthread_mutex_t countMutex; /*!<  */
    pthread_cond_t conditionVar; /*!<  */
    /*!
     *  \brief
     *
     *
     */
    pthread_t createInterceptPacketThread();
    /*!
     *  \brief
     *
     *
     */
    static void* threadEntryPoint(void *object);
#else
    bool readThread; /*!<  */
    mutable SystemMutex pendingReadMutex; /*!<  */
    bool threadStop; /*!<  */
#endif
  };

} /* namespace ns3 */

#endif /* TETRYS_PROTOCOL_H_ */
