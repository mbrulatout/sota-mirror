#include "tetrys-sink-flow.h"
#include <sstream>

using std::ostringstream;
namespace ns3 {

TetrysSinkFlow::TetrysSinkFlow(Ptr<Node> src_node, Ptr<Node> dst_node,
                               uint16_t flowID, uint32_t flowSize,
                               Time start, Time ACKFreq) {
    //NS2
    this->numPacketReceived = 0;
    this->nbUsefulRepairReceived = 0;
    this->maxSeqnumNoLoss = 0;
    this->maxDataSeen = 0;
    this->repairListItems = 0;
    this->lastDataReceived = -1;

    this->blockIndexFEC = 0;
    this->lastBlockFEC = -1;
    for (int i = 0; i < FEC_MAX_BLOCK; i++) {
        this->fecBlock[i].k = 0;
        this->fecBlock[i].n = 0;
        this->fecBlock[i].redCount = 0;
        this->fecBlock[i].dataCount = 0;
        this->fecBlock[i].maxSeq = 0;
        this->fecBlock[i].minSeq = 0;
        this->fecBlock[i].blockId = 0;
    }
    this->dataListSize = MAX_DATA;
    this->repairListSize = MAX_REPAIR;

    //MEM
    this->dataList = (int*) new (std::nothrow) int [dataListSize];
    this->repairList = (int*) new (std::nothrow) int [repairListSize];
    if (NULL == dataList || NULL == repairList)
        NS_FATAL_ERROR("TetrysSinkFlow::TetrysSinkFlow(params) FAIL ALLOC");
    memset(this->dataList, '\0', sizeof (int) *dataListSize);
    memset(this->repairList, '\0', sizeof (int) *repairListSize);

    //NS3
    this->srcNode = src_node;
    this->dstNode = dst_node;
    this->flowId = flowID;
    this->flowFinished = false;
    this->timeStart = start;
    this->timeEnd = start;
    this->ackInterval = ACKFreq;
    this->packetsReceived = 0;
    this->totalLost = 0;
    this->sentAck = 0;
    this->lastSeqno = 0;
    this->meanError = 0.0;
    this->alphaError = ALPHA_VALUE;
    this->repairUseless = 0;
    this->maxRepairSeen = 0;
    this->flowSize = flowSize;
    this->lastFlowPacket = -1;

    //SCHEDULING
    this->sendEvent = Simulator::Schedule(start + ackInterval, &TetrysSinkFlow::send, this);

    //SOCKET
    this->flowSocket = Socket::CreateSocket(this->GetSrcNode(), TypeId::LookupByName("ns3::Ipv4RawSocketFactory"));
    if (flowSocket->Bind() == -1)
        NS_FATAL_ERROR("Sink : Cannot bind.");
    this->flowSocket->SetAttribute("IpHeaderInclude", BooleanValue(true));
}

TetrysSinkFlow::~TetrysSinkFlow() {
    delete[] dataList;
    delete[] repairList;
    this->flowSocket = NULL;
}

TetrysSinkFlow& TetrysSinkFlow::operator=(const TetrysSinkFlow& src) {
    NS_LOG_UNCOND("!!!!!!!!!!!!! SINK OPERATOR = ");
    //NS2
    this->numPacketReceived = src.numPacketReceived;
    this->nbUsefulRepairReceived = src.nbUsefulRepairReceived;
    this->maxSeqnumNoLoss = src.maxSeqnumNoLoss;
    this->maxDataSeen = src.maxDataSeen;
    this->repairListItems = src.repairListItems;
    this->lastDataReceived = src.lastDataReceived;

    this->blockIndexFEC = src.blockIndexFEC;
    this->lastBlockFEC = src.lastBlockFEC;
    for (int i = 0; i < FEC_MAX_BLOCK; i++) {
        this->fecBlock[i].k = src.fecBlock[i].k;
        this->fecBlock[i].n = src.fecBlock[i].n;
        this->fecBlock[i].redCount = src.fecBlock[i].redCount;
        this->fecBlock[i].dataCount = src.fecBlock[i].dataCount;
        this->fecBlock[i].maxSeq = src.fecBlock[i].maxSeq;
        this->fecBlock[i].minSeq = src.fecBlock[i].minSeq;
        this->fecBlock[i].blockId = src.fecBlock[i].blockId;
    }
    this->dataListSize = src.dataListSize;
    this->repairListSize = src.repairListSize;

    //MEM
    this->dataList = new (std::nothrow) int [dataListSize];
    this->repairList = new (std::nothrow) int [repairListSize];
    if (NULL == dataList || NULL == repairList)
        NS_FATAL_ERROR("TetrysSinkFlow::operator= FAIL ALLOC");
    memcpy(this->dataList, src.dataList, sizeof (int) * dataListSize);
    memcpy(this->repairList, src.repairList, sizeof (int) * repairListSize);

    //NS3
    this->srcNode = src.srcNode;
    this->dstNode = src.dstNode;
    this->flowId = src.flowId;
    this->flowFinished = src.flowFinished;
    this->timeStart = src.timeStart;
    this->timeEnd = src.timeEnd;
    this->ackInterval = src.ackInterval;
    this->packetsReceived = src.packetsReceived;
    this->totalLost = src.totalLost;
    this->sentAck = src.sentAck;
    this->lastSeqno = src.lastSeqno;
    this->meanError = src.meanError;
    this->alphaError = src.alphaError;
    this->repairUseless = src.repairUseless;
    this->maxRepairSeen = maxRepairSeen;
    this->flowSize = src.flowSize;
    this->lastFlowPacket = src.lastFlowPacket;

    //SCHEDULING
    Simulator::Remove(src.sendEvent);
    this->sendEvent = Simulator::Schedule(this->timeStart + this->ackInterval, &TetrysSinkFlow::send, this);

    //SOCKET
    this->flowSocket = Socket::CreateSocket(this->GetSrcNode(), TypeId::LookupByName("ns3::Ipv4RawSocketFactory"));
    if (flowSocket->Bind() == -1)
        NS_FATAL_ERROR("Source : Cannot bind.");
    this->flowSocket->SetAttribute("IpHeaderInclude", BooleanValue(true));

    return *this;
}

TetrysSinkFlow::TetrysSinkFlow(const TetrysSinkFlow& src) {
    //NS2
    this->numPacketReceived = src.numPacketReceived;
    this->nbUsefulRepairReceived = src.nbUsefulRepairReceived;
    this->maxSeqnumNoLoss = src.maxSeqnumNoLoss;
    this->maxDataSeen = src.maxDataSeen;
    this->repairListItems = src.repairListItems;
    this->lastDataReceived = src.lastDataReceived;

    this->blockIndexFEC = src.blockIndexFEC;
    this->lastBlockFEC = src.lastBlockFEC;
    for (int i = 0; i < FEC_MAX_BLOCK; i++) {
        this->fecBlock[i].k = src.fecBlock[i].k;
        this->fecBlock[i].n = src.fecBlock[i].n;
        this->fecBlock[i].redCount = src.fecBlock[i].redCount;
        this->fecBlock[i].dataCount = src.fecBlock[i].dataCount;
        this->fecBlock[i].maxSeq = src.fecBlock[i].maxSeq;
        this->fecBlock[i].minSeq = src.fecBlock[i].minSeq;
        this->fecBlock[i].blockId = src.fecBlock[i].blockId;
    }
    this->dataListSize = src.dataListSize;
    this->repairListSize = src.repairListSize;

    //MEM
    this->dataList = new (std::nothrow) int [dataListSize];
    this->repairList = new (std::nothrow) int [repairListSize];
    if (NULL == dataList || NULL == repairList)
        NS_FATAL_ERROR("TetrysSinkFlow::TetrysSinkFlow(const&) FAIL ALLOC");
    memcpy(this->dataList, src.dataList, sizeof (int) * dataListSize);
    memcpy(this->repairList, src.repairList, sizeof (int) * repairListSize);

    //NS3
    this->srcNode = src.srcNode;
    this->dstNode = src.dstNode;
    this->flowId = src.flowId;
    this->flowFinished = src.flowFinished;
    this->timeStart = src.timeStart;
    this->timeEnd = src.timeEnd;
    this->ackInterval = src.ackInterval;
    this->packetsReceived = src.packetsReceived;
    this->totalLost = src.totalLost;
    this->sentAck = src.sentAck;
    this->lastSeqno = src.lastSeqno;
    this->meanError = src.meanError;
    this->alphaError = src.alphaError;
    this->repairUseless = src.repairUseless;
    this->maxRepairSeen = maxRepairSeen;
    this->flowSize = src.flowSize;
    this->lastFlowPacket = src.lastFlowPacket;
    
    //SCHEDULING
    Simulator::Remove(src.sendEvent);
    this->sendEvent = Simulator::Schedule(this->timeStart + this->ackInterval, &TetrysSinkFlow::send, this);

    //SOCKET
    this->flowSocket = Socket::CreateSocket(this->GetSrcNode(), TypeId::LookupByName("ns3::Ipv4RawSocketFactory"));
    if (flowSocket->Bind() == -1)
        NS_FATAL_ERROR("Sink : Cannot bind.");
    this->flowSocket->SetAttribute("IpHeaderInclude", BooleanValue(true));
}

void TetrysSinkFlow::enlargeDataList() {
    int temp_size = this->dataListSize;
    int *temp_list = new (std::nothrow) int [temp_size];
    if (NULL == temp_list)
        NS_FATAL_ERROR("TetrysSinkFlow::enlarge_data_list FAIL ALLOC 1");
    memcpy(temp_list, this->dataList, sizeof (int) * this->dataListSize);
    this->dataListSize += ADJUST_SIZE;
    delete[] this->dataList;
    this->dataList = (int *) new int [this->dataListSize];
    if (NULL == this->dataList)
        NS_FATAL_ERROR("TetrysSinkFlow::enlarge_data_list FAIL ALLOC 2");
    memset(this->dataList + temp_size, '\0', sizeof (int) * (this->dataListSize - temp_size));
    memcpy(this->dataList, temp_list, sizeof (int) * temp_size);
    delete[] temp_list;
}

bool is_old(const pair<double, int>& p) {
    return p.first + ERROR_WINDOW_TIME < Simulator::Now().GetSeconds();
}
bool beginning = 1;

void TetrysSinkFlow::computeErrorRate() {
    this->packetListError.remove_if(is_old);
    double windowError;
    //Nothing has been received (latency is too high compared to ack frequency)
    (this->lastSeqno == 0) ? windowError = 0.0 : windowError = 1.0 - (1.0 * this->packetListError.size()) / (this->packetListError.back().second + 1 - this->packetListError.begin()->second);
    if (beginning) { // First value
        this->meanError = windowError;
        beginning = 0;
    } else
        this->meanError = this->meanError + this->alphaError * (windowError - this->meanError);
}

void TetrysSinkFlow::send() {
    NS_ASSERT(this->sendEvent.IsExpired());
    Ptr<Packet>p = Create<Packet>(0); //Empty packet
    computeErrorRate();
    TetrysHdr tetrysHeader;
    if(TETRYS_TYPE == TETRYS_ADAPTATIVE || TETRYS_TYPE == TETRYS_MOBILE_ADAPTATIVE)
      tetrysHeader.createAckHeader(this->maxSeqnumNoLoss, this->flowId, this->meanError,0);
    else
      tetrysHeader.createAckHeader(this->maxSeqnumNoLoss, this->flowId, 0.0,0);
    p->AddHeader(tetrysHeader);
    Ipv4Header ipHeader;
    ipHeader.SetProtocol(TetrysUtils::PROT_NUMBER);
    ipHeader.SetSource(GetDstNode()->GetObject<Ipv4L3Protocol > ()->GetAddress(1, 0).GetLocal());
    ipHeader.SetDestination(GetSrcNode()->GetObject<Ipv4L3Protocol > ()->GetAddress(1, 0).GetLocal());
    ipHeader.SetPayloadSize(tetrysHeader.GetSerializedSize()); // IP payload is only tetrys header size
    p->AddHeader(ipHeader);
    this->sentAck++;
    if (this->flowSocket->Send(p, 0) < 0)
        NS_FATAL_ERROR(Simulator::Now().GetSeconds() << " Failed to send DATA Raw packet (error 1)");
    if (!IsFinished())
        this->sendEvent = Simulator::Schedule(this->ackInterval, &TetrysSinkFlow::send, this);
}

void TetrysSinkFlow::printLists() {
    //    for (int i = 0; i < dataListSize; i++)
    //        if (dataList[i] != 0)
    //            NS_LOG_UNCOND(" i : " << i << " data list = " << dataList[i]);
    for (int i = 0; i < repairListSize; i++)

        if (repairList[ i] != 0)
            NS_LOG_UNCOND(" i : " << i << " repair list = " << repairList[i] << "..elements :" << elementsInList(repairList, repairListSize));
}

void TetrysSinkFlow::receive(Ptr<Packet> p) {
    if (!IsFinished()) {
        Ptr<Packet> pkt = p->Copy();
        Ipv4Header ipHeader;
        pkt->RemoveHeader(ipHeader);
        TetrysHdr tetrysHeader;
        pkt->RemoveHeader(tetrysHeader);
        /*
         * ***********************************
         * ********** TETRYS mode ************
         * ***********************************
         */
        //        tetrysHeader.Print();
        if (tetrysHeader.getType() == TETRYS_DATA || tetrysHeader.getType() == TETRYS_REPAIR) {
	  
	  //Check end of transmission
	  if(tetrysHeader.getType() == TETRYS_DATA && tetrysHeader.getFlag() == 1 )
	    this->lastFlowPacket = tetrysHeader.getNum();
	  if(tetrysHeader.getType() == TETRYS_REPAIR && tetrysHeader.getFlag() == 1 )
	    this->lastFlowPacket = tetrysHeader.getRedInfo(1);
	  
	  
	  //Error
            this->packetListError.push_back(make_pair(Simulator::Now().GetSeconds(), tetrysHeader.getSeqNum()));
            this->totalLost += tetrysHeader.getSeqNum() - this->lastSeqno - 1;
            //Stats
            this->packetsReceived++;
            this->lastSeqno = tetrysHeader.getSeqNum();

            if ((tetrysHeader.getType() == TETRYS_DATA) && !isPacketInList(dataList, dataListSize, tetrysHeader.getNum(), 0)) {
                //If data_list is full, resize it
                if (dataList[ dataListSize - 1] != 0)
                    enlargeDataList();

                maxDataSeen = MAX(maxDataSeen, tetrysHeader.getNum());
                lastDataReceived = MAX(lastDataReceived, maxSeqnumNoLoss);

                if (abs(tetrysHeader.getNum() - lastDataReceived) != 1) { //lost packet
                    int min_i = MIN(tetrysHeader.getNum(), lastDataReceived);
                    int max_i = MAX(tetrysHeader.getNum(), lastDataReceived);
                    for (int i = min_i + 1; i < max_i; i++)
                        addToRepairList(i);
                }
		
                lastDataReceived = tetrysHeader.getNum();
                numPacketReceived++;

                //Packet was missing !
                if (isPacketInList(repairList, repairListSize, tetrysHeader.getNum(), 1))
                    TetrysUtils::sortArray(repairList, repairListSize);

                //DECODE AFTER RECEIVING A DATA PACKET (ONLY IF NOTHING DONE WHEN REPAIR RECEIVED .....)
                repairListItems = elementsInList(repairList, repairListSize);
                //Try to decode

                //                //TEMMMMMPPPPPPPPPPPPPPPPPPPPPPPPP
                //                                if ((repairListItems <= nbUsefulRepairReceived) && (nbUsefulRepairReceived != 0)) {
                //                                    //Decode all repair_list
                //                                    for (int i = 0; i < repairListSize; i++) {
                //                                        if (repairList[i] != 0) {
                //                                            numPacketReceived++;
                //                                            maxDataSeen = MAX(maxDataSeen, repairList[i]);
                //                                            NS_LOG_UNCOND("\t <<<<<<<<<<<<<<< DECODING DATA " << repairList[i]
                //                                                          << "TODO ADD PAYLOAD TO BUFFER");
                //                                            addToDataList(repairList[i]);
                //                                            //If data_list is full, resize it
                //                                            if (dataList[dataListSize - 1] != 0)
                //                                                enlargeDataList();
                //                                            repairList[i] = 0;
                //                                        }
                //                                    }
                //                //TEMMMMMPPPPPPPPPPPPPPPPPPPPPPPPP
                //
                //                nbUsefulRepairReceived = 0;
                //                repairListItems = 0;
                //            }//end decode
            }

            //Process repair packet
            if (tetrysHeader.getType() == TETRYS_REPAIR) {
                if (isRepairUseful(tetrysHeader.getRedArray())) {
                    //If repair pkt is useful, process it, do nothing otherwise
                    //NS_LOG_UNCOND("repair useful");
                    nbUsefulRepairReceived++;
                    //num_repair_rcv++;
                    //max_repair_seq_num_seen = MAX(max_repair_seq_num_seen, tetrysHeader.getRedInfo(1));


                    processTetrysHeader(tetrysHeader.getRedArray());

                    //WINDOW DECODING
                    if (TETRYS_TYPE == TETRYS_MOBILE || TETRYS_TYPE == TETRYS_MOBILE_ADAPTATIVE) {
                        // repair packet containing only the last data packet, thus ending the session.
                        if (tetrysHeader.getRedInfo(0) == tetrysHeader.getRedInfo(1))
                            maxSeqnumNoLoss = tetrysHeader.getRedInfo(0);

                        maxRepairSeen = MAX(maxRepairSeen, tetrysHeader.getRedInfo(1));
                        int lost_in_block = elementsInBlockList(repairList, repairListSize, maxRepairSeen);
                        repairListItems = elementsInList(repairList, repairListSize);
                        //                        NS_LOG_UNCOND("MAX REPAIR SEEN = " << maxRepairSeen << " lost in block = " << lost_in_block << " REPAIR RECEIVED : " << nbUsefulRepairReceived);
                        if (lost_in_block <= nbUsefulRepairReceived && lost_in_block > 0) {

                            //                            NS_LOG_UNCOND("Missing in Block=" << lost_in_block << " AND repair received="
                            //                                          << nbUsefulRepairReceived << " AND TOTAL MISSING=" << repairListItems);

                            //                            NS_LOG_UNCOND("REPAIR MATRIX BLOCK SIZE : " << lost_in_block << " total lost " << totalLost);
                            NS_LOG_UNCOND((Simulator::Now() - this->timeStart).GetSeconds() << " : REPAIR MATRIX : \t " << lost_in_block << " : List size " << repairListSize);
                            //                            NS_LOG_UNCOND("Can DECODE BLOCK UNTIL  " << maxRepairSeen);


                            for (int i = 0; i < repairListSize; i++) {
                                if (repairList[i] != 0 && repairList[i] <= maxRepairSeen) {
                                    numPacketReceived++;
                                    maxDataSeen = MAX(maxDataSeen, repairList[i]);
                                    addToDataList(repairList[i]);
                                    if (dataList[dataListSize - 1] != 0)
                                        enlargeDataList();
                                    repairList[i] = 0;
                                }
                            }


                            //lost_in_block = elementsInBlockList(repairList, repairListSize, maxRepairSeen);
                            //repairListItems = elementsInList(repairList, repairListSize);
                            //                            NS_LOG_UNCOND("END OF DECODING : Missing in Block (0?) " <<
                            //                                          lost_in_block << " AND TOTAL MISSING" << repairListItems);
                            nbUsefulRepairReceived = 0;
                        }
                    }//CLASSIC DECODING
                    else {
                        repairListItems = elementsInList(repairList, repairListSize);
                        if ((repairListItems <= nbUsefulRepairReceived) && (repairListItems != 0)) {
                            NS_LOG_UNCOND((Simulator::Now() - this->timeStart).GetSeconds() << " : REPAIR MATRIX :   " << repairListItems << " : List size " << repairListSize);
                            //Can decode all data
                            for (int i = 0; i < repairListSize; i++) {
                                if (repairList[i] != 0) {
                                    numPacketReceived++;
                                    maxDataSeen = MAX(maxDataSeen, repairList[i]);
                                    //NS_LOG_UNCOND(max_data_seqnum_seen << " " << repair_list[i] << " " << max_seq_num_no_loss);
                                    //NS_LOG_UNCOND("\t <<<<<<<<<<<<<<< REPAIR DATA " << repair_list[i]);
                                    addToDataList(repairList[i]);
                                    repairList[i] = 0;
                                    if (dataList[dataListSize - 1] != 0)
                                        enlargeDataList();
                                }
                            }
                            nbUsefulRepairReceived = 0;
                            repairListItems = 0;
                        } //end repair
                    } //Decoding (block or classic)
                    TetrysUtils::sortArray(repairList, repairListSize);
                }//end useful
                else
                    this->repairUseless++;
            }
            //whether it's a TETRYS_REPAIR or a TETRYS_DATA
            TetrysUtils::sortArray(dataList, dataListSize);
            updateMaxSeqnumNoLoss();
            //END OF RECEPTION
	    //Old condition without flag
            //if (this->maxSeqnumNoLoss * PACKET_SIZE >= this->flowSize) {
	    if( this->maxSeqnumNoLoss == this->lastFlowPacket) {
	      //NS_LOG_UNCOND("maxseqnoloss " << maxSeqnumNoLoss << " annnnd " << lastFlowPacket);
	      this->timeEnd = Simulator::Now();
                NS_LOG_UNCOND("\n");
                NS_LOG_UNCOND("STAT : ACK (sent) : " << this->sentAck);
                NS_LOG_UNCOND("STAT : TOTAL SENT : " << this->lastSeqno);
                NS_LOG_UNCOND("STAT : TOTAL LOSS : " << this->totalLost);
                NS_LOG_UNCOND("STAT : RECEIVED (sent - lost) : " << this->packetsReceived);
                NS_LOG_UNCOND("STAT : PKT NUMBER (ack + received): " << this->packetsReceived + this->sentAck);
                NS_LOG_UNCOND("STAT : TOTAL PKT (sent + ack) : " << this->lastSeqno + this->sentAck);
                NS_LOG_UNCOND("STAT : USELESS CODED PACKETS : " << this->repairUseless);
                NS_LOG_UNCOND("STAT : OVERHEAD RECEIVED PACKETS (useless / received) : " <<
                              round(1000.0 * this->repairUseless / this->packetsReceived) / 10 << " %");
		NS_LOG_UNCOND("STAT : OVERHEAD PAPER ( (useless + ack)/received ) : " <<
			      round(1000.0 * (this->repairUseless + this->sentAck) / this->packetsReceived) / 10 << " %");
                NS_LOG_UNCOND("STAT : REAL LOSS RATIO : " << 100.0 * this->totalLost / this->lastSeqno << "\n\n");
                NS_LOG_UNCOND("Sink has finished ! Execution time :"
                              << this->timeEnd.GetSeconds() - this->timeStart.GetSeconds());
                NS_LOG_UNCOND("");
                this->flowFinished = true;
            }
        }
    }
}

int TetrysSinkFlow::elementsInBlockList(int *list, int size, int maxDecode) {
    int nb = 0;
    for (int i = 0; i < size; i++)

        if (list[i] != 0 && list[i] <= maxDecode)
            nb++;
    return nb;
}

void TetrysSinkFlow::processTetrysHeader(uint16_t * hdr_red_array) { //only if repair useful

    int flag;
    int temp_size = 0;
    int temp_list_processing_size = hdr_red_array[1] - hdr_red_array[0] + 1;
    int* temp_list_processing = new (std::nothrow) int [temp_list_processing_size];
    if (NULL == temp_list_processing)
        NS_FATAL_ERROR("TetrysSinkFlow::processing_tetrys_header FAIL ALLOC");
    memset(temp_list_processing, '\0', sizeof (int) * temp_list_processing_size);
    //Copy the items in Tetrys header which are not in data list to temp_list_processing
    for (int i = hdr_red_array[0]; i <= hdr_red_array[1]; i++) {
        flag = 0;
        if (i > maxSeqnumNoLoss) {
            flag = TetrysUtils::dico(dataList, 0, dataListSize - 1, i);
            if (flag == 0)
                temp_list_processing[temp_size++] = i;

        }
    }
    //Clear the items in temp_list_processing which are in repair_list
    //temp_list_processing has temp_size elements !!!
    for (int i = 0; i < temp_size; i++)
        for (int j = 0; j < repairListSize; j++) {
            if (repairList[j] == 0)
                break;
            if ((repairList[j] == temp_list_processing[i]))
                temp_list_processing[i] = 0;
        }

    //Check whether the new items added to repair list exceeds the current size.
    //If yes, increase the size
    int temp = elementsInList(repairList, repairListSize);
    int temp1 = elementsInList(temp_list_processing, temp_list_processing_size);

    if (temp + temp1 >= repairListSize) {
        //Increase repair list size
        int old_repair_size = repairListSize;
        int *temp_list1;
        temp_list1 = new (std::nothrow) int [old_repair_size];
        if (NULL == temp_list1)
            NS_FATAL_ERROR("TetrysSinkFlow::processing_header FAIL ALLOC 2");
        //memset(temp_list1, '\0', sizeof (int) *old_repair_size);
        memcpy(temp_list1, repairList, sizeof (int) *old_repair_size);

        repairListSize = temp + temp1 + ADJUST_SIZE;
        repairList = new (std::nothrow) int [repairListSize];
        if (NULL == repairList)
            NS_FATAL_ERROR("TetrysSinkFlow::processing_header FAIL ALLOC 3");
        memset(repairList + old_repair_size, '\0', sizeof (int) * (repairListSize - old_repair_size));
        memcpy(repairList, temp_list1, sizeof (int) *old_repair_size);

        delete[] temp_list1;
    }

    //Add new items from header to repair list (which are not already there nor in data_list)
    int mid = 0; //optimization
    for (int i = 0; i < repairListSize; i++)
        if (repairList[i] == 0)
            for (int j = mid; j < temp_list_processing_size; j++)
                if (temp_list_processing[j] != 0) {
                    repairList[i] = temp_list_processing[j]; //Re-creates repairList from repair packets
                    mid = j + 1;
                    break;
                }

    delete[] temp_list_processing;
}

void TetrysSinkFlow::addToDataList(int seq_num) { //after decoding
    if (seq_num <= maxSeqnumNoLoss) // Do nothing if already acked
        return;
    for (int i = 0; i < dataListSize; i++) {
        if (dataList[i] == 0) {
	  NS_LOG_UNCOND(Simulator::Now().GetSeconds() << "\t : LOG RECEIVE PACKET : " << seq_num);
            dataList[i] = seq_num;
            break;
        }
    }
}

void TetrysSinkFlow::addToRepairList(int seq_num) {
    for (int i = 0; i < repairListSize; i++) {
        if (seq_num == repairList[i])
            return; //do nothing if already in the repair list
        if (repairList[i] == 0) {
            repairList[i] = seq_num;
            break;
        }
    }
}

int TetrysSinkFlow::elementsInList(int *list, int size) {
    int nb = 0;
    for (int i = 0; i < size; i++)

        if (list[i] != 0)
            nb++;
    return nb;
}

//UNTOUCHED SHOULD BE OK
//Return 0 if all members in header are already in data list, 1 otherwise.
//Value 1 means the REPAIR pkt is useful

bool TetrysSinkFlow::isRepairUseful(uint16_t * src_list) {
    //Compare header with data_list.
    int count = 0, middle = 0;
    if (src_list[1] <= maxSeqnumNoLoss)
        return 0;
    for (int i = src_list[0]; i <= src_list[1]; i++) {
        if (i <= maxSeqnumNoLoss) {
            count++;
            continue;
        }
        for (int j = middle; j < dataListSize; j++) {
            if (dataList[j] == i) {

                count++;
                middle = j;
                break;
            }
        }
    }
    return !(count == (src_list[1] - src_list[0] + 1));
}


//1 = found , 0 = not found

bool TetrysSinkFlow::isPacketInList(int *list, int size, int seq_num, int action) {
    if (action == 0) { //data_list
        if (seq_num <= maxSeqnumNoLoss) //Already ack something greater...
            return 1;
        for (int i = 0; i < size; i++) {
            if (list[i] == seq_num)
                return 1;
            if (list[i] == 0) { //add packet if not present
                list[i] = seq_num;
		NS_LOG_UNCOND(Simulator::Now().GetSeconds() << "\t : LOG RECEIVE PACKET NOREPAIR : " << seq_num);
                break;
            }

        }
        return 0;
    }
    if (action == 1) { //repair_list
        for (int i = 0; i < size; i++)
            if (list[i] == seq_num) {

                list[i] = 0; //Remove packet from repair_list if received corresponding DATA packet received
                return 1;
            }
        return 0;
    }
    return 0; //never
}

void TetrysSinkFlow::updateMaxSeqnumNoLoss() {
    int i;
    for (i = 0; i < dataListSize; i++) {
        if ((i != 0) && (dataList[i] == 0))
            break;
        if (dataList[i] <= maxSeqnumNoLoss) {
            dataList[i] = 0;
        } else {
            if (dataList[i] == maxSeqnumNoLoss + 1) {
                maxSeqnumNoLoss++;
                dataList[i] = 0;
            } else
                break;
        }
    }
    if (i == 0)
        return;
    for (int j = 0; j < dataListSize; j++) {
        if (dataList[i] == 0)
            break;
        else {
            dataList[j] = dataList[i];
            dataList[i] = 0;
            i++;

            if (i == dataListSize)
                break;
        }
    }
}

/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////// UTILITIES //////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////


/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
////////////////////// FEC //////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////

void TetrysSinkFlow::removeItemsDataListFEC(int amount_to_remove) {

    memcpy(dataList, dataList + amount_to_remove, sizeof (int) * (dataListSize - amount_to_remove));
    memset(dataList + (dataListSize - amount_to_remove), '\0', sizeof (int) * amount_to_remove);
}

void TetrysSinkFlow::pocessBlockFEC(Ptr<Packet>pkt) {
    Ptr<Packet> pcopy = pkt->Copy();
    Ipv4Header ipHeader;
    pcopy->RemoveHeader(ipHeader);
    TetrysHdr tetrysHeader;
    pcopy->RemoveHeader(tetrysHeader);
    NS_LOG_UNCOND("Block processing " << tetrysHeader.getRedInfo(0) << " " << tetrysHeader.getRedInfo(1) << " "
                  << tetrysHeader.getRedInfo(2) << " " << tetrysHeader.getRedInfo(3) << " " << tetrysHeader.getRedInfo(4));
    blockIndexFEC = -1;
    for (int i = 0; i < FEC_MAX_BLOCK; i++) {
        if (fecBlock[i].blockId == tetrysHeader.getRedInfo(2)) {
            blockIndexFEC = i;
            break;
        }
    }
    if (blockIndexFEC == -1) {
        blockIndexFEC = ++lastBlockFEC;
        fecBlock[blockIndexFEC].redCount = 0;
    }

    if (lastBlockFEC == FEC_MAX_BLOCK - 1)
        lastBlockFEC = -1;

    fecBlock[blockIndexFEC].k = tetrysHeader.getRedInfo(0);
    fecBlock[blockIndexFEC].n = tetrysHeader.getRedInfo(1);
    fecBlock[blockIndexFEC].blockId = tetrysHeader.getRedInfo(2);
    fecBlock[blockIndexFEC].minSeq = tetrysHeader.getRedInfo(3);
    fecBlock[blockIndexFEC].maxSeq = tetrysHeader.getRedInfo(4);
}

void TetrysSinkFlow::tryDecodeFEC(int index) {

    Time now = Simulator::Now();
    //How many items in data_list are between min and max
    fecBlock[index].dataCount = checkDataListFEC(fecBlock[index].minSeq, fecBlock[index].maxSeq);

    if ((fecBlock[index].redCount + fecBlock[index].dataCount >= fecBlock[index].k)) {
        NS_LOG_UNCOND("Can decode block" << index);
        recoverPacketsInBlockFEC(fecBlock[index].minSeq, fecBlock[index].maxSeq);
        for (int i = 0; i < FEC_MAX_BLOCK_SIZE; i++) {
            if (recoverListFEC[i] != 0) {
                NS_LOG_UNCOND(now.GetSeconds() << "SINK FEC DECODE RECOVER DATA " << recoverListFEC[i]);
                numPacketReceived++;
                maxDataSeen = MAX(maxDataSeen, recoverListFEC[i]);

                addToDataList(recoverListFEC[i]);
                recoverListFEC[i] = 0;
            } else
                break;
        }

    }
}

int TetrysSinkFlow::checkDataListFEC(int min_seq, int max_seq) {

    int counter = 0;

    if (min_seq == 0 && max_seq == 0)
        return 0;
    for (int i = 0; i < this->dataListSize; i++) {
        if (this->dataList[i] == 0)
            break;

        if (this->dataList[i] >= min_seq && this->dataList[i] <= max_seq)
            counter++;
    }
    return counter;
}

void TetrysSinkFlow::recoverPacketsInBlockFEC(int min_seq, int max_seq) {
    if (min_seq == 0 && max_seq == 0)
        return;

    int index = 0;
    int idx = 0;
    int found;

    int* tmp_list = new (std::nothrow) int[FEC_MAX_BLOCK_SIZE];
    if (NULL == tmp_list)
        NS_FATAL_ERROR("TetrysSinkFlow::fec_recover_pkts_in_block FAIL ALLOC");
    memset(tmp_list, '\0', sizeof (int) * FEC_MAX_BLOCK_SIZE);

    for (int i = 0; i < dataListSize; i++) {
        if (dataList[i] == 0)
            break;
        if (dataList[i] >= min_seq && dataList[i] <= max_seq)
            tmp_list[index++] = dataList[i];
    }

    memset(recoverListFEC, '\0', sizeof (int) * FEC_MAX_BLOCK_SIZE);

    for (int j = min_seq; j <= max_seq; j++) {
        found = 0;
        for (int k = 0; k < FEC_MAX_BLOCK_SIZE; k++) {
            if (j == tmp_list[k]) {
                found = 1;
                break;
            }
        }

        if (!found)
            recoverListFEC[idx++] = j;
    }
}

//        } else {
//            /*
//             * ***********************************
//             * ************ FEC mode *************
//             * ***********************************
//             */
//            if (tetrysHeader.getType() == FEC_DATA) {
//                if (!isPacketInList(dataList, dataListSize, tetrysHeader.getNum(), 0)) {
//                    NS_LOG_UNCOND("\t <<<<<<<<<<<<<<< FEC USEFUL DATA " << tetrysHeader.getNum());
//
//                    numPacketReceived++;
//                    maxDataSeen = MAX(maxDataSeen, tetrysHeader.getNum());
//
//                    if (dataList[dataListSize - FEC_MAX_BLOCK_SIZE] != 0) {
//                        //Remove first 100 items in data list
//                        NS_LOG_UNCOND("removing things from data_list !!!!");
//                        removeItemsDataListFEC(FEC_MAX_BLOCK_SIZE);
//                    }
//                    pocessBlockFEC(p);
//                    //                    if (fec_block_index >= 0)
//                    //                        fec_try_to_decode(fec_block_index);
//                }
//            }
//            if (tetrysHeader.getType() == FEC_REPAIR) {
//                this->packetsReceived++;
//                NS_LOG_UNCOND("\t <<<<<<<<<<<<<<< FEC REPAIR " << tetrysHeader.getNum() << " " << tetrysHeader.getSeqNum());
//                pocessBlockFEC(p);
//                fecBlock[blockIndexFEC].redCount++;
//            }
//
//            if (blockIndexFEC >= 0)
//                tryDecodeFEC(blockIndexFEC);
//        } //end FEC



//void TetrysSinkFlow::update_mean() {
//    float err = (float) loss_block / ERROR_BLOCK_SIZE;
//
//
//    if (curr_block == 0)
//        mean_err = err;
//
//    else
//        mean_err = mean_err + alpha * (err - mean_err);
//
//
//    loss_block = 0;
//    curr_block++;
//
//    NS_LOG_UNCOND((Simulator::Now() - this->timeStart).GetSeconds() << "\t ERROR ESTIMATION : " << mean_err * 100);
//    //Smoothing coefficient
//
//}

//
//void TetrysSinkFlow::update_error_rate(int seqnum) {
//
//
//    if (seqnum % ERROR_BLOCK_SIZE != 0 && seqnum / ERROR_BLOCK_SIZE == curr_block && this->last_seq_num + 1 != seqnum) {
//        //Loss in current block
//        int consecutive_loss = seqnum - this->last_seq_num - 1;
//        this->total_lost += consecutive_loss;
//        for (int i = 1; i <= consecutive_loss; i++) { //iterate through all lost packets
//            loss_block++;
//            //            NS_LOG_UNCOND("Handling loss of packet " << this->last_seq_num + i
//            //                          << " block number " << curr_block << " lost in block :" << loss_block);
//        }
//    } else if (seqnum / ERROR_BLOCK_SIZE != curr_block && this->last_seq_num + 1 != seqnum) {
//        int consecutive_loss = seqnum - this->last_seq_num - 1;
//        this->total_lost += consecutive_loss;
//        for (int i = 1; i <= consecutive_loss; i++) { //iterate through all lost packets
//            if ((this->last_seq_num + i) % ERROR_BLOCK_SIZE != 0) { //Error in current block
//                loss_block++;
//                //                NS_LOG_UNCOND("Handling loss of packet " << this->last_seq_num + i
//                //                              << " block number " << curr_block << " lost in block :" << loss_block);
//
//            } else { //block ending with an error, starting a new one
//                loss_block++;
//                //                NS_LOG_UNCOND("Handling loss of packet " << this->last_seq_num + i
//                //                              << " block number " << curr_block << " lost in block :" << loss_block);
//                update_mean();
//            }
//        }
//        if (seqnum % ERROR_BLOCK_SIZE == 0) { //end of a block
//            update_mean();
//        }
//    } else if (seqnum % ERROR_BLOCK_SIZE == 0 && this->last_seq_num + 1 == seqnum) //new block and no loss
//        update_mean();
//
//    else
//        return; //nothing to do
//}


//
//void TetrysSinkFlow::q_sort(int *numbers, int left, int right) {
//    int pivot, l_hold, r_hold;
//    l_hold = left;
//    r_hold = right;
//    pivot = numbers[left];
//    while (left < right) {
//        while ((numbers[right] >= pivot) && (left < right))
//            right--;
//        if (left != right) {
//            numbers[left] = numbers[right];
//            left++;
//        }
//        while ((numbers[left] <= pivot) && (left < right))
//            left++;
//        if (left != right) {
//            numbers[right] = numbers[left];
//            right--;
//        }
//    }
//    numbers[left] = pivot;
//    pivot = left;
//    left = l_hold;
//    right = r_hold;
//    if (left < pivot)
//        q_sort(numbers, left, pivot - 1);
//
//    if (right > pivot)
//        q_sort(numbers, pivot + 1, right);
//}
}
