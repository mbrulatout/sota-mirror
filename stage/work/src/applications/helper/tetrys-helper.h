//============================================================================
// Name        : tetrys-helper.h
// Author      : Mathias Brulatout
// Date        : April 2014
// Version     : v0.1
// Copyright   : Copyright (c) 2014 Thales Inc. All Rights Reserved.
// Description :
//============================================================================

#ifndef TETRYS_HELPER_H_
#define TETRYS_HELPER_H_

#include <stdint.h>
#include "ns3/application-container.h"
#include "ns3/node-container.h"
#include "ns3/object-factory.h"
#include "ns3/ipv4-address.h"

namespace ns3 {

/**
 * \brief Create a server application which waits for input udp packets
 *        and sends them back to the original sender.
 */
class TetrysHelper {
public:
	/**
	 * Create UdpEchoServerHelper which will make life easier for 
         * people trying to set up simulations with echos.
	 */
	TetrysHelper();
	virtual ~TetrysHelper();

	/**
	 * Record an attribute to be set in each Application
         *  after it is is created.
	 * \param name the name of the attribute to set
	 * \param value the value of the attribute to set
	 */
	void SetAttribute(std::string name, const AttributeValue &value);

	/**
	 * Create a UdpEchoServerApplication on the specified Node.
	 *
	 * \param node The node on which to create the Application.
         * The node is specified by a Ptr<Node>.
	 *
	 * \returns An ApplicationContainer holding the Application created,
	 */
	ApplicationContainer Install(Ptr<Node> node) const;

	/**
	 * Create a UdpEchoServerApplication on specified node
	 *
	 * \param nodeName The node on which to create the application.
         *   The node is specified by a node name previously
         *  registered with the Object Name Service.
	 *
	 * \returns An ApplicationContainer holding the Application created.
	 */
	ApplicationContainer Install(std::string nodeName) const;

	/**
	 * \param c The nodes on which to create the Applications.  The nodes
	 *          are specified by a NodeContainer.
	 *
	 * Create one udp echo server application on each of the Nodes in the
	 * NodeContainer.
	 *
	 * \returns The applications created, one Application per Node in the
	 *          NodeContainer.
	 */
	ApplicationContainer Install(NodeContainer c) const;

private:
	/**
	 * \internal
	 */
	Ptr<Application> InstallPriv(Ptr<Node> node) const;

	ObjectFactory m_factory;
};
}
#endif /* TETRYS_HELPER_H_ */
