//============================================================================
// Name        : tetrys-helper.cc
// Author      : Mathias Brulatout
// Date        : April 2014
// Version     : v0.1
// Copyright   : Copyright (c) 2014 Thales Inc. All Rights Reserved.
// Description :
//============================================================================

#include "ns3/tetrys-helper.h"
#include "ns3/tetrys-protocol.h"

#include "ns3/names.h"

namespace ns3 {

TetrysHelper::TetrysHelper() {
	m_factory.SetTypeId("ns3::TetrysProtocol");
	//m_factory.Set ("Remote", Ipv4AddressValue (remote));
}

TetrysHelper::~TetrysHelper() {
	// TODO Auto-generated destructor stub
}

void TetrysHelper::SetAttribute(std::string name, const AttributeValue &value) {
	m_factory.Set(name, value);
}

ApplicationContainer TetrysHelper::Install(Ptr<Node> node) const {
	return ApplicationContainer(InstallPriv(node));
}

ApplicationContainer TetrysHelper::Install(std::string nodeName) const {
	Ptr<Node> node = Names::Find<Node>(nodeName);
	return ApplicationContainer(InstallPriv(node));
}

ApplicationContainer TetrysHelper::Install(NodeContainer c) const {
	ApplicationContainer apps;
	for (NodeContainer::Iterator i = c.Begin(); i != c.End(); ++i) {
		apps.Add(InstallPriv(*i));
	}

	return apps;
}

Ptr<Application> TetrysHelper::InstallPriv(Ptr<Node> node) const {
	Ptr<TetrysProtocol> app = m_factory.Create<TetrysProtocol>();
	node->AddApplication(app);

	return app;
}

} // namespace ns3
