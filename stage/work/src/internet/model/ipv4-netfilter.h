// vim: sw=2 cin cino=>4,n-2,{2,^-2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1 sw=2 ts=8 et:
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Texas at Dallas
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Qasim Javed <qasim@utdallas.edu>
 *         Adrian S. Tam <adrian.sw.tam@gmail.com>
 */

/* This is a major rework of the code by Qasim, almost a complete rewrite to
   simplify the structure. The outline is as follows:

   This is a Ipv4Netfilter class which defines the Netfilter function for IPv4
   protocol stack, i.e. to interact with Ipv4L3Protocol class. This class holds
   a bunch of filter chains, named by their role. They are namely,
   NF_INET_INPUT, NF_INET_FORWARD, NF_INET_OUTPUT, NF_INET_PREROUTING, and
   NF_INET_POSTROUTING.  These chains are mimicing their counterpart in Linux's
   Netfilter.

   The chains are an ordered list of "hooks" where each hook is a matcher-
   manipulator pair. The matcher identifies if the packet is valid to be
   manipulated by a hook and the manipulation is done by the manipulator.
   The first matching hook in the chain will have its manipulator invoked
   and the return value of the manipulator is taken as the return value of
   the netfilter. If no hook is matched, by default, the netfilter returns
   true without any modification to the packet.
 */
#ifndef IPV4_NETFILTER_H
#define IPV4_NETFILTER_H

#include "ns3/object.h"
#include "ipv4-netfilter-hook.h"
#include <vector>

namespace ns3 {

class Packet;
class NetDevice;

/* Types of Netfilter hooks */
typedef enum
{
  NF_INET_PREROUTING,
  NF_INET_INPUT,
  NF_INET_FORWARD,
  NF_INET_OUTPUT,
  NF_INET_POSTROUTING,
  NF_INET_NUMHOOKS
} Hook_t;

/**
  * \brief Implementation of netfilter
  *
  * This implements functionality similar to netfilter in the Linux Kernel.
  */

class Ipv4Netfilter : public Object
{
public:
  static TypeId GetTypeId (void);

  Ipv4Netfilter ();

  /**
   * Append the hook function at the specified chain.
   *
   * \param type The chain to be appended
   * \param hook The hook function to be appended to the chain
   * \returns 0 on success
   */
  uint32_t AppendNetfilterHook (Hook_t type, Ptr<Ipv4NetfilterHook> hook);

  /**
   * Remove the hook function from the specified chain.
   *
   * \param type The chain the hook is located
   * \param hook The hook to be removed
   * \returns 0 on success
   */
  uint32_t RemoveNetfilterHook (Hook_t type, Ptr<Ipv4NetfilterHook> hook);

  /**
   * Visit each hook to process the packet
   *
   * \param type The chain to visit
   * \param p    The packet to process
   * \param iph  IPv4 Header of the packet
   * \param in   Input net device
   * \param out  Output net device
   * \returns 1 on packet is valid to deliver by Ipv4L3Protocol, 0 if the packet is dropped
   */
  uint32_t ProcessHooks (Hook_t type, Ptr<Packet> p, Ipv4Header& iph, Ptr<NetDevice> in, Ptr<NetDevice> out);
private:
  std::vector<HooksChain> m_chains;
};

} // Namespace ns3
#endif /* IPV4_NETFILTER_H */
