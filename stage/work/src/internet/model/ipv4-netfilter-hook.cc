// vim: sw=2 cin cino=>4,n-2,{2,^-2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1 sw=2 ts=8 et:
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Texas at Dallas
 * Copyright (c) 2009 New York University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Qasim Javed <qasim@utdallas.edu>
 *         Adrian S. Tam <adrian.sw.tam@gmail.com>
 */

#include "ns3/packet.h"
#include "ipv4-netfilter-hook.h"
#include "ipv4-netfilter.h"

namespace ns3 {

TypeId
Ipv4NetfilterHook::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::Ipv4NetfilterHook")
    .SetParent<Object> ()
  ;
  return tid;
}

TypeId
Ipv4NetfilterHook::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

Ipv4NetfilterHook::Ipv4NetfilterHook ()
{
}

Ipv4NetfilterHook::~Ipv4NetfilterHook ()
{
}

void
Ipv4NetfilterHook::RegisterNetfilter (Ptr<Ipv4Netfilter> nf)
{
  m_netfilter = nf;
}

bool
Ipv4NetfilterHook::operator== (const Ipv4NetfilterHook& hook) const
{
  // By default, all hooks are not equal
  return false;
}

} // Namespace ns3
