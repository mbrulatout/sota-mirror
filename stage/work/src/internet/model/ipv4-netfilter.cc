// vim:set cin cino=>4,n-2,{2,^-2,:2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1 sw=2 ts=8 et :
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Texas at Dallas
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Qasim Javed <qasim@utdallas.edu>
 *         Adrian S. Tam <adrian.sw.tam@gmail.com>
 */

#include "ns3/log.h"
#include "ipv4-netfilter.h"
#include "ipv4-netfilter-hook.h"

NS_LOG_COMPONENT_DEFINE ("Ipv4Netfilter");

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED (Ipv4Netfilter);

TypeId
Ipv4Netfilter::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::Ipv4Netfilter")
    .SetParent<Object> ()
  ;
  return tid;
}

Ipv4Netfilter::Ipv4Netfilter () : m_chains (NF_INET_NUMHOOKS)
{
  NS_LOG_FUNCTION_NOARGS ();
}

uint32_t
Ipv4Netfilter::AppendNetfilterHook (Hook_t type, Ptr<Ipv4NetfilterHook> hook)
{
  NS_LOG_FUNCTION (this << type << hook);
  m_chains[type].push_back (hook);
  hook->RegisterNetfilter (this);
  return 0;
}

uint32_t
Ipv4Netfilter::RemoveNetfilterHook (Hook_t type, Ptr<Ipv4NetfilterHook> hook)
{
  HooksChain::iterator i;
  for (i = m_chains[type].begin (); i != m_chains[type].end (); ++i)
    {
      if (*i == hook)
        {
          m_chains[type].erase (i);
          return 0;
        }
    }
  return 1;
}

uint32_t
Ipv4Netfilter::ProcessHooks (Hook_t type, Ptr<Packet> p, Ipv4Header& iph,
                             Ptr<NetDevice> in, Ptr<NetDevice> out)
{
  HooksChain::iterator i;
  for (i = m_chains[type].begin (); i != m_chains[type].end (); ++i)
    {
      if ((*i)->Match (p,iph,in,out))
        {
          return (*i)->Manipulate (p,iph,in,out);
        }
    }
  return 1;
}

} // Namespace ns3
