// vim: sw=2 cin cino=>4,n-2,{2,^-2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1 sw=2 ts=8 et:
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009-2010 New York University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Adrian S. Tam <adrian.sw.tam@gmail.com>
 */

#ifndef IPV4_NF_PACKET_FILTER_H
#define IPV4_NF_PACKET_FILTER_H

#include "ns3/ipv4-address.h"
#include "ipv4-netfilter-hook.h"
#include "ipv4-netfilter.h"
//#include "ns3/hop-by-hop.h"
//#include "ns3/hbh-protocol.h"
#include "ns3/tetrys-utils.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/node.h"
namespace ns3 {

/**
  * \brief Implementation of the Hook datastructure
  *
  * This contains information such as the hook callback function, the priority
  * of the hook callback, the protocol family this callback caters to and the
  * hook number. The hook number is needed to identify the hook and thus the
  * callback chain where the hook function should be inserted.
  */

class Ipv4NfPacketFilter : public Ipv4NetfilterHook
{
public:
  typedef enum {
    ACCEPT, DROP
  } action_t;

  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;
  Ipv4NfPacketFilter ();
  virtual ~Ipv4NfPacketFilter ();

  virtual bool operator== (const Ipv4NfPacketFilter& hook) const;
  virtual bool Match (Ptr<Packet> p, Ipv4Header& h, Ptr<NetDevice> in, Ptr<NetDevice> out);
  virtual bool Manipulate (Ptr<Packet> p, Ipv4Header& h, Ptr<NetDevice> in, Ptr<NetDevice> out);
  void RegisterProcess (Callback<void, Ptr<Packet>, Ipv4Header > c);
  void Config (const char* configStr);
private:
  Callback<void, Ptr<Packet>, Ipv4Header > m_c;
  enum
  {
    SADDR = 0x0001,
    SMASK = 0x0002,
    DADDR = 0x0004,
    DMASK = 0x0008,
    DNODESTINATION = 0x1000,
    PROTO = 0x0010,
    SPORT = 0x0020,
    DPORT = 0x0040,
    INDEV = 0x0080,
    OUDEV = 0x0100
  };
  unsigned m_checktypes;
  uint16_t m_sport;
  uint16_t m_dport;
  uint8_t m_protocol;
  Ipv4Address m_saddr;
  Ipv4Address m_daddr;
  Ipv4Mask m_smask;
  Ipv4Mask m_dmask;
  Ptr<NetDevice> m_indev;
  Ptr<NetDevice> m_outdev;
  action_t m_action;
};

} // Namespace ns3
#endif /* IPV4_NETFILTER_HOOK_H */
