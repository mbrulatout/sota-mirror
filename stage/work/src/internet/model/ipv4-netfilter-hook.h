// vim: sw=2 cin cino=>4,n-2,{2,^-2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1 sw=2 ts=8 et:
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Texas at Dallas
 * Copyright (c) 2009 New York University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Qasim Javed <qasim@utdallas.edu>
 *         Adrian S. Tam <adrian.sw.tam@gmail.com>
 */

#ifndef IPV4_NETFILTER_HOOK_H
#define IPV4_NETFILTER_HOOK_H

#include "ns3/packet.h"
#include "ns3/ptr.h"
#include "ns3/net-device.h"
#include "ns3/callback.h"
#include <list>

namespace ns3 {

class Ipv4Header;
class Ipv4Netfilter;

/**
  * \brief Implementation of the Hook datastructure
  *
  * This contains information such as the hook callback function, the priority
  * of the hook callback, the protocol family this callback caters to and the
  * hook number. The hook number is needed to identify the hook and thus the
  * callback chain where the hook function should be inserted.
  */

class Ipv4NetfilterHook : public Object
{
public:
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;
  Ipv4NetfilterHook ();
  virtual ~Ipv4NetfilterHook ();
  virtual bool operator== (const Ipv4NetfilterHook& hook) const;
  virtual bool Match (Ptr<Packet> p, Ipv4Header& h, Ptr<NetDevice> in, Ptr<NetDevice> out) = 0;
  virtual bool Manipulate (Ptr<Packet> p, Ipv4Header& h, Ptr<NetDevice> in, Ptr<NetDevice> out) = 0;
  void RegisterNetfilter (Ptr<Ipv4Netfilter> nf);
private:
  Ptr<Ipv4Netfilter> m_netfilter;
};

typedef std::list<Ptr<Ipv4NetfilterHook> > HooksChain;

} // Namespace ns3
#endif /* IPV4_NETFILTER_HOOK_H */
