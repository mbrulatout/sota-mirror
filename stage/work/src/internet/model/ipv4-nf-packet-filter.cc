// vim: sw=2 cin cino=>4,n-2,{2,^-2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1 sw=2 ts=8 et:
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009-2010 New York University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Adrian S. Tam <adrian.sw.tam@gmail.com>
 */

#include "ns3/packet.h"
#include "ns3/tcp-header.h"
#include "ns3/udp-header.h"
#include "ns3/ipv4-netfilter.h"
#include "tcp-l4-protocol.h"
#include "udp-l4-protocol.h"
//#include "ns3/hbh-protocol.h"
//#include "ns3/tetrys-protocol.h"
#include "ns3/log.h"
#include "ipv4-nf-packet-filter.h"

NS_LOG_COMPONENT_DEFINE ("Ipv4NfPacketFilter");

namespace ns3 {

TypeId
Ipv4NfPacketFilter::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::Ipv4NfPacketFilter")
    .SetParent<Object> ()
  ;
  return tid;
}

TypeId
Ipv4NfPacketFilter::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

Ipv4NfPacketFilter::Ipv4NfPacketFilter () : m_checktypes (0),
                                            m_action (ACCEPT)
{
}

Ipv4NfPacketFilter::~Ipv4NfPacketFilter ()
{
}

bool
Ipv4NfPacketFilter::operator== (const Ipv4NfPacketFilter& hook) const
{
  if (GetInstanceTypeId () != hook.GetInstanceTypeId ())
    {
      return false;
    }
  if (m_checktypes != hook.m_checktypes)
    {
      return false;
    }
  if ((m_checktypes & SADDR) && m_saddr != hook.m_saddr)
    {
      return false;
    }
  if ((m_checktypes & DADDR) && m_daddr != hook.m_daddr)
    {
      return false;
    }
  if ((m_checktypes & SMASK) && m_smask != hook.m_smask)
    {
      return false;
    }
  if ((m_checktypes & DMASK) && m_dmask != hook.m_dmask)
    {
      return false;
    }
  if ((m_checktypes & PROTO) && m_protocol != hook.m_protocol)
    {
      return false;
    }
  if ((m_checktypes & SPORT) && m_sport != hook.m_sport)
    {
      return false;
    }
  if ((m_checktypes & DPORT) && m_dport != hook.m_dport)
    {
      return false;
    }
  if ((m_checktypes & INDEV) && m_indev != hook.m_indev)
    {
      return false;
    }
  if ((m_checktypes & OUDEV) && m_outdev != hook.m_outdev)
    {
      return false;
    }
  return true;
}

void
Ipv4NfPacketFilter::RegisterProcess(Callback<void, Ptr<Packet>, Ipv4Header > c){
	NS_LOG_FUNCTION ("RegisterProcess");
        m_c=c;
}

bool
Ipv4NfPacketFilter::Match (Ptr<Packet> p, Ipv4Header& h, Ptr<NetDevice> in, Ptr<NetDevice> out)
{
  if (m_checktypes & SADDR)
    {
      Ipv4Address packetSrcAddr = h.GetSource ();
      if (m_checktypes & SMASK)
        {
          if (!m_smask.IsMatch (m_saddr, packetSrcAddr))
            {
              return false;
            }
        }
      else
        {
          if (m_saddr != packetSrcAddr)
            {
              return false;
            }
        }
    }
  if (m_checktypes & DADDR)
    {
      Ipv4Address packetDestAddr = h.GetDestination ();
      if (m_checktypes & DMASK)
        {
          if (!m_dmask.IsMatch (m_daddr, packetDestAddr)){
              return false;
            }
        }
      else
        {
          if (m_daddr != packetDestAddr)
            {
              return false;
            }
        }
    }
  if (m_checktypes & PROTO)
    {
      if (m_protocol != h.GetProtocol ())
        {
          return false;
        }
        //NS_LOG_UNCOND ("check proto..." << p->GetUid() );
        //if(m_protocol == TcpL4Protocol::PROT_NUMBER) NS_LOG_UNCOND ("check tcp..." );
       if (m_checktypes & DNODESTINATION){
       		Ipv4Address packetDestAddr = h.GetDestination ();
		//NS_LOG_UNCOND ("check dest..." << packetDestAddr);
		  if(out->GetNode()->GetObject<Ipv4L3Protocol>()->IsDestinationAddress(packetDestAddr, 0)){
		  	NS_LOG_UNCOND ("NODESTINATION..." );
		  	return false;
		  }
       }   	
		         	
      if ((m_checktypes & (SPORT | DPORT)) && m_protocol == TcpL4Protocol::PROT_NUMBER)
        {
          TcpHeader h;
          p->PeekHeader (h);
          if ((m_checktypes & SPORT) && m_sport != h.GetSourcePort ())
            {
              return false;
            }
          if ((m_checktypes & DPORT) && m_dport != h.GetDestinationPort ())
            {
              return false;
            }
        }
      else if ((m_checktypes & (SPORT | DPORT)) && m_protocol == UdpL4Protocol::PROT_NUMBER)
        {
          UdpHeader h;
          p->PeekHeader (h);
          if ((m_checktypes & SPORT) && m_sport != h.GetSourcePort ())
            {
              return false;
            }
          if ((m_checktypes & DPORT) && m_dport != h.GetDestinationPort ())
            {
              return false;
            }
        }
    }
  if ((m_checktypes & INDEV) && in != m_indev)
    {
      return false;
    }
  if ((m_checktypes & OUDEV) && out != m_outdev)
    {
      return false;
    }

  //callback
  m_c(p,h);
  return true;
}

bool
Ipv4NfPacketFilter::Manipulate (Ptr<Packet> p, Ipv4Header& h, Ptr<NetDevice> in, Ptr<NetDevice> out)
{
  return (m_action == ACCEPT);
}

void
Ipv4NfPacketFilter::Config (const char* configStr)
{
  NS_LOG_FUNCTION (configStr);
  // Parse the configuration string using C90 strtok() call
  char* token;
  char* s = (char*) malloc (strlen (configStr) + 1);
  strcpy (s, configStr);

  m_checktypes = 0;     // Reset config
  m_action = ACCEPT;
   //NS_LOG_UNCOND ("cong filter");
  for (token = strtok (s, " \t"); token; token = strtok (NULL, " \t"))
    {
      // source address
      if (strncmp (token, "-s", 3) == 0)
        {
          token = strtok (NULL, " \t");
          if (token == NULL)
            {
              NS_LOG_LOGIC ("Error parsing config string " << configStr << ". Halted.");
              break;
            }
          char* mask = strchr (token,'/');
          if (mask != NULL)
            {
              *mask = '\0';
              mask++;
              m_smask = Ipv4Mask (mask);
              m_checktypes |= SMASK;
              NS_LOG_LOGIC ("Setting source mask" << m_smask);
            }
          m_saddr = Ipv4Address (token);
          m_checktypes |= SADDR;
          NS_LOG_LOGIC ("Setting source address " << m_saddr);
        }// destination address
        else if (strncmp (token, "-d", 3) == 0)
        {
          token = strtok (NULL, " \t");
          if (token == NULL)
            {
              NS_LOG_LOGIC ("Error parsing config string " << configStr << ". Halted.");
              break;
            }
          char* mask = strchr (token,'/');
          if (mask != NULL)
            {
              *mask = '\0';
              mask++;
              m_dmask = Ipv4Mask (mask);
              m_checktypes |= DMASK;
              NS_LOG_LOGIC ("Setting destination mask" << m_dmask);
            }
          m_daddr = Ipv4Address (token);
          m_checktypes |= DADDR;
          NS_LOG_LOGIC ("Setting source address " << m_daddr);
        }
       else if (strncmp (token, "-nodestination", 15) == 0)
        {
        //token = strtok (NULL, " \t");
          if (token == NULL)
            {
              NS_LOG_LOGIC ("Error parsing config string " << configStr << ". Halted.");
              break;
            }
          m_checktypes |= DNODESTINATION;
        }
      // L4 protocol
      else if (strncmp (token, "-p", 3) == 0)
        {
          token = strtok (NULL, " \t");
          if (token == NULL)
            {
              NS_LOG_LOGIC ("Error parsing config string " << configStr << ". Halted.");
              break;
            }
          
          if (strncmp (token, "tcp", 4) == 0)
            {
              m_protocol = TcpL4Protocol::PROT_NUMBER;
              //NS_LOG_UNCOND ("tcp");
            }
          else if (strncmp (token, "udp", 4) == 0)
            {
              m_protocol = UdpL4Protocol::PROT_NUMBER;
            }
          else if (strncmp (token, "tetrys", 7) == 0)
            { 
              m_protocol = TetrysUtils::PROT_NUMBER;
	    }
          //else if (strncmp (token, "hbh", 4) == 0)
		  //{
		  //   m_protocol = HbhProtocol::PROT_NUMBER;
              //NS_LOG_UNCOND ("tcp");
          //  }
          else
            {
              m_protocol = atoi (token);
            }
          m_checktypes |= PROTO;
          NS_LOG_LOGIC ("Setting protocol " << (unsigned)m_protocol);
        }
      // source port
      else if (strncmp (token, "--sport", 8) == 0)
        {
          token = strtok (NULL, " \t");
          if (token == NULL)
            {
              NS_LOG_LOGIC ("Error parsing config string " << configStr << ". Halted.");
              break;
            }
          m_sport = atoi (token);
          m_checktypes |= SPORT;
          NS_LOG_LOGIC ("Setting source port " << m_sport);
        }
      // destination port
      else if (strncmp (token, "--dport", 8) == 0)
        {
          token = strtok (NULL, " \t");
          if (token == NULL)
            {
              NS_LOG_LOGIC ("Error parsing config string " << configStr << ". Halted.");
              break;
            }
          m_dport = atoi (token);
          m_checktypes |= DPORT;
          NS_LOG_LOGIC ("Setting destination port " << m_dport);
        }
      // action
      else if (strncmp (token, "-j", 3) == 0)
        {
          token = strtok (NULL, " \t");
          //NS_LOG_UNCOND ("action");
          if (token == NULL)
            {
              NS_LOG_LOGIC ("Error parsing config string " << configStr << ". Halted.");
              break;
            }
          m_action = strncmp (token, "ACCEPT", 7) ? DROP : ACCEPT;
          //if(m_action==DROP) NS_LOG_UNCOND ("coucou");
          NS_LOG_LOGIC ("Setting action " << m_action);
        }
    }
  free (s);
}

} // Namespace ns3
