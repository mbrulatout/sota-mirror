//============================================================================
// Name        : tetrys-scenario.cc
// Author      : Mathias Brulatout
// Date        : April 2014
// Version     : v0.1
// Copyright   : Copyright (c) 2014 Thales Inc. All Rights Reserved.
// Description :
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <errno.h>
#include <stdint.h>



#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/csma-helper.h"
#include "ns3/csma-net-device.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/packet-sink.h"

#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/network-module.h"
#include "ns3/config-store-module.h"

#include "ns3/net-device-container.h"
#include "ns3/random-variable.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/gnuplot.h"
#include "ns3/point-to-point-module.h"
#include "ns3/tetrys-protocol.h"
#include "ns3/tetrys-helper.h"
#include "ns3/tetrys-utils.h"


NS_LOG_COMPONENT_DEFINE("TetrysScenario");

double RTOGlobal = 0;

using namespace ns3;

using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::ostringstream;
using std::istringstream;
using std::pair;
using std::make_pair;
using std::list;


vector<vector<bool> > readNxNMatrix(string);
vector<vector<double> > readCordinatesFile(string);
void printMatrix(const char* description, vector<vector<bool> >);
void printCoordinateArray(const char* description, vector<vector<double> >);
static void EnableFilter(Ptr<Ipv4L3Protocol>, Ptr<Ipv4NfPacketFilter>, Hook_t);
void CloseWhenFinished(ApplicationContainer &tetrysApps);
void processLinkEvents(list<pair<float, pair<pair<int, int >,
                       pair<string, pair<string, float> > > > >&, NodeContainer&);

//Apply changes to link properties (data rate, errors, delay...) at a given time

void processLinkEvents(list<pair<float, pair<pair<int, int >, pair<string, pair<string, float> > > > > &events, NodeContainer & c) {
    int interval = 100000000; // EVERY 0.1 SECOND !!!!
    //cout << Simulator::Now().GetSeconds() << " Checking events : Modify only DataRate !" << endl;

    list<pair<float, pair<pair<int, int >, pair<string, pair<string, float> > > > >::iterator it_events = events.begin();
    if (it_events != events.end()) {

        if (it_events->first + STARTING_TIME < Simulator::Now().GetSeconds()) {
            //reconfigure links
            //            NS_LOG_UNCOND("Should process event with data rate " << it_events->second.second.first
            //                          << " and error rate " << it_events->second.second.second.second);
            for (NodeContainer::Iterator iter = c.Begin(); iter != c.End(); iter++) {
                for (unsigned int i = 1; i < (*iter)->GetNDevices(); i++) {
                    Ptr<RateErrorModel> em = CreateObjectWithAttributes<RateErrorModel > (
                                                                                          "ErrorUnit",
                                                                                          StringValue("ERROR_UNIT_PACKET"),
                                                                                          "ErrorRate",
                                                                                          DoubleValue(it_events->second.second.second.second));
                    //Update Error ratio and link data rate
                    DynamicCast<PointToPointNetDevice > ((*iter)->GetDevice(i))->SetAttribute("ReceiveErrorModel", PointerValue(em));
                    DynamicCast<PointToPointNetDevice > ((*iter)->GetDevice(i))->SetDataRate(DataRate(it_events->second.second.first));
                }
            }

            /*PointToPointHelper p2p;
            p2p.SetDeviceAttribute("DataRate", StringValue(it_events->second.second.first));
            p2p.SetChannelAttribute("Delay", StringValue(it_events->second.second.second));
            NodeContainer n_links = NodeContainer(c.Get(it_events->second.first.first), c.Get(it_events->second.first.second));
            NetDeviceContainer n_devs = p2p.Install(n_links);
             */

            //remove processed event
            events.pop_front();
            NS_LOG_UNCOND(Simulator::Now().GetSeconds() << " Event processed !!!!!!!!!!!!!!!! ");
        }
    }

    Time next = Time(Simulator::Now().GetSeconds() + interval);
    Simulator::Schedule(next, &processLinkEvents, events, c);
}

struct KeyEqualFlow {
    const long& i;
    const long& j;

    KeyEqualFlow(const long& i, const long& j) :
    i(i), j(j) {
    }

    bool operator()(std::pair<int, int> & lhs) const {
        return ((lhs.first == i) && (lhs.second == j));
    }
};

struct KeyEqualSrcFlow {
    const long& i;
    const long& j;

    KeyEqualSrcFlow(const long& i, const long& j) :
    i(i), j(j) {
    }

    bool operator()(std::pair<int, int> & lhs) const {
        return lhs.first == i;
    }
};

struct KeyEqualDstFlow {
    const long& i;
    const long& j;

    KeyEqualDstFlow(const long& i, const long& j) :
    i(i), j(j) {
    }

    bool operator()(std::pair<int, int> & lhs) const {
        return lhs.second == j;
    }
};

Ptr<OutputStreamWrapper> stream;

int main(int argc, char *argv[]) {
    LogComponentEnable("TetrysScenario", LOG_LEVEL_INFO);
    /* Default parameters */
    double distance = 70; /* in  meters */
    string phyMode("DsssRate2Mbps");
    uint32_t totalTxBytes = 2000;
    int nbRun = 1;
    list<pair<float, pair<pair<int, int >, pair<string, pair<string, float> > > > > events;

    /* Execution options */
    bool verbose = false;
    bool tracing = false;
    bool wireless = false;

    /* Tetrys parameters */
    uint16_t MaxNbFlows = 1;

    bool tetrys = true;
    double ErrorRate = 0.1;
    string LinkRate("4000kbps");
    string LinkDelay("150ms");
    int RedRatio = 10;
    int numNodes = 9;
    int seed = 42;
    string filename("./userdata/random.txt");
    int WindowSize = 100;
    double ACKFreq = 0.2;
    
    int maxBurstSize=20;
    // Uniform
    // Sinus
    // Burst
    string LossModel("Uniform");
    /* Command line parameters */
    CommandLine cmd;
    cmd.AddValue("phyMode", "\t\t\t Wifi Phy mode", phyMode);
    cmd.AddValue("distance", "\t\t distance (m)", distance);
    cmd.AddValue("verbose", "\t\t\t turn on all WifiNetDevice log components", verbose);
    cmd.AddValue("tracing", "\t\t\t turn on ascii and pcap tracing", tracing);
    cmd.AddValue("numNodes", "\t\t number of nodes", numNodes);
    cmd.AddValue("MaxNbFlows", "\t\t number of TCP flows", MaxNbFlows);
    cmd.AddValue("tetrys", "\t\t\t activate the tetrys protocol", tetrys);
    cmd.AddValue("nbRun", "\t\t\t run id", nbRun);
    cmd.AddValue("wireless", "\t\t wireless or not", wireless);
    cmd.AddValue("TxSize", "\t\t\t size to send", totalTxBytes);
    cmd.AddValue("LinkRate", "\t\t rate", LinkRate);
    cmd.AddValue("LinkDelay", "\t\t delay", LinkDelay);
    cmd.AddValue("ErrorRate", "\t\t error rate", ErrorRate);
    cmd.AddValue("RedRatio", "\t\t redundancy ratio", RedRatio);
    cmd.AddValue("filename", "\t\t file to transfer", filename);
    cmd.AddValue("seed", "\t\t random seed used", seed);
    cmd.AddValue("LossModel", "\t\t Loss distribution : Uniform or Sinus or Burst", LossModel);
    cmd.AddValue("EncodingWindow", "\t\t Encoding window maximum size in packets", WindowSize);
    cmd.AddValue("ACKFreq","\t\t Ack frequency in seconds",ACKFreq);
    cmd.AddValue("BurstSize","\t\t Max Burst Size",maxBurstSize);
    cmd.Parse(argc, argv);

    
    string strBurst="ns3::UniformRandomVariable[Min=1|Max=" + std::to_string(maxBurstSize) + "]";
    
    string adj_mat_file_name = "./userdata/adjacency_matrix" + std::to_string(numNodes) + ".txt";
    string node_coordinates_file_name = "./userdata/node_coordinates" + std::to_string(numNodes) + ".txt";

    //Different random seed for each run
    //    int byte_count = 64;
    //    char data[32];
    //    FILE * fp;
    //    fp = fopen("/dev/urandom", "r");
    //    fread(&data, 1, byte_count, fp);
    //    fclose(fp);
    //    RngSeedManager::SetSeed((int) data);
    RngSeedManager::SetSeed(seed);

    //For LOG purpose only
    NS_LOG_UNCOND("INFO : ACK_FREQ " << ACKFreq);
    NS_LOG_UNCOND("INFO : HEADER_SIZE " << TetrysHdr::getSize());
    NS_LOG_UNCOND("INFO : PACKET_SIZE " << PACKET_SIZE);
    NS_LOG_UNCOND("INFO : CBR_FREQ " << CBR_FREQ);
    NS_LOG_UNCOND("INFO : PROTOCOL " << TETRYS_NAME);
    NS_LOG_UNCOND("INFO : ERROR_ESTIMATION_FREQ " << ERROR_WINDOW_TIME);
    NS_LOG_UNCOND("INFO : ALPHA " << ALPHA_VALUE);
    NS_LOG_UNCOND("INFO : ENCODING WINDOW SIZE" << WindowSize);
    NS_LOG_UNCOND("INFO : MAX BURST SIZE " << maxBurstSize);
    
    /* disable fragmentation for frames below 2200 bytes */
    Config::SetDefault("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue("2200"));
    /* turn off RTS/CTS for frames below 2200 bytes */
    Config::SetDefault("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue("2200"));
    /* Fix non-unicast data rate to be the same as that of unicast */
    Config::SetDefault("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue(phyMode));

    //Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (1000));
    /*disable WiFi acknowledgements */
    //Config::SetDefault("ns3::WifiRemoteStationManager::MaxSlrc", StringValue("0"));
    GlobalValue::Bind("ChecksumEnabled", BooleanValue(false));
    /* Create the nodes */
    NodeContainer c;
    c.Create(numNodes);


    /* Set of helper to set channel attributes */
    PointToPointHelper p2p;
    CsmaHelper csma;
    InternetStackHelper internet;
    NetDeviceContainer devices;
    OlsrHelper olsr;

    if (verbose) {
        //        wifi.EnableLogComponents(); /* Turn on all Wifi logging */
        //        LogComponentEnable("Ipv4RawSocketImpl", LOG_LEVEL_INFO);
        //        LogComponentEnable("PacketSink", LOG_LEVEL_INFO);
        //        LogComponentEnable("Ipv4NfPacketFilter", LOG_LEVEL_INFO);
        //        //LogComponentEnable("TcpSocket", LOG_LEVEL_INFO);
        //        //LogComponentEnable("TcpSocketBase", LOG_LEVEL_INFO);
        //        LogComponentEnable("Ipv4L3Protocol", LOG_LEVEL_INFO);
        //        LogComponentEnable("WifiHelper", LOG_LEVEL_INFO);
        //       LogComponentEnable("OlsrRoutingProtocol", LOG_LEVEL_INFO);
    }

    ///////////////////////////////////////////////////////////////
    // Wifi
    ////////////////////////////////////////////////////////////////

    WifiHelper wifi;
    YansWifiPhyHelper wifiPhy;
    NqosWifiMacHelper wifiMac;
    if (wireless == true) {
        /* Configure Wi-Fi channel */
        wifiPhy = YansWifiPhyHelper::Default();
        /* set it to zero; otherwise, gain will be added */
        //		wifiPhy.Set ("RxGain", DoubleValue (-10) );
        /* ns-3 supports RadioTap and Prism tracing extensions for 802.11b */
        //		wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
        YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
        wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
        //wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel");
        //wifiPhy.SetErrorRateModel ("ns3::YansErrorRateModel")
        wifiPhy.SetChannel(wifiChannel.Create());

        /* Add a non-QoS upper mac, and disable rate control */
        wifiMac = NqosWifiMacHelper::Default();
        wifi.SetStandard(WIFI_PHY_STANDARD_80211b);
        wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager", "DataMode", StringValue(phyMode),
                                     "ControlMode", StringValue(phyMode));
        /* Set it to adhoc mode */
        wifiMac.SetType("ns3::AdhocWifiMac");
        devices = wifi.Install(wifiPhy, wifiMac, c);

        /* Allocate nodes positions */
        MobilityHelper mobility;
        mobility.SetPositionAllocator("ns3::GridPositionAllocator", "MinX",
                                      DoubleValue(0.0), "MinY", DoubleValue(0.0),
                                      "DeltaX", DoubleValue(distance), "DeltaY",
                                      DoubleValue(distance), "GridWidth",
                                      UintegerValue(5), "LayoutType",
                                      StringValue("RowFirst"));
        mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
        mobility.Install(c);

        /* Enable OLSR */
        Ipv4StaticRoutingHelper staticRouting;
        Ipv4ListRoutingHelper list;
        list.Add(staticRouting, 0);
        list.Add(olsr, 10); /* 10 is the priority */

        /* Install TCP/IP & Assign IP addresses */
        internet.SetRoutingHelper(list); /* has effect on the next Install () */
        internet.Install(c);
        Ipv4AddressHelper ipv4;
        ipv4.SetBase("10.1.1.0", "255.255.255.0");
        Ipv4InterfaceContainer i = ipv4.Assign(devices);
    }

    //////////////////////////////////////////////////////////////////////////////
    // Wired
    ///////////////////////////////////////////////////////////////////////////////

    if (wireless == false) {


        NS_LOG_UNCOND("Wired configuration");
        //Retrieve topology
        vector < vector<bool> > Adj_Matrix;
        Adj_Matrix = readNxNMatrix(adj_mat_file_name);
        vector<vector<double> > coord_array;
        coord_array = readCordinatesFile(node_coordinates_file_name);

        uint32_t n_nodes = coord_array.size();
        uint32_t matrixDimension = Adj_Matrix.size();

        if ((matrixDimension != n_nodes) && (n_nodes != numNodes)) {
            NS_FATAL_ERROR("The number of lines in coordinate file is: " << n_nodes << \
 " not equal to the number of nodes in adjacency matrix size " << matrixDimension);
        }

        csma.SetChannelAttribute("DataRate", StringValue(LinkRate));
        csma.SetChannelAttribute("Delay", StringValue(LinkDelay));
        csma.SetDeviceAttribute("Mtu", UintegerValue(1500));
        csma.SetDeviceAttribute("EncapsulationMode", StringValue("Dix"));

        p2p.SetDeviceAttribute("DataRate", StringValue(LinkRate));
        p2p.SetChannelAttribute("Delay", StringValue(LinkDelay));

        internet.Install(c);
        Ipv4AddressHelper ipv4_n;
        ipv4_n.SetBase("10.1.1.0", "255.255.255.0", "0.0.0.1");
        uint32_t linkCount = 0;

        for (size_t i = 0; i < Adj_Matrix.size(); i++)
            for (size_t j = 0; j < Adj_Matrix[i].size(); j++)
                if ((Adj_Matrix[i][j] == 1) && (i < j)) {
                    NodeContainer n_links = NodeContainer(c.Get(i), c.Get(j));
                    NetDeviceContainer n_devs = p2p.Install(n_links);
                    //                    NetDeviceContainer n_devs = csma.Install(n_links);
                    Ptr<ErrorModel> em;
                    //if (TETRYS_TYPE == TETRYS_ADAPTATIVE || TETRYS_TYPE == TETRYS_MOBILE_ADAPTATIVE)
                    if (LossModel.compare("Uniform") == 0) {
                        NS_LOG_UNCOND(LossModel);
                        em = CreateObjectWithAttributes<RateErrorModel > (
                                                                          "ErrorUnit",
                                                                          StringValue("ERROR_UNIT_PACKET"),
                                                                          "ErrorRate",
                                                                          DoubleValue(ErrorRate));
                    } else if (LossModel.compare("Sinus") == 0 || LossModel.compare("Random") == 0) //Sinus and Random handled by events
                        em = CreateObjectWithAttributes<RateErrorModel > (
                                                                          "ErrorUnit",
                                                                          StringValue("ERROR_UNIT_PACKET"),
                                                                          "ErrorRate",
                                                                          DoubleValue(0.0));
                    else if (LossModel.compare("Burst") == 0)
                        em = CreateObjectWithAttributes<BurstErrorModel > (
                                                                           "ErrorRate",
                                                                           DoubleValue(0.01),
                                                                           "BurstStart",
                                                                           StringValue("ns3::UniformRandomVariable[Min=0.0|Max=1.0]"),
                                                                           "BurstSize",
                                                                           StringValue(strBurst));
                    n_devs.Get(0)->SetAttribute("ReceiveErrorModel", PointerValue(em));
                    n_devs.Get(1)->SetAttribute("ReceiveErrorModel", PointerValue(em));
                    ipv4_n.Assign(n_devs);
                    ipv4_n.NewNetwork();
                    linkCount++;
                }
        cout << "Number of links in the adjacency matrix is: " << linkCount << endl;
        cout << "Number of all nodes is: " << c.GetN() << endl;

        /* print list of IPs */
        string str = "List of IPs: \n";

        for (NodeContainer::Iterator iter = c.Begin(); iter != c.End(); iter++) {
            ostringstream convert;
            convert << "Node " << (*iter)->GetId() << " : ";
            for (int i = 1; i < (*iter)->GetNDevices(); i++)
                convert << ((*iter)->GetObject<Ipv4L3Protocol > ()->GetAddress(i, 0).GetLocal()) << "\t";
            str += convert.str() + "\n";
        }
        NS_LOG_INFO(str);

        /* Initialize Global Routing */
        Ipv4GlobalRoutingHelper::PopulateRoutingTables();

        /* Allocate Positions to Nodes */
        //NS_LOG_UNCOND("POSITIONING");
        MobilityHelper mobility_n;
        Ptr<ListPositionAllocator> positionAlloc_n = CreateObject<ListPositionAllocator > ();

        for (size_t m = 0; m < coord_array.size(); m++) {
            positionAlloc_n->Add(Vector(coord_array[m][0], coord_array[m][1], 0));
            Ptr<Node> n0 = c.Get(m);
            Ptr<ConstantPositionMobilityModel> nLoc = n0->GetObject<ConstantPositionMobilityModel > ();
            if (nLoc == 0) {
                nLoc = CreateObject<ConstantPositionMobilityModel > ();
                n0->AggregateObject(nLoc);
            }
            /* y-coordinates are negated for correct display in NetAnim
             * NetAnim's (0,0) reference coordinates are located
             * on upper left corner by negating the y coordinates,
             * we declare the reference (0,0) coordinate
             * to the bottom left corner
             */
            Vector nVec(coord_array[m][0], -coord_array[m][1], 0);
            nLoc->SetPosition(nVec);

        }
        mobility_n.SetPositionAllocator(positionAlloc_n);
        mobility_n.Install(c);
    }

    if (tracing == true) {
        NS_LOG_INFO("Setting tracing on, written to tetrys.tr");
        AsciiTraceHelper ascii;
        stream = ascii.CreateFileStream("tetrys.tr"); /* old NS2 format */
        internet.EnableAsciiIpv4All(stream);

        if (wireless == true) {
            wifiPhy.EnableAsciiAll(stream);
            wifiPhy.EnablePcap("tetrys", devices);

            /* Trace routing tables */
            Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper > ("tetrys.routes", std::ios::out);
            olsr.PrintRoutingTableAllAt(Seconds(2.0), routingStream);
        } else {
            p2p.EnableAsciiAll(stream);
        }
    }

    TetrysHelper tetrys_app;
    ApplicationContainer tetrysApps;
    for (NodeContainer::Iterator iter = c.Begin(); iter != c.End(); iter++) {
        Ptr<Node> node = *iter;
        tetrysApps.Add(tetrys_app.Install(node));
        tetrysApps.Start(Seconds(1.0));
        //tetrysApps.Stop (Seconds (300.0));
    }

    //    for (uint32_t j = 0; j < tetrysApps.GetN(); j++)
    //        Ptr<TetrysProtocol> app = DynamicCast<TetrysProtocol > (tetrysApps.Get(j));


    //read events from files IF WIRELESS DISABLED
    if (wireless == false && (LossModel.compare("Sinus") == 0 || LossModel.compare("Random") == 0)) {
        //SINUS
        NS_LOG_INFO("Reading events");
        ifstream file;
        string filename = "./userdata/events";
        uint16_t nbEvents;
        errno = 0;
        file.open(filename.c_str(), std::ios::in);
        if (file.fail()) {
            int tmp = errno;
            NS_FATAL_ERROR("File " << filename.c_str() <<
                           " not found: strerror(" << tmp << "): " << strerror(tmp));
        }
        if (!(file >> nbEvents))
            NS_FATAL_ERROR("File " << filename.c_str() << " not correctly formatted.");
        while (nbEvents > 0) {
            //NS_LOG_INFO("Creating one event");
            float time, error;
            uint32_t src, dst;
            string bw, delay;

            if (!file.fail() && !file.eof()) {
                file >> time >> src >> dst >> bw >> delay >> error;
                //                NS_LOG_UNCOND("EVENT : " << time << " " << src << " " << dst << " "
                //                              << bw << " " << delay << " " << error);
                events.push_back(make_pair(time, make_pair(make_pair(src, dst), make_pair(bw, make_pair(delay, error)))));
            }
            nbEvents--;
        }
        file.close();
    }

    /* Setup Netfilter & install our process on all nodes */
    if (tetrys == true) {
        NS_LOG_INFO("Setting up Netfilter on all nodes");
        Ptr<Ipv4L3Protocol> I3;
        for (NodeContainer::Iterator iter = c.Begin(); iter != c.End(); iter++) {
            Ptr<Node> node = *iter;
            for (uint32_t j = 0; j < tetrysApps.GetN(); j++) {
                Ptr<TetrysProtocol> app = DynamicCast<TetrysProtocol > (tetrysApps.Get(j));
                if (app->GetNode()->GetId() == node->GetId()) {
                    I3 = node->GetObject<Ipv4L3Protocol > ();
                    Ptr<Ipv4NfPacketFilter> filter1;
                    ostringstream outs1;
                    outs1 << "-p tetrys -j DROP";
                    filter1 = CreateObject<Ipv4NfPacketFilter>();
                    filter1->Config(outs1.str().c_str());
                    filter1->RegisterProcess(MakeCallback(&TetrysProtocol::interceptPacketTetrys, app));
                    EnableFilter(I3, filter1, NF_INET_PREROUTING); //NF_INET_PREROUTING, NF_INET_OUTPUT, NF_INET_FORWARD, NF_INET_INPUT
                }
            }
        }
    }

    //create external UDP/TCP flows (x unique pairs of nodes)
    uint16_t nbFlows = MaxNbFlows;
    vector<pair<int, int> > flows; //list of flows
    vector<pair<int, int> >::iterator it_flow;
    vector<pair<int, int> >::iterator it_src_flow;
    vector<pair<int, int> >::iterator it_dst_flow;



    FILE* flowfile = fopen(filename.c_str(), "r");
    if (NULL == flowfile)
        NS_FATAL_ERROR("Error opening file");
    fseek(flowfile, 0L, SEEK_END);
    uint32_t filesize = ftell(flowfile);
    fclose(flowfile);
    NS_LOG_UNCOND("File to send has a size of " << filesize);
    NS_LOG_INFO("INFO : FILESIZE " << filesize);

    ifstream file;
    if (MaxNbFlows == 0) {
        //NS_LOG_INFO("Creating flows from file ./userdata/flows");
        string filename = "./userdata/flows";
        errno = 0;
        file.open(filename.c_str(), std::ios::in);
        if (file.fail()) {
            int tmp = errno;
            NS_FATAL_ERROR("File " << filename.c_str() <<
                           " not found: strerror(" << tmp << "): " << strerror(tmp));
        }
        if (!(file >> nbFlows)) {
            NS_FATAL_ERROR("File " << filename.c_str() <<
                           " not correctly formatted.");
        }
    } else {
        ofstream outFileFlow("./userdata/tetrys.flow");
        UniformVariable RandVariableNodes(0, c.GetN());
        while (nbFlows > 0) {
            uint32_t rand_src = 0;
            uint32_t rand_dst = 0;
            uint16_t rand_flow = 0;
            if (!file.fail() && !file.eof()) {
                int src = 0;
                int dst = 0;
                file >> src >> dst;
                rand_src = (uint32_t) src;
                rand_dst = (uint32_t) dst;
            } else {
                rand_src = RandVariableNodes.RandomVariable::GetInteger();
                rand_dst = RandVariableNodes.RandomVariable::GetInteger();
            }
            UniformVariable RandFlowID(0, UINT16_MAX);
            rand_flow = RandFlowID.RandomVariable::GetInteger();
            if (rand_src != rand_dst) {
                //                it_flow = find_if(flows.begin(), flows.end(), KeyEqualFlow(rand_src, rand_dst));
                //                it_src_flow = find_if(flows.begin(), flows.end(), KeyEqualSrcFlow(rand_src, rand_dst));
                //                it_dst_flow = find_if(flows.begin(), flows.end(), KeyEqualDstFlow(rand_src, rand_dst));
                //                if ((it_src_flow == flows.end()) && (it_dst_flow == flows.end())) {
                //                    it_src_flow = find_if(flows.begin(), flows.end(), KeyEqualSrcFlow(rand_dst, rand_src));
                //                    it_dst_flow = find_if(flows.begin(), flows.end(), KeyEqualDstFlow(rand_dst, rand_src));
                //                    if ((it_src_flow == flows.end()) && (it_dst_flow == flows.end())) {

                /* create flows */
                flows.push_back(make_pair(rand_src, rand_dst));
                NS_LOG_UNCOND("Flow between " << rand_src << " and " <<
                              rand_dst << " with ID " << rand_flow);
                outFileFlow << "Flow between " << rand_src << " and " <<
                        rand_dst << " with ID " << rand_flow << endl;

                /* Install TetrysProtocol application */
                for (uint32_t j = 0; j < tetrysApps.GetN(); j++) {
                    Ptr<TetrysProtocol> app = DynamicCast<TetrysProtocol > (tetrysApps.Get(j));
                    if (app->GetNode()->GetId() == rand_src)
                        app->installTetrysSourceFlow(app->GetNode(), tetrysApps.Get(rand_dst)->GetNode(),
                                                     rand_flow, filesize, Seconds(STARTING_TIME), filename, RedRatio, WindowSize);
                    else if (app->GetNode()->GetId() == rand_dst)
                        app->installTetrysSinkFlow(tetrysApps.Get(rand_src)->GetNode(), app->GetNode(),
                                                   rand_flow, filesize, Seconds(STARTING_TIME), Seconds(ACKFreq));

                }
                nbFlows--;
            }
        }
        outFileFlow.close();
        file.close();
    }

    // Scheduler
    Simulator::ScheduleNow(&CloseWhenFinished, tetrysApps); //Kill simulation when flows are finished
    Simulator::ScheduleNow(&processLinkEvents, events, c); // Event processor

    Simulator::Run();

    /* extract positions: */
    //NS_LOG_INFO("Extracting position, written to ./userdata/tetrys.pos");
    ofstream outFile("./userdata/tetrys.pos");
    for (NodeContainer::Iterator iter = c.Begin(); iter != c.End(); iter++) {
        Ptr<Node> node = *iter;
        Ptr<MobilityModel> mob = node->GetObject<MobilityModel > ();
        Vector pos = mob->GetPosition();
        Ptr<Ipv4L3Protocol> ipv4 = node->GetObject<Ipv4L3Protocol > ();
        Ipv4Address node_ipv4 = ipv4->GetAddress(1, 0).GetLocal();
        outFile << "POS: " << node->GetId() << " " << pos.x << " " << pos.y << " " << node_ipv4 << endl;
    }
    outFile.close();

    Simulator::Destroy();
    return 0;
}

/*
 ***************************************************************************
 ***************************************************************************
 ******************************** Utilities ***********************************
 ***************************************************************************
 ***************************************************************************
 */



static void EnableFilter(Ptr<Ipv4L3Protocol> l3, Ptr<Ipv4NfPacketFilter> f, Hook_t h) {
    l3->GetNetfilter().AppendNetfilterHook(h, f);
}



// Kill simulation if all flows are finished

void CloseWhenFinished(ApplicationContainer & tetrysApps) {
    bool finished = true;
    for (uint32_t j = 0; j < tetrysApps.GetN(); j++) {
        Ptr<TetrysProtocol> app = DynamicCast<TetrysProtocol > (tetrysApps.Get(j));
        if (app->isFinished() == false)
            finished = false;
    }
    if (Simulator::Now().GetSeconds() > MAX_SIMULATION_TIME) {
        NS_LOG_UNCOND("TIMEOUT ! Stopping everything. Execution Time :0.0");
        Simulator::Stop();
    }

    /* If not finished and no timeout, continue */
    if ((finished == false) && (Simulator::Now().GetSeconds() < MAX_SIMULATION_TIME)) {
        Time next = Time(Simulator::Now().GetSeconds() + 50000000);
        Simulator::Schedule(next, &CloseWhenFinished, tetrysApps);
    } else {
        //NS_LOG_UNCOND("Trying to stop the entire simulation");
        bool all_stopped = true;
        for (uint32_t j = 0; j < tetrysApps.GetN(); j++) {
            Ptr<TetrysProtocol> app = DynamicCast<TetrysProtocol > (tetrysApps.Get(j));
            if (app->isApplicationStopped() == false) {
                all_stopped = false;
                app->StopApplication();
            }
        }
        if (all_stopped == true) {
            NS_LOG_UNCOND(Simulator::Now().GetSeconds() << " Stopping Simulation properly !");
            Simulator::Stop();
        } else {
            Time next = Time(Simulator::Now().GetSeconds() + 50000000);
            Simulator::Schedule(next, &CloseWhenFinished, tetrysApps);
        }
    }
}

vector < vector<bool> > readNxNMatrix(string adj_mat_file_name) {
    ifstream adj_mat_file(adj_mat_file_name.c_str());
    errno = 0;
    if (adj_mat_file.fail()) {
        int tmp = errno;
        NS_FATAL_ERROR("File " << adj_mat_file_name.c_str() << " not found: strerror(" << tmp << "): " << strerror(tmp));
    }
    vector < vector<bool> > array;
    int i = 0;
    int n_nodes = 0;

    while (!adj_mat_file.eof()) {
        string line;
        getline(adj_mat_file, line);
        if (line == "") {
            NS_LOG_WARN("WARNING: Ignoring blank row in the array: " << i);
            break;
        }

        istringstream iss(line);
        bool element;
        vector<bool> row;
        int j = 0;

        while (iss >> element) {
            row.push_back(element);
            j++;
        }

        if (i == 0)
            n_nodes = j;

        if (j != n_nodes) {
            NS_LOG_ERROR("ERROR: Number of elements in line " << i << ": " << j << " not equal to number of elements in line 0: " << n_nodes);
            NS_FATAL_ERROR("ERROR: The number of rows is not equal to the number of columns! in the adjacency matrix");
        } else
            array.push_back(row);

        i++;
    }
    if (i != n_nodes) {

        NS_LOG_ERROR("There are " << i << " rows and " << n_nodes << " columns.");
        NS_FATAL_ERROR("ERROR: The number of rows is not equal to the number of columns! in the adjacency matrix");
    }
    adj_mat_file.close();
    return array;
}

vector<vector<double> > readCordinatesFile(string node_coordinates_file_name) {
    ifstream node_coordinates_file;
    node_coordinates_file.open(node_coordinates_file_name.c_str(), std::ios::in);
    if (node_coordinates_file.fail())
        NS_FATAL_ERROR("File " << node_coordinates_file_name.c_str() << " not found");

    vector<vector<double> > coord_array;
    int m = 0;

    while (!node_coordinates_file.eof()) {
        string line;
        getline(node_coordinates_file, line);

        if (line == "") {
            NS_LOG_WARN("WARNING: Ignoring blank row: " << m);
            break;
        }

        istringstream iss(line);
        double coordinate;
        vector<double> row;
        int n = 0;
        while (iss >> coordinate) {
            row.push_back(coordinate);
            n++;
        }

        if (n != 2) {
            NS_LOG_ERROR("ERROR: Number of elements at line#" << m << " is " << n << " which is not equal to 2 for node coordinates file");
            exit(1);
        } else
            coord_array.push_back(row);
        m++;
    }
    node_coordinates_file.close();
    return coord_array;
}

void printMatrix(const char* description, vector < vector<bool> > array) {
    cout << "**** Start " << description << "********" << endl;
    for (size_t m = 0; m < array.size(); m++) {

        for (size_t n = 0; n < array[m].size(); n++)
            cout << array[m][n] << ' ';
        cout << endl;
    }
    cout << "**** End " << description << "********" << endl;
}

void printCoordinateArray(const char* description, vector<vector<double> > coord_array) {
    cout << "**** Start " << description << "********" << endl;
    for (size_t m = 0; m < coord_array.size(); m++) {
        for (size_t n = 0; n < coord_array[m].size(); n++)
            cout << coord_array[m][n] << ' ';
        cout << endl;
    }
    cout << "**** End " << description << "********" << endl;
}
