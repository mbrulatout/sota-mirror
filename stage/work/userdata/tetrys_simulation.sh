#!/bin/bash
scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
name="$(date +%Y-%m-%d-%H-%M-%S)"
res="$scriptDir/results/$name"
pushd "$scriptDir/.." > /dev/null
prog="$(pwd)"
popd > /dev/null

#Colors for fun


default='\e[0m'
bold='\e[1m'
normal='\e[21m'
red='\e[31m'
green='\e[32m'
yellow='\e[33m'
blue='\e[94m'
purple='\e[95m'
cyan='\e[36m'

echo 
echo -e "${green}${bold}***************************************"
echo -e "***************************************"
echo -e "********** Simulation script **********"
echo -e "***************************************"
echo -e "***************************************${default}"
echo 
noask=0
draw=0
real=0
send=0
parallel=0
noask=0
version=-1
loss=0
nocolors=0
#############
## OPTIONS ##
#############

for param in $* ; do
    case $param in
	"--help" )
	    echo -e "${cyan}Help :"
	    echo -e "1 - The script is run by default with a cleaning safety. Result directory will be removed only if you say so. To remove this safety, run the script using the ${bold}\"--no-ask\"${normal} option."
	    echo -e "2 - To improve the script's execution time, you can type ${bold}\"sudo nice --20 $0\"${normal}."
	    echo -e "3 - To run large tests, run the script using the ${bold}\"--real\"${normal} option."
	    echo -e "4 - To run small tests, run the script using the ${bold}\"--test\"${normal} option (default)."
	    echo -e "5 - To run multiple runs simultaneously, run the script using the ${bold}\"--parallel\"${normal} option. This is much faster but combined with a niceness of -20, you will not be able to use your computer."
	    echo -e "6 - To run a specific Tetrys version, provide the protocol version as an option such as ${bold}\"--default\"${normal}, ${bold}\"--adaptative\"${normal}(default version), ${bold}\"--mobile\"${normal} or ${bold}\"--adaptativemobile\"${normal}"
	    echo -e "7 - To run this script with a specific error model, you can provide one of these options : ${bold}\"--uniform\"${normal}, ${bold}\"--random\"${normal}, ${bold}\"--sinus\"${normal} (both are generated using ${bold}\"the loss_generation.sh script\"${normal} or ${bold}\"--burst\"${normal}"
	    echo -e "8 - To set the color option off (if you cannot see this message in cyan, you should), use ${bold}"--nocolors"${normal}"
	    echo -e "Example : For an uniform error model, adaptative Tetrys using the large set of runs running in parallel, type : ${bold}\"$0 --draw --real --parallel --adaptative --uniform\"${default}"
	    exit 0 ;;
	#Set of tests
	'--real' )
	    real=1;;
	'--test' )
	    real=0;;
	#Misc
	'--no-ask' )
	    noask=1;;
	'--draw' )
	    draw=1;;
	'--send' )
	    send=1;;
	'--nocolors' )
	    nocolors=1;;
	'--parallel' )
	    parallel=1;;
	#Tetrys version
	'--default' ) 
	    version=0;;
	'--adaptative' )
	    version=1;;
	'--mobile' )
	    version=2;;
	'--adaptativemobile' )
	    version=3;;
	#Loss model
	'--uniform' )
	    loss=0;;
	'--random' )
	    loss=1;;
	'--sinus' )
	    loss=2;;
	'--burst' )
	    loss=3;;
	* )
	    echo -e "${red}Unknown parameter ${bold}$param${default}"
	    exit -1 ;;
    esac;
done;
echo -e "${yellow}Type $0 --help to get information about the different options available.${default}"
echo
umask 022


if [[ $nocolors == 1 ]] ; then
    default=''
    bold=''
    normal=''
    red=''
    green=''
    yellow=''
    blue=''
    purple=''  
fi;

#####################
## TETRYS VERSIONS ##
#####################

vartype="TETRYS_TYPE"
varname="TETRYS_NAME"
prottype=("TETRYS_DEFAULT" "TETRYS_ADAPTATIVE" "TETRYS_MOBILE" "TETRYS_MOBILE_ADAPTATIVE")
protname=("Tetrys default" "Tetrys with Adaptative Redundancy" "Tetrys with Mobile Window" "Tetrys with Adaptative Redundancy and Mobile Window")

#Adaptative Tetrys is default version
if [[ $version == -1 ]] ; then
    version=1 
fi;

#Create loss events for sinus and random loss error model.
if [[ $loss == 1 ]] ; then
    ./loss_generation.sh --random
fi;

if [[ $loss == 2 ]] ; then
    ./loss_generation.sh --sinus
fi;

#Do we have to change the protocol version in tetrys-utils.h file ?
grep -q "[ ]*//[ ]*#define TETRYS_TYPE ${prottype[$version]}" $prog/src/applications/model/tetrys-utils.h
if [[ $? == 0 ]] ; then
    #Wrong protocol version is set.
    #Comment all protocol versions.
    sed -i "s/^[[:blank:]]*#define $vartype/\/\/#define $vartype/g"  $scriptDir/../src/applications/model/tetrys-utils.h
    sed -i "s/^[[:blank:]]*#define $varname/\/\/#define $varname/g" $scriptDir/../src/applications/model/tetrys-utils.h
    #Uncomment wanted protocol version.
    sed -i "s/[[:blank:]]*\/\/[[:blank:]]*#define $varname \"${protname[$version]}\"$/#define $varname \"${protname[$version]}\"/g" $scriptDir/../src/applications/model/tetrys-utils.h
    sed -i "s/[[:blank:]]*\/\/[[:blank:]]*#define $vartype ${prottype[$version]}$/#define $vartype ${prottype[$version]}/g" $scriptDir/../src/applications/model/tetrys-utils.h
    echo "Changing tetrys-utils.h file. Compilation needed"
    #Compilation process
    echo ""
    tmpwd=$PWD
    cd $prog 
    ./waf 2>&1 > /dev/null
    cd $tmpwd
else
    #Just compile if needed
    cd $prog 
    ./waf 2>&1 > /dev/null
    cd $tmpwd
fi

############
## SAFETY ##
############
#useless
if [[ -d $res ]] ; then
    echo "Directory $res already exists and might contain other simulation results."
    if [[ $noask -ne 1 ]] ; then
	read -p "Do you want to clean the directory? (y/n) " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]] ; then
	    rm -rf $res
	    mkdir $res
	    echo "$res cleaned."
	else
	    echo 'Simulations aborted.'
	    exit 1
	fi
    else
	rm -rf $res
	mkdir $res
	echo "$res cleaned. (Safety disabled)"
    fi
else
    mkdir $res
    #$res
    echo -e  "${yellow}[INFO] $scriptDir/results/${bold}$name${normal} created.${default}"
fi

#Check if I'm root
if [[ $(whoami) != "root" ]] ; then
    echo 
    echo -e "${red}root didn't lauch this script. Since its execution can be fairly long, you can type \"${bold}sudo nice --20 $0${normal}\" to increase its CPU priority. Proper ownership will be restored at the end of the script.${default}"
fi

##############
# PARAMETERS #
##############

seeds=(1 13 42 256 1024 1337 1991 5555 65536 58008918)

if [[ $real -eq 1 ]] ; then
    if [[ $loss == 0 ]] ; then
	errorlist=(0.00 0.05 0.10 0.15 0.20 0.25 0.30)
	#errorlist=(0.00 0.05 0.10 0.15)
    else
	errorlist=(0.05)
    fi;
    if [[ $version == 0 || $version == 2 ]] ; then
	redlist=(0 5 10 15 20 25 30)
	#redlist=(10 15 20 25)
    else
	redlist=(30)
	#echo -e "${red}${bold}TEMPORARY HUGE REDLIST${default}"
	#redlist=(0 5 10 15 20 25 30)
    fi;
    nbruns=10
else
    # errorlist=(0.15 0.20 0.35)
    # redlist=(25 30)
    errorlist=(0.05)
    redlist=(1 10 20)
    nbruns=10
fi

lossmodel=("Uniform" "Random" "Sinus" "Burst")

linkdelaylist=("150ms")
linkratelist=("10Mbps")

nodes=2

filesize=1
prefix="./userdata/random"
suffix="mo.txt"
filename=$prefix$filesize$suffix

if [[ $nassim -eq 1 ]] ; then
    errorlist=(0.00 0.01 0.05 0.10 0.15 0.20)
    redlist=(0 5 10 15 20 25)
    nbruns=10
    linkdelaylist=("500ms")
    linkratelist=("500kbps")
    filename="./userdata/nassim.txt"
fi
echo -e "${green}Running ${bold}${protname[$version]}${normal} version with ${bold}${lossmodel[$loss]} ${normal}error model. ${default}"

nbsimu=$( echo ${#linkdelaylist[@]}*${#linkratelist[@]}*${#redlist[@]}*${#errorlist[@]}*$nbruns | bc )
#Save chosen parameters
for (( e=0; e < ${#errorlist[@]}; e++ )) ; do
    echo $(echo ${errorlist[$e]}*100 | bc ) >> $res/errorlist.tmp
done;
for (( r=0; r < ${#redlist[@]}; r++ )) ; do
    echo ${redlist[$r]} >> $res/redlist.tmp
done;
for (( ld=0; ld < ${#linkdelaylist[@]}; ld++ )) ; do
    echo ${linkdelaylist[$ld]} >> $res/linkdelaylist.tmp
done;
for (( lr=0; lr < ${#linkratelist[@]}; lr++ )) ; do
    echo ${linkratelist[$lr]} >> $res/linkratelist.tmp
done;

if [[ $parallel -eq 1 ]] ; then
    echo -e "${green}Running $nbsimu simulations ($nbruns simultaneously) on file ${bold}$filename${normal} with $nodes nodes${default}"
else
    echo -e "${green}Running $nbsimu simulations ($nbruns for each set of parameters) on file $filename with $nodes nodes${default}"
fi;
##############
# SIMULATION #
##############
simu=1
for lr in ${linkratelist[@]} ; do
    for ld in ${linkdelaylist[@]} ; do
	for e in ${errorlist[@]} ; do
	    for r in ${redlist[@]} ; do
		tmpwd=$PWD
		cd $prog 
		if [[ $parallel -eq 1 ]] ; then
		    #Parallel
		    for run in `seq 1 $nbruns` ; do
			echo -e "${purple}Run $run/$nbruns ($simu/$nbsimu), link rate $lr, link delay $ld, error $e, red $r${default}"
			((simu++))
			./waf --run "tetrys-scenario --MaxNbFlows=1 --numNodes=$nodes --wireless=0 --LinkRate=$lr -LinkDelay=$ld --ErrorRate=$e --RedRatio=$r --seed=${seeds[$run - 1]} --LossModel='${lossmodel[$loss]}' --filename='$filename'" > $res/temp-$ld-$lr-$e-$r-$run.tmp 2>&1 &
		    done;
		    wait
		    for run in `seq 1 $nbruns` ; do
			cp $res/temp-$ld-$lr-$e-$r-$run.tmp $res/ack-$ld-$lr-$e-$r-$run.tmp
			cat $res/temp-$ld-$lr-$e-$r-$run.tmp >> $res/tmp-$ld-$lr-$e-$r.tmp
		    done;
		    cd $tmpwd
		    unset tmpwd
		    
		else
		    # Sequential
		    for run in `seq 1 $nbruns` ; do
			toilet -f mono12 --gay "$simu/$nbsimu"
			tmpwd=$PWD
			cd $prog
			#echo "Run $run/$nbruns ($simu/$nbsimu), link rate $lr, link delay $ld, error $e, red $r "
			((simu++))
			./waf --run "tetrys-scenario --MaxNbFlows=1 --numNodes=$nodes --wireless=0 --LinkRate=$lr -LinkDelay=$ld --ErrorRate=$e --RedRatio=$r --seed=${seeds[$run - 1]} --LossModel='${lossmodel[$loss]}' --filename='$filename'" > $res/temp-$ld-$lr-$e-$r-$run.tmp 2>&1
			cp $res/temp-$ld-$lr-$e-$r-$run.tmp $res/ack-$ld-$lr-$e-$r-$run.tmp
			cat $res/temp-$ld-$lr-$e-$r-$run.tmp >> $res/tmp-$ld-$lr-$e-$r.tmp	
			cd $tmpwd
			unset tmpwd
		    done;
		fi;
	    done;
	done;
    done;
done;
cd $scriptDir
#########################
# SIMULATION PARAMETERS #
#########################

info=`cat $res/temp-$ld-$lr-$e-$r-$run.tmp | grep 'INFO' | cut -d: -f2 | cut -d' ' -f2-`
echo "$info" >> $res/info
echo "nbruns : $nbruns" >> $res/info
echo "LOSS MODEL : ${lossmodel[$loss]}" >> $res/info

####################
#### STATISTICS ####
####################

echo -e "${blue}Computing execution time statistics${default}"
min=99999999
for lr in ${linkratelist[@]} ; do
    for ld in ${linkdelaylist[@]} ; do
	for e in ${errorlist[@]} ; do
	    error=$(echo $e*100 | bc)
	    for r in ${redlist[@]} ; do

		##############
		# TIME STATS #
		##############

		mean=`cat $res/tmp-$ld-$lr-$e-$r.tmp | grep 'Source' | grep 'Execution' |cut -d: -f2 | awk '{x+=$1;next}END{print x/NR}'`
		echo "$r $mean" >> $res/redvstime-$ld-$lr-$e.tmp
		echo "$error $mean" >> $res/errorvstime-$ld-$lr-$r.tmp
		# var/(FNR - 1) to get the sample standard deviation
		# var/(FNR) to get the population standard deviation
		#avg-stddev min max avg+stddev avg
		stats=`cat $res/tmp-$ld-$lr-$e-$r.tmp | grep 'Source' | grep 'Execution' |cut -d: -f2 | awk 'BEGIN {i=1};  {tab[i] = $1; i++; next}; END {avg = 0; var=0 ; min = tab[1] ; max = tab[1]; for(j = 1; j <= FNR; j ++){avg = avg + tab[j]; if(min > tab[j]){min = tab[j]} if(max < tab[j]){max = tab[j]} }; avg = avg / (FNR); for(j = 1; j <= FNR; j ++){tab_e[j] = (tab[j] - avg)^2}; for(j = 1;j <=FNR; j ++){var += tab_e[j]}; var = var/(FNR - 1); stddev = sqrt(var); inter = (1.96 * stddev)/sqrt(FNR) ; {print avg-inter " " min " " max " " avg+inter " " avg}}'`
		
		echo "$r $stats" >> $res/redvstime-$ld-$lr-$e.test
		echo "$error $stats" >> $res/errorvstime-$ld-$lr-$r.test
	    done;
	done;
    done;
done;

#################
###### ACK ######
#################
echo -e "${blue}Computing ack and decoding statistics${default}"

for lr in ${linkratelist[@]} ; do
    for ld in ${linkdelaylist[@]} ; do
	index=0 #for meanpacket file name
	for e in ${errorlist[@]} ; do
	    error=$(echo $e*100 | bc )
	    for r in ${redlist[@]} ; do
		for run in `seq 1 $nbruns` ; do

		    ################
		    # PACKET STATS #
		    ################
		    stats=`cat $res/ack-$ld-$lr-$e-$r-$run.tmp | grep 'STAT :' | cut -d: -f2- | awk 'BEGIN {ORS="<br>";} {print;}'`
		    echo "$stats" > $res/stats-$ld-$lr-$e-$r-$run
		    ############
		    # OVERHEAD #
		    ############
		    overhead=`cat $res/stats-$ld-$lr-$e-$r-$run | grep "OVERHEAD" | cut -d: -f9 | cut -d' ' -f2`
		    echo "$overhead" >> $res/overhead-$ld-$lr
		    #############
		    # ACK STATS #
		    #############
		    acktimevsseqno=`cat $res/ack-$ld-$lr-$e-$r-$run.tmp | grep 'ACK :' | cut -d: -f1,2 | tr -d 'ACK :'`
		    echo "$acktimevsseqno" > $res/timevsseqno-$ld-$lr-$e-$r-$run
		    
		    #If adaptative redundancy
		    if [[ $version == 1 || $version == 3 ]] ; then 
			#########################
			# ADAPTATIVE ESTIMATION #
			#########################
			#time errorratio
			errorestimation=`cat $res/ack-$ld-$lr-$e-$r-$run.tmp | grep 'ACK :' | cut -d: -f1,3 | tr -d 'ACK :' | awk '{printf("%0.1f %0.1f\n",$1,$2*100);next;}'`
			echo "$errorestimation" > $res/errorestimation-$ld-$lr-$e-$r-$run
			# time red k
			redundancyestimation=`cat $res/ack-$ld-$lr-$e-$r-$run.tmp | grep 'REDUNDANCY :' | cut -d: -f1,2,3 | tr -d 'REDUNDANCY :' | awk '{printf("%0.1f %d %d\n",$1,$2,$3);next;}'`
			echo "$redundancyestimation" > $res/redestimation-$ld-$lr-$e-$r-$run
			
			#Error calculation
			#Useless, almost duplicate
			#errorcalc=`cat $res/ack-$ld-$lr-$e-$r-$run.tmp | grep 'ERROR ESTIMATION :' | cut -d: -f1,2 | tr -d 'ERROR ESTIMATION :' | awk '{printf("%0.1f %0.1f\n",$1,$2);next;}'`
			#echo "$errorcalc" >> $res/errorcalc-$ld-$lr-$e-$r-$run
		    fi;
		    
		    ##################
		    # DECODING STATS #
		    ##################
		    #&& ( $version == 1 || $version == 3 )
		    if [ $(echo "$e > 0.0" | bc) == 1 ] ; then
			#############
			# HISTOGRAM #
			#############
			histo=`cat $res/ack-$ld-$lr-$e-$r-$run.tmp | grep 'REPAIR MATRIX' | cut -d: -f3 | awk 'BEGIN {i=1}; {tab[i] = $1; i++; next}; END {max = 0; for(j = 1; j <= FNR ; j++) {histo[tab[j]] = 0; if(tab[j] > max) {max = tab[j]}}; for(j = 1; j <= FNR; j++){histo[tab[j]]++;}; for(j = 1; j <= max; j++) {if(histo[j] > 0 ) {print j " " histo[j] " " j/5} }  }'`
			echo "$histo" > $res/histo-$ld-$lr-$e-$r-$run
			
			#decoding over time
			timesize=`cat $res/ack-$ld-$lr-$e-$r-$run.tmp | grep 'REPAIR MATRIX' | cut -d: -f1,3 | tr -d ':' | awk '{printf("%0.1f %d\n",$1,$2);}'`
			echo "$timesize" >> $res/timedecoding-$ld-$lr-$e-$r-$run
			# Decoding operations number
			decodingop=`cat $res/histo-$ld-$lr-$e-$r-$run | awk 'BEGIN {op=0 ; num=0}; {num+=$2; op+= $1 * $1 * $2}; END { print op/num}'`
			echo $decodingop >> $res/decoding-$ld-$lr-$e-$r
		    fi;
		    ################
		    # SENT PACKETS #
		    ################
		    packetnumber=`cat $res/ack-$ld-$lr-$e-$r-$run.tmp | grep 'PKT NUMBER' | cut -d: -f3`
		    echo "$packetnumber" >> $res/packet-$ld-$lr-$e-$r
		done;#nbruns
		
		#####################
		# MEAN SENT PACKETS #
		#####################
		meanpkt=`cat $res/packet-$ld-$lr-$e-$r | awk '{x+=$1;next}END{print x/NR}'`
		echo "$r $meanpkt" >> $res/packet-$ld-$lr-$index
		
		if (( $(bc <<< "$e > 0") == 1 )) ; then
		    ####################
		    # GLOBAL HISTOGRAM #
		    ####################
		    #Unused

		    # matrixglobal=`cat $res/tmp-$ld-$lr-$e-$r.tmp | grep 'REPAIR MATRIX' | cut -d: -f2`
		    # echo "$matrixglobal" > $res/matrixraw-$ld-$lr-$e-$r
		    # histoglobal=`cat $res/matrixraw-$ld-$lr-$e-$r | awk 'BEGIN {i=1}; {tab[i] = $1; i++; next}; END {max = 0; for(j = 1; j <= FNR ; j++) {histo[tab[j]] = 0; if (max < tab[j]) {max=tab[j]}}; for(j = 1; j <= FNR; j++){histo[tab[j]]++;}; for(j = 1; j <= max; j++) { if(histo[j] > 0){ print j " " histo[j] " " j/5}}  }'`
		    # echo "$histoglobal" > $res/histo-$ld-$lr-$e-$r
		    # rm -f $res/matrixraw-$ld-$lr-$e-$r
		    
		    #################
		    # MEAN DECODING #
		    #################
		    meandecoding=`cat $res/decoding-$ld-$lr-$e-$r | awk '{x+=$1;next}END{print x/NR}'`
		    echo "$r $meandecoding" >> $res/decoding-$ld-$lr-$index
		fi;
	    done;
	    (( index++ ))
	done;
    done;
done;

for lr in ${linkratelist[@]} ; do
    for ld in ${linkdelaylist[@]} ; do
	meanoverhead=`cat $res/overhead-$ld-$lr | awk '{x+=$1} END{print  x/FNR}'`
	echo "$meanoverhead" > $res/meanoverhead
    done;
done;

#################
##### ERROR #####
#################
echo -e "${blue}Formatting error statistics${default}"
for lr in ${linkratelist[@]} ; do
    for ld in ${linkdelaylist[@]} ; do
	#Superposition
	for (( e=0; e<${#errorlist[@]}; e++ )) ; do
	    paste $res/redvstime-$ld-$lr-${errorlist[$e]}.tmp $res/errorlist.tmp > $res/redvstime-$ld-$lr-$e
	done;
    done;
done;

#################
## REDUNDANCY ###
#################
echo -e "${blue}Formatting redundancy statistics.${default}"
for lr in ${linkratelist[@]} ; do
    for ld in ${linkdelaylist[@]} ; do
	#Superposition
	for (( r=0; r<${#redlist[@]}; r++ )) ; do
	    paste $res/errorvstime-$ld-$lr-${redlist[$r]}.tmp $res/redlist.tmp > $res/errorvstime-$ld-$lr-$r
	done;
    done;
done;

echo -e "${yellow}[INFO] Archive available at $scriptDir/archive/$name${default}"
cp -r $res $scriptDir/archive/$name

###########
## CHOWN ##
###########
if [ "$(ps aux | grep "$0" | head -n1 | cut -d' ' -f1)" == "root" ] ; then
    user=`who | cut -d" " -f1 | head -n1`
    chown -R $user:$user $res
    chown -R $user:$user $scriptDir/archive/$name
    echo -e "${yellow}[INFO] Script launched as ${bold}root${normal}, $res has been chowned to ${bold}$user${normal} user and group.${default}"
fi

#############
## DRAWING ##
#############
if [[ $draw -eq 1 ]] ; then
    ./tetrys_results.sh $res $version $loss
else
    rm -rf $res
    echo -e "${red}You should run ${bold}tetrys_results.sh${normal} to create graphics on computed stats. $res folder has been removed.${default}"
fi

if [[ $send -eq 1 ]] ; then
    ./poststats.sh
fi
echo -e "${green}End of simulation script.${default}"
exit 0
