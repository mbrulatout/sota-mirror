#!/bin/bash

scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
res="$scriptDir/results"
file=index.html

echo "Creating remote $file"
rm -f $res/$file
rm -f $scriptDir/$file

echo "<html><body>" >> $scriptDir/$file
echo "<h1>Results Main Page</h1><br><br>" >> $scriptDir/$file
echo "Welcome. To access simulation results, go <a href=\"results\"> here </a>" >> $scriptDir/$file
echo "</body></html>" >> $scriptDir/$file

echo "<html><body>" >> $res/$file
echo "<h1>Results Main Page</h1><br><br>" >> $res/$file
echo "You can find simulations results in separate folders listed below.<br><br><br>"  >> $res/$file

echo "Searching for drawn simulations"
for dir in `find $res -maxdepth 1 -type d | sort` ; do
	if ! [ $dir == $res ] ; then
		if [ -f $dir/index.html ] ; then #Drawn simulation
			echo "Will send $dir"
			echo "&emsp;<a href=\"${dir#$res/}/\">${dir#$res/}/</a><br>"  >> $res/$file
		fi
	fi
done
echo "</body></html>" >> $res/$file

echo "File $file successfully generated : "
#cat $res/$file


FTP="/usr/bin/ncftpput"
AUTHFILE="/root/.myupload"

myf="ftp.magix-online.com"
myu="mathias.brulatout@gmail.com"
if [ -f $AUTHFILE ] ; then 
  # use the file for auth
  CMD="$FTP -m -R -f $AUTHFILE $myf $remotedir $localdir"
else
	echo
	echo "Connection parameters :"
	echo "server : $myf"
	echo "username : $myu"
	read -p "Do you want to keep these parameters? (y/n) " -n 1 -r
	echo 
	if ! [[ $REPLY =~ ^[Yy]$ ]] ; then
		read -p "Enter FTP server name : " myf
		read -p "Enter FTP username : " myu
	fi

	read -s -p "Enter $myu's password : " myp
	echo 
  read -p "Enter FTP remote directory [/] : " remotedir
  read -p "Enter local directory to upload [results/] : " localdir
  [ "$remotedir" == "" ] && remotedir="/" || :
  [ "$localdir" == "" ] && localdir="./results/" || :
  CMD="$FTP -m -R -F -z -u $myu -p $myp $myf $remotedir $localdir"
fi

#cat $scriptDir/$file
echo
echo "Move some file to send only relevant files"
for dir in `find $res -maxdepth 1 -type d | sort` ; do
	if ! [ $dir == $res ] ; then
		if [ -f $dir/index.html ] ; then #Drawn simulation
			tmp=$scriptDir/${dir#$res/}
			mkdir -p $tmp/
			cd $dir
			mv `ls $dir | egrep -v 'index*'` $tmp/
			echo "Files to transfer for directory ${dir#$res/} : $(ls -l $dir/ | wc -l)"
		else
			#Remove useless simulations
			rm -rf $dir
		fi;
	fi;
done;
echo
cd $scriptDir


$CMD

echo
echo "Move files to where they belong"
for dir in `find $res -maxdepth 1 -type d | sort` ; do
	if ! [ $dir == $res ] ; then
		if [ -f $dir/index.html ] ; then #Drawn simulation
			tmp=$scriptDir/${dir#$res/}
			if [ `ls -l $tmp/ | wc -l` -gt 1 ] ; then
				#echo "not empty"
				cp $tmp/* $dir
				#echo "Files in directory : $(ls -l $dir/ | wc -l)"
			fi;
		   	rm -rf $tmp/
		fi;
	fi;
done;

echo "End of script"
