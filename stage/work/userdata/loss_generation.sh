#!/bin/bash

#Create error rate events stored in $file every second until $MAX_TIME.
#Can create random or sinusoidal error rate whether --random or --sinus is provided to the script. 

MAX_TIME=150
MAX_RATE=0.2
file=events

scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

rm -f $scriptDir/$file

function sortarray  { for i in ${array[@]}; do echo "$i"; done | sort -n; }

law=0

for param in $* ; do
    case $param in 
	'--help' )
	    echo 'Known options : "--square" and "--sinus" to generate specific loss ratio events.'
	    exit -1;;
	'--random' )
	    law=0;;
	'--sinus' )
	    law=1;;
	* )
	    echo 'Unknown option ! '
	    exit -1;;
    esac
done;


echo "" >> $scriptDir/$file
echo "$MAX_TIME" >> $scriptDir/$file

mod=`echo "$MAX_RATE*1000" | bc `
for run in `seq 1 $MAX_TIME` ; do
    if [[ $law == 0 ]] ; then
	error=`echo "$RANDOM%${mod%.*}" | bc | awk '{printf("%.2f",$1/1000);}' ` 
    else
	error=`echo "$run $MAX_RATE" | awk '{printf("%0.2f",(sin($1/2)+1) /2 *$2);}' `
    fi;
    echo "$run 0 1 500kbps 150ms $error" >> $scriptDir/$file
done
#sorted=( $(sortarray) )


if [[ $law == 0 ]] ; then
    echo "\"$file\" file generated with random error rate (up to $MAX_RATE)"
else
    echo "\"$file\" file generated with sinusoidal error rate (up to $MAX_RATE)"
fi;
exit 0
