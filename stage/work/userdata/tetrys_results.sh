 #!/bin/bash

 ####################
 ## INITIALIZATION ##
 ####################
 
 scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
 res="$scriptDir/results/2014-08-21-18-59-33/"
 pushd "$scriptDir/.." > /dev/null
 prog="$(pwd)"
 popd > /dev/null

 default='\e[0m'
bold='\e[1m'
normal='\e[21m'
red='\e[31m'
green='\e[32m'
yellow='\e[33m'
blue='\e[94m'
purple='\e[95m'
cyan='\e[36m'

 umask 022 
 echo 
 echo -e "${green}${bold}*************************************"
 echo -e "*************************************"
 echo -e "********** Plotting script **********"
 echo -e "*************************************"
 echo -e "*************************************${default}"
 ##############
 # PARAMETERS #
 ##############
 
version=0
loss=0
protname=("Tetrys default" "Tetrys with Adaptative Redundancy" "Tetrys with Mobile Window" "Tetrys with Adaptative Redundancy and Mobile Window")
lossmodel=("Uniform" "Random" "Sinus" "Burst")
echo
 
 if [ $# == 3 ] && [ -d $1 ] ; then
	 res=$1
	 version=$2
	 loss=$3
	 echo -e "${yellow}[INFO] Running $0 script on directory $res. ${default}"
	 echo -e "${yellow}[INFO] ${bold}${protname[$version]}${normal} with ${bold}${lossmodel[$loss]}${normal} loss model. ${default}"
 else
	 if [[ -d $res ]] ; then
		 echo -e "${yellow}[INFO] Drawing default simulation at $res${default}"
	 else
		 echo -e "${red}Please provide an existing directory.${default}"
		 exit -1
	 fi
 fi
 
 echo -e "${yellow}[INFO] Removing png file and index.html${default}"
 rm -f $res/index*
 echo
 echo -e "${yellow}[INFO] Plotting stats with parameters taken out from simulation script : ${default}"

 eval errorlist=($(cat $res/errorlist.tmp | awk '{ printf("%4.2f " ,$0/100) }' ))
 eval redlist=($(cat $res/redlist.tmp))
 nbruns=$(cat $res/info | grep "nbruns" | cut -d: -f2 | awk '{ printf("%d ",$0) }')
 eval linkdelaylist=($(cat $res/linkdelaylist.tmp))
 eval linkratelist=($(cat $res/linkratelist.tmp))
 
 echo -e "${yellow}[INFO] Error values : ${bold}${errorlist[*]}${normal}"
 echo -e "[INFO] Inline redundancy values : ${bold}${redlist[*]}${normal}" 
 echo -e "[INFO] Runs per simulation : ${bold}$nbruns${normal} ${default}"
 #For legend purpose
 eval errorlistprint=( $(cat $res/errorlist.tmp | awk '{ printf("%d ",$0) }'))
 
 nodes=2


 ##################
 #### TOPOLOGY ####
 ##################
 echo -e "${blue}Drawing topology${default}"

 cat $scriptDir/tetrys.pos | awk 'BEGIN{print "graph{"; print "graph [size=\"8,5\"];"; print "node [label=\"\\N\", pin=true, shape=circle];";};{print $2 " [pos=\"" $3 "," $4 "\", pin=true, style=filled];"}' > $scriptDir/topo.dot
 awk '{for(i=0;i<NF;i++){if($i==1){print NR " " i}}}' $scriptDir/adjacency_matrix$nodes.txt | awk '{if($1>$2){print $1-1 " -- " $2-1 ";"}};END{print "}"}' >> $scriptDir/topo.dot
 dot -Kneato -Tpng -n $scriptDir/topo.dot -o $scriptDir/index-topo.png
 mv $scriptDir/topo.dot $res/
 mv $scriptDir/index-topo.png $res/
 echo "<html><body><b>Topology:</b><br><center><img src='./index-topo.png'/></center><br>" >> $res/index.html

 ##############
 #### INFO ####
 ##############
 
 ackfreq=`cat $res/info | grep 'ACK_FREQ' | cut -d' ' -f2`
 cbrfreq=`cat $res/info | grep 'CBR_FREQ' | cut -d' ' -f2`
 packetsize=`cat $res/info | grep 'PACKET_SIZE' | cut -d' ' -f2`
 protocol=`cat $res/info | grep 'PROT' | cut -d' ' -f2-`
 headersize=`cat $res/info | grep 'HEADER_SIZE' | cut -d' ' -f2`
 filesize=`cat $res/info | grep 'FILESIZE' | cut -d' ' -f2 | awk '{x=$0 / 1024 /1024} END {print x}'`
 rate=`echo "($packetsize + $headersize + 20.) * 8 / $cbrfreq / 1024." | bc`
 errorestimation=`cat $res/info | grep 'ERROR_ESTIMATION_FREQ' | cut -d' ' -f2`
lossmodel=`cat $res/info | grep 'LOSS MODEL' | cut -d: -f2 | awk '{print tolower($0)}'`
alpha=`cat $res/info | grep 'ALPHA' | cut -d' ' -f2`
 echo "<font size=\"5\"><b>Simulation parameters :</b></font><br><br>" >> $res/index.html
echo "<b>Link parameters :</b><br>" >> $res/index.html
 echo "&emsp;Link rate : ${linkratelist[*]} <br>" >> $res/index.html
 echo "&emsp;Link delay : ${linkdelaylist[*]} <br>" >> $res/index.html
echo "&emsp;Loss model : <b>$lossmodel</b><br><br>" >> $res/index.html
echo "<b>Flow parameters :</b><br>" >> $res/index.html
 echo "&emsp;CBR frequency : ${cbrfreq}s <br>" >> $res/index.html
 echo "&emsp;Tetrys header size : $headersize bytes <br>" >> $res/index.html
 echo "&emsp;Payload size : $packetsize bytes <br>" >> $res/index.html
 echo "&emsp;Flow rate : $rate kbps  (at IP level)<br><br>" >> $res/index.html
 echo "<b>Tetrys parameters :</b><br>" >> $res/index.html
 echo "&emsp;Protocol : <b> $protocol </b> <br>"  >> $res/index.html
 echo "&emsp;Ack frequency : ${ackfreq}s <br>" >> $res/index.html
 if [[ $version == 1 || $version == 3 ]] ; then
     echo "&emsp;Error estimation frequency : ${errorestimation}s<br>" >> $res/index.html
     echo "&emsp;Alpha for Exponential Moving Average : $alpha <br>" >> $res/index.html
     echo "<br><b><font color=\"color:red\">WARNING ! This file was generated from a script. Plot scripts are always the same. However, this protocol version provides an adaptative redundancy feature making the INLINE REDUNDANCY PARAMETER USELESS.</font> Since the same set of random seeds is provided for each error rate and inline redundancy ratio pair, results should be EXACTLY the same for any inline redundancy ratio value and the same error rate.</b>" >> $res/index.html
fi
 
echo $lossmodel | grep -q "uniform"
if [[ $? != 0 ]] ; then
echo "<br><b><font color=\"color:red\">WARNING ! This file was generated from a script. Plot scripts are always the same. However, this simulation set uses a non-uniform error ratio  making the ERROR RATIO PARAMETER USELESS.</font> Since the same set of random seeds is provided for each error rate and inline redundancy ratio pair, results should be EXACTLY the same for any error rate and the same inline redundancy ratio.</b>" >> $res/index.html
fi

 ####################

 ####################
 min=9999
 max=0
 for lr in ${linkratelist[@]} ; do
	 for ld in ${linkdelaylist[@]} ; do
		 for e in ${errorlist[@]} ; do
			 for r in ${redlist[@]} ; do
				 if [ -f $res/tmp-$ld-$lr-$e-$r.tmp ] ; then
					 mean=`cat $res/tmp-$ld-$lr-$e-$r.tmp | grep 'Source' | grep 'Execution' |cut -d: -f2 | awk '{x+=$1;next}END{print x/NR}'`
					 file=`cat $res/tmp-$ld-$lr-$e-$r.tmp | grep 'Source' | grep 'Execution' |cut -d: -f2`
					 minmax=`cat $res/tmp-$ld-$lr-$e-$r.tmp | grep 'Source' | grep 'Execution' |cut -d: -f2 | awk 'BEGIN {min=9999;max=0} {if(min > $1){min=$1;}if(max < $1){max=$1;};next}END{printf("%0.1f %0.1f",min,max);}'`
					 tmp=`echo $minmax | cut -d' ' -f2`
					 if [ $(echo " $max < $tmp" | bc) == 1 ] ; then
						 max=$tmp
					 fi
					 tmp=`echo $minmax | cut -d' ' -f1`
					 if [ $(echo " $min > $tmp" | bc) == 1 ] ; then
						 min=$tmp
					 fi
				 fi
			 done;
		 done;
	 done;
 done;

 echo -e "${purple}Minimal simulation time $min"
 echo -e "Maximal simulation time $max${default}"
 echo "<br><br>" >> $res/index.html

 ################
 ##### HTML #####
 ################

 echo -e "${blue}Creating html table${default}"
 echo "<br><hr><br><b>Statistics for each error and redundancy ratios.</b><br><br><br>" >> $res/index.html
 #CSS
 echo "<style media=\"screen\" type=\"text/css\"> " >> $res/index.html
 echo "td, th { " >> $res/index.html
 echo "text-align : center;" >> $res/index.html
 echo "}" >> $res/index.html
 echo "</style>" >> $res/index.html
 
 echo "<table align=\"center\" border=\"1\">" >> $res/index.html
 echo "<tr><th rowspan=\"2\" colspan=\"2\"></th><th colspan=\"${#redlist[@]}\">Inline redundancy in %</th>" >> $res/index.html
 echo "<tr>" >> $res/index.html
 for r in ${redlist[@]} ; do
	 echo "<th>$r</th>" >> $res/index.html
 done;
 echo "</tr>" >> $res/index.html
 for e in ${errorlist[@]} ; do
	 error=$(echo $e*100 | bc  | awk '{printf "%d", $0}')
	 echo "<tr>" >> $res/index.html
	 if [[ $(echo "$e == ${errorlist[0]}" | bc) -eq 1 ]] ; then
		 echo "<th rowspan=\"${#errorlist[@]}\">Error <br> rate <br> in %</th>" >> $res/index.html 
	 fi
	 echo "<th>$error</th>"  >> $res/index.html
	 for r in ${redlist[@]} ; do
		 echo "<td><a href=\"index-$e-$r.html\">Here</a></td>" >> $res/index.html
		 echo "<h2>Result page for simulations with $error % of error and $r % of inline redundancy.</h2>" >> $res/index-$e-$r.html
	 done;
	 echo "</tr>"  >> $res/index.html
 done;
 echo "</table>"  >> $res/index.html


 ######################################
 ##########                  ##########
 ########## index-$e-$r.html ##########
 ##########                  ##########
 ######################################


 #################
 ##### STATS #####
 #################
 echo -e "${blue}Writing runs stats.${default}"
 for lr in ${linkratelist[@]} ; do
	 for ld in ${linkdelaylist[@]} ; do
		 for e in ${errorlist[@]} ; do
			 for r in ${redlist[@]} ; do
				 for run in `seq 1 $nbruns` ; do
					 echo "<br><br><b>Statistics for run $run </b><br>" >> $res/index-$e-$r.html
					 cat $res/stats-$ld-$lr-$e-$r-$run >> $res/index-$e-$r.html
				 done;
			 done;
		 done;
	 done;
 done;

 #################
 ###### ACK ######
 #################
 echo -e "${blue}Drawing ack and decoding statistics.${default}"
 #echo "<br><hr><br><b>Ack and Matrix Statistics</b><br>" >> $res/index.html
 for lr in ${linkratelist[@]} ; do
	 for ld in ${linkdelaylist[@]} ; do
		 if [ `find $res -name "errorvstime-$ld-$lr*" | wc -l` -ne 0 ] ; then
			 for e in ${errorlist[@]} ; do
				 error=$(echo $e*100 | bc  | awk '{printf "%d", $0}')
				 for r in ${redlist[@]} ; do
					 echo "<br><br><hr><b>Ack and Matrix statistics</b><br>" >> $res/index-$e-$r.html
    gnuplot <<EOF
#set terminal png truecolor
set terminal svg dynamic enhanced
set output '$res/index-timevsseqno-$ld-$lr-$e-$r.svg'

set key inside left top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
set title "$filesize MB file with $error % of error and $r % of inline redundancy"
set ylabel "Ack seqno"
set xlabel 'Ack time'

set size 0.8,0.8
set xrange [:$max]
set xtics out
set ytics out
#unset border
plot for [i=1:$nbruns] '$res/timevsseqno-$ld-$lr-$e-$r-'.i title 'Run '.i with fsteps lw 4
EOF
# 					 gnuplot <<EOF
#  set terminal png truecolor  
# set output '$res/index-timevsseqno-$ld-$lr-$e-$r.png'
# set key inside left top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
# set size 1,1
# set title "$filesize MB file with $error % of error and $r % of inline redundancy"
# set xrange [:$max]
# set ylabel "Ack seqno"
# set xlabel 'Ack time'
# plot for [i=1:$nbruns] '$res/timevsseqno-$ld-$lr-$e-$r-'.i title ' Run '.i with fsteps      
# EOF
					 echo "<img src='./index-timevsseqno-$ld-$lr-$e-$r.png'/> <br><br>" >> $res/index-$e-$r.html
					 
					 ####################
					 ##### DECODING #####
					 ####################
					 
					 if (( $(bc <<< "$e > 0") )) ; then
					     for run in `seq 1 $nbruns` ; do
#DONOTREMOVE COMMENT ________________________
#__________________________________
					# 		 gnuplot <<EOF
# set terminal png truecolor  
# set output '$res/index-histo-$ld-$lr-$e-$r-$run.png'
# set key inside right top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
# set title "$filesize MB file with $error % of error and $r % of inline redundancy"
# set ylabel 'Number of decoding'
# set xlabel 'Matrix size'
# set size 1,1
# set style fill solid border -1
# set logscale x
# set offset 0,0,1,0
# set xrange[1:]
# set yrange[0:]
# set xtics nomirror rotate by -90 font "Verdana,8"
# plot '$res/histo-$ld-$lr-$e-$r-$run' using 1:2:3:xtic(1) title 'Run $run' with boxes
# EOF
# 							 echo "<img src='./index-histo-$ld-$lr-$e-$r-$run.png'/>" >> $res/index-$e-$r.html
							 
							 

  gnuplot <<EOF
#set terminal png truecolor  
set terminal svg dynamic enhanced
set output '$res/index-timedecoding-$ld-$lr-$e-$r-$run.svg'

set key inside left top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
set title "$filesize MB file with $error % of error and $r % of inline redundancy"
set y2label 'Ack seqno'
set ylabel 'Matrix size'
set xlabel 'Time '

set y2tics nomirror
set ytics nomirror
set xtics nomirror
set yrange[0:]
set size 0.8,0.8
set style fill solid border -1
#unset border
set xtics out
set ytics out
set y2tics out
plot '$res/timedecoding-$ld-$lr-$e-$r-$run' u 1:2 title 'Decoded matrix size' w impulses axes x1y1 lw 4, '$res/timevsseqno-$ld-$lr-$e-$r-$run' title 'ACK seqno' with fsteps axes x1y2 lw 4
EOF
							 #  gnuplot <<EOF
# set terminal png truecolor  
# set output '$res/index-timedecoding-$ld-$lr-$e-$r-$run.png'
# set key inside left top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
# set title "$filesize MB file with $error % of error and $r % of inline redundancy"
# set y2tics nomirror
# set ytics nomirror
# set xtics nomirror
# set y2label 'Ack seqno'
# set ylabel 'Matrix size'
# set xlabel 'Time '
# set yrange[0:]
# set size 1,1
# set style fill solid border -1
# plot '$res/timedecoding-$ld-$lr-$e-$r-$run' u 1:2 title 'Decoded matrix size' w impulses axes x1y1, '$res/timevsseqno-$ld-$lr-$e-$r-$run' title 'ACK seqno' with fsteps axes x1y2
# EOF
							 echo "<img src ='./index-timedecoding-$ld-$lr-$e-$r-$run.png'/>" >> $res/index-$e-$r.html

							 #if adaptative redundancy, draw it !
							 if [[ $version == 1 || $version == 3 ]] ; then
	gnuplot <<EOF
#set terminal png truecolor
set terminal svg dynamic enhanced
set output '$res/index-errorestimation-$ld-$lr-$e-$r-$run.svg'

set key inside right top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
 set title "$filesize MB file with $error % of error"
set y2label 'Interval between coded packets'
 set ylabel 'Estimation in %'
 set xlabel 'Time'

set y2tics nomirror
set ytics nomirror
set xtics nomirror
set y2range[0:]
set size 0.8,0.8
set style fill solid border -1
set pointsize 0
set xtics out
set ytics out
set y2tics out
#unset border
plot  '$res/redestimation-$ld-$lr-$e-$r-$run' u 1:2 title 'Redundancy'  w l axes x1y1 lw 3, '$res/redestimation-$ld-$lr-$e-$r-$run' u 1:3 title 'k value' w l axes x1y2 lw 2 , '$res/errorestimation-$ld-$lr-$e-$r-$run'  title 'Error rate in ACK' w l axes x1y1 lw 2
EOF
							# 	  gnuplot <<EOF
# set terminal png truecolor  
# set output '$res/index-errorestimation-$ld-$lr-$e-$r-$run.png'
# set key inside right top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
# set title "$filesize MB file with $error % of error"
# set y2tics nomirror
# set ytics nomirror
# set xtics nomirror
# set y2range[0:]
# set y2label 'Interval between coded packets'
# set ylabel 'Estimation in %'
# set xlabel 'Time'
# set size 1,1
# set style fill solid border -1
# set pointsize 0
# plot  '$res/redestimation-$ld-$lr-$e-$r-$run' u 1:2 title 'Redundancy' w l axes x1y1,  '$res/redestimation-$ld-$lr-$e-$r-$run' u 1:3 title 'k value' w l axes x1y2 , '$res/errorestimation-$ld-$lr-$e-$r-$run' title 'Error rate in ACK' w l axes x1y1
# EOF

								  echo "<img src='./index-errorestimation-$ld-$lr-$e-$r-$run.png'/><br><br>" >> $res/index-$e-$r.html  
							 fi
						 done;
					 fi;
				 done;
			 done;
		 fi;
	 done;
 done;
 
 ######################################
 ##########                  ##########
 ##########    index.html    ##########
 ##########                  ##########
 ######################################

 #################
 #### PACKET #####
 #################

 # if [[ $version == 0 || $version == 2 ]] ; then 
 echo "<br><hr><br><b>Packet and decoding operations statistics</b><br>" >> $res/index.html
 echo -e "${blue}Drawing packet and decoding operations statistics.${default}"
 for lr in ${linkratelist[@]} ; do
     for ld in ${linkdelaylist[@]} ; do
	 if [ `find $res -name "packet-$ld-$lr*" | wc -l` -ne 0 ] ; then

	gnuplot <<EOF
#set terminal png truecolor  
set terminal svg dynamic enhanced
set output '$res/index-packet-$ld-$lr.svg'

set key inside left top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
set title "$filesize MB file"
set ylabel "Amount of sent packet"
set xlabel 'Inline redundancy ratio in %'

set size 0.8,0.8
set xtics out
set ytics out
#unset border
 list="${errorlistprint[*]}"
 item(n) = word(list,n)
plot for [w = 0 : ${#errorlist[@]} - 1] '$res/packet-$ld-$lr-'.w title item(w+1).'% of error' w lp lw 4
EOF
	 #     gnuplot <<EOF
# #set terminal png truecolor  
# set terminal svg dynamic enhanced
# set output '$res/index-packet-$ld-$lr.svg'

# set key inside right bottom vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
# set size 1,1
# set title "$filesize MB file"
# set ylabel "Amount of sent packet"
# set xlabel 'Inline redundancy ratio'
# list="${errorlistprint[*]}"
# item(n) = word(list,n)
# plot for [w = 0 : ${#errorlist[@]} - 1] '$res/packet-$ld-$lr-'.w title item(w+1).'% of error' with linespoints
# EOF
		 echo "<img src='./index-packet-$ld-$lr.png'/>" >> $res/index.html
		 	gnuplot <<EOF
#set terminal png truecolor  
set terminal svg dynamic enhanced
set output '$res/index-decoding-$ld-$lr.svg'

set key inside left bottom vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
set title "$filesize MB file"
set ylabel "Mean decoding operations"
set xlabel 'Inline redundancy ratio in %'

set size 0.8,0.8
set logscale y
set xtics out
set ytics out
#unset border
 list="${errorlistprint[*]}"
 item(n) = word(list,n)
plot for [w = 1 : ${#errorlist[@]} - 1] '$res/decoding-$ld-$lr-'.w title item(w+1).'% of error' w lp lw 4
EOF
	# 	 gnuplot <<EOF
# # set terminal png truecolor  
# set terminal svg dynamic enhanced
# set output '$res/index-decoding-$ld-$lr.svg'

# set key inside left bottom vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
# set title "$filesize MB file"
# set ylabel "Mean decoding operations"
# set xlabel 'Inline redundancy ratio'

# set size 1,1
# set logscale y
# list="${errorlistprint[*]}"
# item(n) = word(list,n)
# plot for [w = 0 : ${#errorlist[@]} - 1] '$res/decoding-$ld-$lr-'.w title item(w+1).'% of error' with linespoints
# EOF
		 echo "<img src='./index-decoding-$ld-$lr.png'/>" >> $res/index.html
	 fi;
     done;
 done;
 
 
     #################
     ##### ERROR #####
     #################
 if [[ $loss == 0 ]] ; then 
     #Unform loss model (parameter taken in count
     echo "<br><hr><br><b>Error statistics</b><br>" >> $res/index.html
     echo -e "${blue}Drawing error statistics.${default}"
     for lr in ${linkratelist[@]} ; do
	 for ld in ${linkdelaylist[@]} ; do
	     if [ `find $res -name "errorvstime-$ld-$lr*" | wc -l` -ne 0 ] ; then
		 for e in ${errorlist[@]} ; do
		     error=$(echo $e*100 | bc  | awk '{printf "%d", $0}')
		     if [ -f $res/redvstime-$ld-$lr-$e.test ]; then
			   gnuplot <<EOF
#set terminal png truecolor     
set terminal svg dynamic enhanced
set output '$res/index-redvstime-$ld-$lr-$e-test.svg'

set key inside bottom right vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
 set title "$filesize MB file with $error % of error"
 set ylabel "Flow duration"
 set xlabel "Inline redundancy ratio in %"

set size 0.8,0.8
set style fill empty
set auto fix
set offsets graph 0.0, 0.1
set yrange [$min:$max + ($max - $min)/10]
set xrange [:$4]
set boxwidth 0.4
set xtics out
set ytics out
#unset border
plot "$res/redvstime-$ld-$lr-$e.test" using 1:2:3:4:5 title "95% confidence interval" with candlesticks whiskerbars lw 2, "" using 1:6:6:6:6 with candlesticks notitle lw 2 , "" using 1:6 with linespoints title "Mean value" lw 2
EOF
			#  gnuplot <<EOF
# set terminal png truecolor     
# set key inside bottom right vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
# set output '$res/index-redvstime-$ld-$lr-$e-test.png'
# set size 1,1
# set title "$filesize MB file with $error % of error"
# set ylabel "Flow duration"
# set xlabel "Inline redundancy ratio in %"
# set style fill empty
# set auto fix
# set offsets graph 0.0, 0.1
# set yrange [$min:$max + ($max - $min)/10]
# set xrange [:$4]
# set boxwidth 0.4
# plot "$res/redvstime-$ld-$lr-$e.test" using 1:2:3:4:5 title "95% confidence interval" with candlesticks whiskerbars , "" using 1:6:6:6:6 with candlesticks notitle , "" using 1:6 with linespoints title "Mean value"
# EOF
			 echo "<img src='./index-redvstime-$ld-$lr-$e-test.png'/>" >> $res/index.html
		     fi;
		 done;

		  gnuplot <<EOF
#set terminal png truecolor     
set terminal svg dynamic enhanced
set output '$res/index-redvstime-$ld-$lr.svg'

set key inside right bottom 
set title "$filesize MB file with all error rates"
set ylabel "Flow duration"
set xlabel "Inline redundancy ratio in %"

set size 0.8,0.8
set xtics out
set ytics out
#unset border
 list="${errorlistprint[*]}"
 item(n) = word(list,n)
plot for [w = 0 : ${#errorlist[@]} - 1] '$res/redvstime-$ld-$lr-'.w title item(w+1).'% of error' with linespoints lw 4
EOF
		 # gnuplot <<EOF
# set terminal png truecolor     
# set key inside right bottom 
# set output '$res/index-redvstime-$ld-$lr.png'
# set size 1,1
# set title "$filesize MB file with all error rates"
# set ylabel "Flow duration"
# set xlabel "Inline redundancy ratio in %"
# list="${errorlistprint[*]}"
# item(n) = word(list,n)
# plot for [w = 0 : ${#errorlist[@]} - 1] '$res/redvstime-$ld-$lr-'.w title item(w+1).'% of error' with linespoints
# EOF
		 echo "<br><br><b>Superposition des courbes avec erreur fixe et redondance variable</b><br>" >> $res/index.html
		 echo "<img src='./index-redvstime-$ld-$lr.png'/>" >> $res/index.html
	     fi;
	 done;
     done;
 fi;
     #########################
     ### INLINE REDUNDANCY ###
     #########################
 if [[ $version == 0 || $version == 2 ]] ; then 
     echo "<br><hr><br><b>Inline redundancy statistics</b><br>" >> $res/index.html
     echo -e "${blue}Drawing inline redundancy statistics.${default}"
     for lr in ${linkratelist[@]} ; do
	 for ld in ${linkdelaylist[@]} ; do
	     if [ `find $res -name "errorvstime-$ld-$lr*" | wc -l` -ne 0 ] ; then
		 for r in ${redlist[@]} ; do
		     if [ -f $res/errorvstime-$ld-$lr-$r.test ]; then

 gnuplot <<EOF
#set terminal png truecolor     
set terminal svg dynamic enhanced
set output '$res/index-errorvstime-$ld-$lr-$r-test.svg'

 set key inside bottom right vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
 set title "$filesize MB file with $r % of inline redundancy"
 set ylabel "Flow duration"
 set xlabel "Error ratio in %"

set size 0.8,0.8
set yrange [$min:$max+($max-$min)/10]
set style fill empty
set auto fix
set boxwidth 0.4
set xtics out
set ytics out
#unset border
plot "$res/errorvstime-$ld-$lr-$r.test" using 1:2:3:4:5 title "95% confidence interval" with candlesticks whiskerbars lw 3, "" using 1:6:6:6:6 with candlesticks notitle lw 2 , "" using 1:6 title "Mean value" w lp lw 2
EOF
		# 	 gnuplot <<EOF
# set terminal png truecolor     
# set key inside bottom right vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000
# set output '$res/index-errorvstime-$ld-$lr-$r-test.png'
# set size 1,1
# set title "$filesize MB file with $r % of inline redundancy"
# set ylabel "Flow duration"
# set xlabel "Error ratio in %"
# set yrange [$min:$max+($max-$min)/10]
# set style fill empty
# set auto fix
# set boxwidth 0.4
# plot "$res/errorvstime-$ld-$lr-$r.test" using 1:2:3:4:5 title "95% confidence interval" with candlesticks whiskerbars , "" using 1:6:6:6:6 with candlesticks notitle , "" using 1:6 with linespoints title "Mean value"
# EOF
			 echo "<img src='./index-errorvstime-$ld-$lr-$r-test.png'/>" >> $res/index.html
		     fi;
		 done;

		  gnuplot <<EOF
#set terminal png truecolor  
set terminal svg dynamic enhanced
set output '$res/index-errorvstime-$ld-$lr.svg'

 set key inside right bottom 
set title "$filesize MB file with all inline redundancy ratios"
 set ylabel "Flow duration"
 set xlabel "Error ratio in %"

set size 0.8,0.8
set yrange [$min:$max]
set xrange [:$4]
set xtics out
set ytics out
#unset border
 list="${redlist[*]}"
 item(n) = word(list,n)
plot for [w = 0 : ${#redlist[@]} - 1] '$res/errorvstime-$ld-$lr-'.w title item(w+1).'% of redundancy' with linespoints lw 4
EOF
	# 	 gnuplot <<EOF
# set terminal png truecolor    
# set key inside right bottom 
# set output '$res/index-errorvstime-$ld-$lr.png'
# set size 1,1
# set title "$filesize MB file with all inline redundancy ratios"
# set ylabel "Flow duration"
# set xlabel "Error ratio in %"
# set yrange [$min:$max]
# set xrange [:$4]

# list="${redlist[*]}"
# item(n) = word(list,n)
# plot for [w = 0 : ${#redlist[@]} - 1] '$res/errorvstime-$ld-$lr-'.w title item(w+1).'% of redundancy' with linespoints
# EOF
		 echo "<br><br><b>Superposition des courbes avec redondance fixe et erreur variable</b><br>" >> $res/index.html
		 echo "<img src='./index-errorvstime-$ld-$lr.png'/>" >> $res/index.html
	     fi;
	 done;
     done;
 fi;


 echo "</body></html>" >> $res/index.html

 ################
 ##### MISC #####
 ################
 
 #CHOWN/CHMOD
 if [ "$(ps aux | grep "$0" | head -n1 | cut -d' ' -f1)" == "root" ] ; then
	 user=`who | cut -d" " -f1 | head -n1`
	 chown -R $user:$user $res
	 echo -e "${yellow}[INFO] Script launched as ${bold}root${normal}, $res has been chowned to ${bold}$user${normal} user and group.${default}"
 fi

 #echo "Cleaning results directory"
 #find $res -type f -not -name "index*" -delete
 #cd $res/
 #rm `ls | egrep -v 'index*'`
 #cd -
 echo -e "${green}End of drawing script.${default}"
