#!/bin/bash

#NON-PORTABLE (need gnuplot 5.0 rc1 to draw heatmaps)

scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
res="$scriptDir/results"
pushd "$scriptDir/.." > /dev/null
prog="$(pwd)"
popd > /dev/null

paper="$scriptDir/../../../paper/real/images"
echo "WARNING : This script will save images to $paper, where the article should be !"

echo "Default uniform 15% error 0 15 20 30 % redundancy"
temparray=(0 15 20 30)
for redvalue in ${temparray[@]} ; do
    gnuplot <<EOF
set terminal svg dynamic enhanced
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set xtics out
set ytics out
unset border
set output '$paper/defaultuniform15-$redvalue.svg'
set y2tics nomirror
set ytics nomirror
set xtics nomirror
set xlabel "Time (in seconds)"
set ylabel "Decoded matrix size (in packets)"
set y2label "Ack sequence number"
set yrange[0:]
set size 0.6,0.6
plot '$res/defaultuniform/timedecoding-150ms-500kbps-0.15-${redvalue}-1' u 1:2 notitle w impulses axes x1y1 lw 5, '$res/defaultuniform/timevsseqno-150ms-500kbps-0.15-${redvalue}-1' notitle with fsteps axes x1y2 lw 5
EOF
done;

echo "Adaptative uniform 1"
gnuplot <<EOF
set terminal svg dynamic enhanced
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set xtics out
set ytics out
unset border
set output '$paper/adaptativeuniform1.svg'
set y2tics nomirror
set ytics nomirror
set xtics nomirror
set xlabel "Time (in seconds)"
set ylabel "Decoded matrix size (in packets)"
set y2label "Ack sequence number"
set yrange[0:]
set size 0.6,0.6
plot '$res/adaptativeuniform/timedecoding-150ms-500kbps-0.10-0-1' u 1:2 notitle w impulses axes x1y1 lw 5, '$res/adaptativeuniform/timevsseqno-150ms-500kbps-0.10-0-1' notitle with fsteps axes x1y2 lw 5
EOF
echo "Adaptative uniform 2"
gnuplot <<EOF
set terminal svg dynamic enhanced
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set xtics out
set ytics out
set y2tics out
unset border
set output '$paper/adaptativeuniform2.svg'
set y2tics nomirror
set ytics nomirror
set xtics nomirror
set xlabel "Time (in seconds)"
set ylabel "Redundancy and error ratios (in %)"
set y2label "Value of k (in packets)"
set yrange[0:]
set y2range[0:]
set size 0.6,0.6
set pointsize 0
plot  '$res/adaptativeuniform/redestimation-150ms-500kbps-0.10-0-1' u 1:2 notitle  w l axes x1y1 lw 5, '$res/adaptativeuniform/redestimation-150ms-500kbps-0.10-0-1' u 1:3 notitle w l axes x1y2 lw 5 , '$res/adaptativeuniform/errorestimation-150ms-500kbps-0.10-0-1' notitle w l axes x1y1 lw 5
EOF


echo "Adaptative sinus 1"
gnuplot <<EOF
set terminal svg dynamic enhanced
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set xtics out
set ytics out
unset border
set output '$paper/adaptativesinus1.svg'
set y2tics nomirror
set ytics nomirror
set xtics nomirror
set xlabel "Time (in seconds)"
set ylabel "Decoded matrix size (in packets)"
set y2label "Ack sequence number"
set yrange[0:]
set size 0.6,0.6
plot '$res/adaptativesinus/timedecoding-150ms-500kbps-0.15-20-1' u 1:2 notitle w impulses axes x1y1 lw 5, '$res/adaptativesinus/timevsseqno-150ms-500kbps-0.15-20-1' notitle with fsteps axes x1y2 lw 5
EOF
echo "Adaptative sinus 2"
gnuplot <<EOF
set terminal svg dynamic enhanced
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set xtics out
set ytics out
set y2tics out
unset border
set output '$paper/adaptativesinus2.svg'
set y2tics nomirror
set ytics nomirror
set xtics nomirror
set xlabel "Time (in seconds)"
set ylabel "Redundancy and error ratios (in %)"
set y2label "Value of k (in packets)"
set y2range[0:]
set size 0.6,0.6
set pointsize 0
plot  '$res/adaptativesinus/redestimation-150ms-500kbps-0.15-20-1' u 1:2 notitle  w l axes x1y1 lw 5, '$res/adaptativesinus/redestimation-150ms-500kbps-0.15-20-1' u 1:3 notitle w l axes x1y2 lw 5 , '$res/adaptativesinus/errorestimation-150ms-500kbps-0.15-20-1' notitle w l axes x1y1 lw 5
EOF

echo "Adaptative Burst 1"
gnuplot <<EOF
set terminal svg dynamic enhanced
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set xtics out
set ytics out
unset border
set output '$paper/adaptativeburst1.svg'
set y2tics nomirror
set ytics nomirror
set xtics nomirror
set xlabel "Time (in seconds)"
set ylabel "Decoded matrix size (in packets)"
set y2label "Ack sequence number"
set yrange[0:]
set size 0.6,0.6
plot '$res/adaptativeburst/timedecoding-150ms-500kbps-0.05-30-5' u 1:2 notitle w impulses axes x1y1 lw 5, '$res/adaptativeburst/timevsseqno-150ms-500kbps-0.05-30-5' notitle with fsteps axes x1y2 lw 5
EOF
echo "Adaptative Burst 2"
gnuplot <<EOF
set terminal svg dynamic enhanced
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set xtics out
set ytics out
set y2tics out
unset border
set output '$paper/adaptativeburst2.svg'
set y2tics nomirror
set ytics nomirror
set xtics nomirror
set xlabel "Time (in seconds)"
set ylabel "Redundancy and error ratios (in %)"
set y2label "Value of k (in packets)"
set y2range[0:]
set size 0.6,0.6
set style fill solid border -1
set pointsize 0
plot  '$res/adaptativeburst/redestimation-150ms-500kbps-0.05-30-5' u 1:2 notitle  w l axes x1y1 lw 5, '$res/adaptativeburst/redestimation-150ms-500kbps-0.05-30-5' u 1:3 notitle w l axes x1y2 lw 5 , '$res/adaptativeburst/errorestimation-150ms-500kbps-0.05-30-5' notitle w l axes x1y1 lw 5
EOF

echo "Adaptative Mobile Burst 1"
gnuplot <<EOF
set terminal svg dynamic enhanced
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set xtics out
set ytics out
unset border
set output '$paper/mobileadaptativeburst1.svg'
set y2tics nomirror
set ytics nomirror
set xtics nomirror
set xlabel "Time (in seconds)"
set ylabel "Decoded matrix size (in packets)"
set y2label "Ack sequence number"
set yrange[0:]
set size 0.6,0.6
plot '$res/mobileadaptativeburst/timedecoding-150ms-10Mbps-0.05-30-10' u 1:2 notitle w impulses axes x1y1 lw 5, '$res/mobileadaptativeburst/timevsseqno-150ms-10Mbps-0.05-30-10' notitle with fsteps axes x1y2 lw 5
EOF
echo "Adaptative Mobile Burst 2"
gnuplot <<EOF
set terminal svg dynamic enhanced
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set xtics out
set ytics out
set y2tics out
unset border
set output '$paper/mobileadaptativeburst2.svg'
set y2tics nomirror
set ytics nomirror
set xtics nomirror
set xlabel "Time (in seconds)"
set ylabel "Redundancy and error ratios (in %)"
set y2label "Value of k (in packets)"
set y2range[0:]
set size 0.6,0.6
set pointsize 0
plot  '$res/mobileadaptativeburst/redestimation-150ms-10Mbps-0.05-30-10' u 1:2 notitle  w l axes x1y1 lw 5, '$res/mobileadaptativeburst/redestimation-150ms-10Mbps-0.05-30-10' u 1:3 notitle w l axes x1y2 lw 5 , '$res/mobileadaptativeburst/errorestimation-150ms-10Mbps-0.05-30-10' notitle w l axes x1y1 lw 5
EOF


resrtt=$res/paperRTT
resfack=$res/paperFACK2
resper=$res/paperBURST
#########################
#### RTT SIMULATIONS ####
#########################

echo "Running simulations RTT/encoding window simulations (if needed) !"

echo "Storing simulations results at $resrtt"
mkdir -p $resrtt
#rm -rf $resrtt/*
rttarray=("50ms" "100ms" "150ms" "200ms" "250ms" "300ms" "350ms" "400ms" "450ms" "500ms")
warray=(30 60 90 120 150 180 210 240 270 300)
runs=10

nbsimus=`echo "${#rttarray[@]} * ${#warray[@]}" | bc`
simu=1
seeds=(1 13 42 256 1024 1337 1991 5555 65536 58008918)
for rtt in ${rttarray[@]} ; do
    for w in ${warray[@]} ; do
	restmp=$resrtt/$rtt-$w
	mkdir -p $restmp
	tmpwd=$PWD
	cd $prog
		
	#Run simulation if needed
	filenumber=`ls -l $restmp | wc -l`
	if (( $filenumber != ( $runs + 1 ) )) ; then
	    rm -f $restmp/*
	    echo "[$simu/$nbsimus] Delay $rtt and encoding window $w."
	    for run in `seq 1 $runs` ; do
		./waf --run "tetrys-scenario --MaxNbFlows=1 --numNodes=2 --wireless=0 --LinkRate='10Mbps' -LinkDelay=$rtt --seed=${seeds[$run - 1]} --LossModel='Burst' --filename='./userdata/random1mo.txt' --EncodingWindow=$w" > $restmp/temp-$run.tmp 2>&1 &
	    done;	
	fi;
	
	((simu++))
	wait
	cd $tmpwd
    done;
done;


# Stats are quite long, uncomment if needed.

# echo "Computing RTT overhead matrices"
# rm -f $resrtt/overhead*
# rm -f $resrtt/decoding*
# rm -f $resrtt/size*
# for w in ${warray[@]} ; do
#     echo -n " ,$w" >> $resrtt/overhead-matrix
#     echo -n " ,$w" >> $resrtt/decoding-matrix
#     echo -n " ,$w" >> $resrtt/size-matrix
# done;
# echo "" >> $resrtt/overhead-matrix
# echo "" >> $resrtt/decoding-matrix
# echo "" >> $resrtt/size-matrix
# for rtt in ${rttarray[@]} ; do
#     figrtt=`echo "${rtt%%m*} * 2" | bc`
#     echo -n "$figrtt" >> $resrtt/overhead-matrix
#     echo -n "$figrtt" >> $resrtt/decoding-matrix
#     echo -n "$figrtt" >> $resrtt/size-matrix
#     for w in ${warray[@]} ; do
# 	restmp=$resrtt/$rtt-$w
# 	for run in `seq 1 $runs` ; do
# 	    overhead=`cat $restmp/temp-$run.tmp | grep "OVERHEAD PAPER" | cut -d: -f3- | awk '{printf("%0.1f",$1)}'`
# 	    echo "$overhead" >> $resrtt/overhead-$rtt-$w
	    
# 	    size=`cat $restmp/temp-$run.tmp | grep "MATRIX" | cut -d: -f3 | awk '{printf("%d\n",$1);}' | awk '{x+=$1} END {print x/FNR}'`
# 	    echo $size >> $resrtt/size-$rtt-$w
	    
# 	    sendtime=`cat $restmp/temp-$run.tmp | grep "LOG SEND PACKET" | cut -d: -f1,3 | tr -d ":" | awk '{printf("%0.3f %d\n",$1,$2)}'`
# 	    receivetime=`cat $restmp/temp-$run.tmp | grep "LOG RECEIVE PACKET" | cut -d: -f1,3 | tr -d ":" | awk '{printf("%0.3f %d\n",$1,$2)}'`
# 	    dectime=`echo -e "$receivetime\n$sendtime" | awk 'BEGIN{sum=0;max=0}; {if(tab_r[$2] == 0)tab_r[$2]=$1; else tab_s[$2]=$1 ; if(max < $2) max=$2;} END {for(i=1; i<=max;i++) { tab_t[i] = tab_r[i] - tab_s[i] ; sum+=tab_t[i];}print sum/max; }'`
# 	    #dectime=`cat $restmp/temp-$run.tmp | grep "Sink" | cut -d: -f2`
# 	    echo "$dectime" >> $resrtt/decoding-$rtt-$w
# 	done;
# 	meanoverhead=`cat $resrtt/overhead-$rtt-$w | awk '{x+=$1} END{print  x/FNR}'`
# 	meandecoding=`cat $resrtt/decoding-$rtt-$w | awk '{x+=$1} END{print  x/FNR}'`
# 	meansize=`cat $resrtt/size-$rtt-$w | awk '{x+=$1} END{print  x/FNR}'`
# 	echo "$rtt-$w = overhead $meanoverhead% : Mean delivery delay $meandecoding s : matrix size $size"
# 	echo -n ", $meanoverhead" >> $resrtt/overhead-matrix
# 	echo -n ", $meandecoding" >> $resrtt/decoding-matrix
# 	echo -n ", $meansize" >> $resrtt/size-matrix
#     done;
#     echo "" >> $resrtt/overhead-matrix
#     echo "" >> $resrtt/decoding-matrix
#     echo "" >> $resrtt/size-matrix
# done;
# rm -f $resrtt/decoding-*ms
# rm -f $resrtt/overhead-*ms
# rm -f $resrtt/size-*ms

echo "Draw overhead matrix !"
/home/tai/usr/bin/gnuplot <<EOF
set terminal svg dynamic enhanced size 600,480
set output '$resrtt/overhead.svg'
#set title "Overhead (in %)"
set size 0.5,0.5
set autoscale fix
set xtics nomirror rotate by -45
set xlabel "Encoding window size (in packets)"
set ylabel "RTT (in milliseconds)"
set datafile separator comma
set cblabel "Overhead in %"
plot '$resrtt/overhead-matrix' matrix rowheaders columnheaders using 1:2:3 notitle with image pixels
unset output
EOF
echo "Draw delay graph !"
/home/tai/usr/bin/gnuplot <<EOF
set terminal svg dynamic enhanced size 600,480
set output '$resrtt/decoding.svg'
set size 0.5,0.5
set autoscale fix
set xtics nomirror rotate by -45
#set title "Mean flow duration (in seconds)"
set xlabel "Encoding window size (in packets)"
set ylabel "RTT (in milliseconds)"
set datafile separator comma
set cblabel "Mean packet delay"
plot '$resrtt/decoding-matrix' matrix rowheaders columnheaders using 1:2:3 notitle  with image pixels
unset output
EOF
echo "Draw matrix size graph !"
/home/tai/usr/bin/gnuplot <<EOF
set terminal svg dynamic enhanced size 600,480
set output '$resrtt/size.svg'
set size 0.5,0.5
set autoscale fix
set xtics nomirror rotate by -45
#set title "Mean decoded matrix size (in packets)"
set xlabel "Encoding window size (in packets)"
set ylabel "RTT (in milliseconds)"
set datafile separator comma
set cblabel "Mean matrix size"
plot '$resrtt/size-matrix' matrix rowheaders columnheaders using 1:2:3 notitle  with image pixels
EOF

cp $resrtt/overhead.svg $paper/overhead.svg
cp $resrtt/decoding.svg $paper/decoding.svg
cp $resrtt/size.svg $paper/size.svg

#######################
#### ACK FREQUENCY ####
#######################
echo "Running simulations FACK/encoding window simulations (if needed)!"
#fackarray=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0)
fackarray=(0.1 0.3 0.5 0.7 0.9 1.1 1.3 1.5 1.7 1.9)
simu=1
echo "Storing at $resfack"
mkdir -p $resfack
for fack in ${fackarray[@]} ; do
    for w in ${warray[@]} ; do
	restmp=$resfack/$fack-$w
	mkdir -p $restmp
	tmpwd=$PWD
	cd $prog

	#Run simulation if needed
	filenumber=`ls -l $restmp | wc -l`
	if (( $filenumber != ( $runs + 1 ) )) ; then
	    rm -f $restmp/*
	    echo "[$simu/$nbsimus] ACK Freq $fack and encoding window $w."
	    for run in `seq 1 $runs` ; do
		./waf --run "tetrys-scenario --MaxNbFlows=1 --numNodes=2 --wireless=0 --LinkRate='10Mbps' -LinkDelay='150ms' --seed=${seeds[$run - 1]} --LossModel='Burst' --filename='./userdata/random1mo.txt' --EncodingWindow=$w --ACKFreq=$fack " > $restmp/temp-$run.tmp 2>&1 &
	    done;	
	fi;
	((simu++))
	wait
	cd $tmpwd
    done;
done;

# Stats are quite long, uncomment if needed.

#  echo "Computing FACK overhead matrices"
# rm -f $resfack/overhead*
# rm -f $resfack/decoding*
# rm -f $resfack/size*
# for w in ${warray[@]} ; do
#     echo -n " ,$w" >> $resfack/overhead-matrix
#     echo -n " ,$w" >> $resfack/decoding-matrix
#     echo -n " ,$w" >> $resfack/size-matrix
# done;
# echo "" >> $resfack/overhead-matrix
# echo "" >> $resfack/decoding-matrix
# echo "" >> $resfack/size-matrix   
# for fack in ${fackarray[@]} ; do
#      figfack=`echo "$fack * 1000" | bc | awk '{printf("%d",$1);}'`
#     echo -n "$figfack" >> $resfack/overhead-matrix
#     echo -n "$figfack" >> $resfack/decoding-matrix
#     echo -n "$figfack" >> $resfack/size-matrix
#     for w in ${warray[@]} ; do
# 	restmp=$resfack/$fack-$w
# 	for run in `seq 1 $runs` ; do
# 	     overhead=`cat $restmp/temp-$run.tmp | grep "OVERHEAD PAPER" | cut -d: -f3- | awk '{printf("%0.1f",$1)}'`
# 	    echo "$overhead" >> $resfack/overhead-$fack-$w
	    
# 	    size=`cat $restmp/temp-$run.tmp | grep "MATRIX" | cut -d: -f3 | awk '{printf("%d\n",$1);}' | awk '{x+=$1} END {print x/FNR}'`
# 	    echo $size >> $resfack/size-$fack-$w
	    
# 	    sendtime=`cat $restmp/temp-$run.tmp | grep "LOG SEND PACKET" | cut -d: -f1,3 | tr -d ":" | awk '{printf("%0.3f %d\n",$1,$2)}'`
# 	    receivetime=`cat $restmp/temp-$run.tmp | grep "LOG RECEIVE PACKET" | cut -d: -f1,3 | tr -d ":" | awk '{printf("%0.3f %d\n",$1,$2)}'`
# 	    dectime=`echo -e "$receivetime\n$sendtime" | awk 'BEGIN{sum=0;max=0}; {if(tab_r[$2] == 0)tab_r[$2]=$1; else tab_s[$2]=$1 ; if(max < $2) max=$2;} END {for(i=1; i<=max;i++) { tab_t[i] = tab_r[i] - tab_s[i] ; sum+=tab_t[i];}print sum/max; }'`
# 	    #dectime=`cat $restmp/temp-$run.tmp | grep "Sink" | cut -d: -f2`
# 	    echo "$dectime" >> $resfack/decoding-$fack-$w
# 	done;
# 	meanoverhead=`cat $resfack/overhead-$fack-$w | awk '{x+=$1} END{print  x/FNR}'`
# 	meandecoding=`cat $resfack/decoding-$fack-$w | awk '{x+=$1} END{print  x/FNR}'`
# 	meansize=`cat $resfack/size-$fack-$w | awk '{x+=$1} END{print  x/FNR}'`
# 	echo "$figfack-$w = overhead $meanoverhead% : Mean duration $meandecoding s : matrix size $size"
# 	echo -n ", $meanoverhead" >> $resfack/overhead-matrix
# 	echo -n ", $meandecoding" >> $resfack/decoding-matrix
# 	echo -n ", $meansize" >> $resfack/size-matrix
#     done;
#     echo "" >> $resfack/overhead-matrix
#     echo "" >> $resfack/decoding-matrix
#     echo "" >> $resfack/size-matrix
# done;
# rm -f $resfack/decoding-*.
# rm -f $resfack/overhead-*.
# rm -f $resfack/size-*.


echo "Draw overhead matrix !"
/home/tai/usr/bin/gnuplot <<EOF
set terminal svg dynamic enhanced size 600,480
set output '$resfack/overhead.svg'
set size 0.5,0.5
set autoscale fix
set xtics nomirror rotate by -45
#set title "RTT=300ms; Overhead (in %)"
set xlabel "Encoding window size (in packets)"
set ylabel "ACK interval (in milliseconds)"
set datafile separator comma
set cblabel "Overhead in %"
plot '$resfack/overhead-matrix' matrix rowheaders columnheaders using 1:2:3 notitle with image pixels
unset output
EOF
echo "Draw delay graph !"
/home/tai/usr/bin/gnuplot <<EOF
set terminal svg dynamic enhanced size 600,480
set output '$resfack/decoding.svg'
set size 0.5,0.5
set autoscale fix
set xtics nomirror rotate by -45
#set title "RTT=300ms; Mean flow duration (in seconds)"
set xlabel "Encoding window size (in packets)"
set ylabel "ACK interval (in milliseconds)"
set datafile separator comma
set cblabel "Mean packet delay"
plot '$resfack/decoding-matrix' matrix rowheaders columnheaders using 1:2:3 notitle  with image pixels
unset output
EOF
echo "Draw matrix size graph !"
/home/tai/usr/bin/gnuplot <<EOF
set terminal svg dynamic enhanced size 600,480
set output '$resfack/size.svg'
set size 0.5,0.5
set autoscale fix
set xtics nomirror rotate by -45
#set title "RTT=300ms; Mean decoded matrix size (in packets)"
set xlabel "Encoding window size (in packets)"
set ylabel "ACK interval (in milliseconds)"
set datafile separator comma
set cblabel "Mean matrix size"
plot '$resfack/size-matrix' matrix rowheaders columnheaders using 1:2:3 notitle  with image pixels
EOF

cp $resfack/overhead.svg $paper/overhead2.svg
cp $resfack/decoding.svg $paper/decoding2.svg
cp $resfack/size.svg $paper/size2.svg


####################
#### BURST SIZE ####
####################
echo "Running simulations Burst size/encoding window simulations (if needed)!"
burstarray=(5 10 15 20 25 30 35 40 45 50)
simu=1
echo "Storing at $resper"
mkdir -p $resper
for burst in ${burstarray[@]} ; do
    for w in ${warray[@]} ; do
	restmp=$resper/$burst-$w
	mkdir -p $restmp
	tmpwd=$PWD
	cd $prog

	#Run simulation if needed
	filenumber=`ls -l $restmp | wc -l`
	if (( $filenumber != ( $runs + 1 ) )) ; then
	    rm -f $restmp/*
	    echo "[$simu/$nbsimus] Burst  $burst and encoding window $w."
	    for run in `seq 1 $runs` ; do
		./waf --run "tetrys-scenario --MaxNbFlows=1 --numNodes=2 --wireless=0 --LinkRate='10Mbps' -LinkDelay='150ms' --seed=${seeds[$run - 1]} --LossModel='Burst' --filename='./userdata/random1mo.txt' --EncodingWindow=$w --BurstSize=$burst " > $restmp/temp-$run.tmp 2>&1 &
	    done;	
	fi;
	((simu++))
	wait
	cd $tmpwd
    done;
done;

# Stats are quite long, uncomment if needed.

# echo "Computing BURST overhead matrices"
# rm -f $resper/overhead*
# rm -f $resper/decoding*
# rm -f $resper/size*
# for w in ${warray[@]} ; do
#     echo -n " ,$w" >> $resper/overhead-matrix
#     echo -n " ,$w" >> $resper/decoding-matrix
#     echo -n " ,$w" >> $resper/size-matrix
# done;
# echo "" >> $resper/overhead-matrix
# echo "" >> $resper/decoding-matrix
# echo "" >> $resper/size-matrix   
# for burst in ${burstarray[@]} ; do
#     echo -n "$burst" >> $resper/overhead-matrix
#     echo -n "$burst" >> $resper/decoding-matrix
#     echo -n "$burst" >> $resper/size-matrix
#     for w in ${warray[@]} ; do
# 	restmp=$resper/$burst-$w
# 	for run in `seq 1 $runs` ; do
# 	     overhead=`cat $restmp/temp-$run.tmp | grep "OVERHEAD PAPER" | cut -d: -f3- | awk '{printf("%0.1f",$1)}'`
	     
# 	    size=`cat $restmp/temp-$run.tmp | grep "MATRIX" | cut -d: -f3 | awk '{printf("%d\n",$1);}' | awk '{x+=$1} END {print x/FNR}'`
	   
# 	    echo "$overhead" >> $resper/overhead-$burst-$w
# 	    echo $size >> $resper/size-$burst-$w

	    
# 	     sendtime=`cat $restmp/temp-$run.tmp | grep "LOG SEND PACKET" | cut -d: -f1,3 | tr -d ":" | awk '{printf("%0.3f %d\n",$1,$2)}'`
# 	    receivetime=`cat $restmp/temp-$run.tmp | grep "LOG RECEIVE PACKET" | cut -d: -f1,3 | tr -d ":" | awk '{printf("%0.3f %d\n",$1,$2)}'`
# 	    dectime=`echo -e "$receivetime\n$sendtime" | awk 'BEGIN{sum=0;max=0}; {if(tab_r[$2] == 0)tab_r[$2]=$1; else tab_s[$2]=$1 ; if(max < $2) max=$2;} END {for(i=1; i<=max;i++) { tab_t[i] = tab_r[i] - tab_s[i] ; sum+=tab_t[i];}print sum/max; }'`
# 	    #dectime=`cat $restmp/temp-$run.tmp | grep "Sink" | cut -d: -f2`
# 	    echo "$dectime" >> $resper/decoding-$burst-$w
# 	done;
# 	meanoverhead=`cat $resper/overhead-$burst-$w | awk '{x+=$1} END{print  x/FNR}'`
# 	meandecoding=`cat $resper/decoding-$burst-$w | awk '{x+=$1} END{print  x/FNR}'`
# 	meansize=`cat $resper/size-$burst-$w | awk '{x+=$1} END{print  x/FNR}'`
# 	echo "$burst-$w = overhead $meanoverhead% : Mean duration $meandecoding s : matrix size $size"
# 	echo -n ", $meanoverhead" >> $resper/overhead-matrix
# 	echo -n ", $meandecoding" >> $resper/decoding-matrix
# 	echo -n ", $meansize" >> $resper/size-matrix
#     done;
#     echo "" >> $resper/overhead-matrix
#     echo "" >> $resper/decoding-matrix
#     echo "" >> $resper/size-matrix
# done;
# rm -f $resper/decoding-*.
# rm -f $resper/overhead-*.
# rm -f $resper/size-*.


echo "Draw overhead matrix !"
/home/tai/usr/bin/gnuplot <<EOF
set terminal svg dynamic enhanced size 600,480
set output '$resper/overhead.svg'
set size 0.5,0.5
set autoscale fix
set xtics nomirror rotate by -45
#set title "RTT=300ms; Overhead (in %)"
set xlabel "Encoding window size (in packets)"
set ylabel "Max burst size (in packet)"
set datafile separator comma
set cblabel "Overhead in %"
plot '$resper/overhead-matrix' matrix rowheaders columnheaders using 1:2:3 notitle with image pixels
unset output
EOF
echo "Draw delay graph !"
/home/tai/usr/bin/gnuplot <<EOF
set terminal svg dynamic enhanced size 600,480
set output '$resper/decoding.svg'
set size 0.5,0.5
set autoscale fix
set xtics nomirror rotate by -45
#set title "RTT=300ms; Mean flow duration (in seconds)"
set xlabel "Encoding window size (in packets)"
set ylabel "Max burst size (in packet)"
set cblabel "Mean packet delay"
set datafile separator comma
plot '$resper/decoding-matrix' matrix rowheaders columnheaders using 1:2:3 notitle  with image pixels
unset output
EOF

echo "Draw matrix size graph !"
/home/tai/usr/bin/gnuplot <<EOF
set terminal svg dynamic enhanced size 600,480
set output '$resper/size.svg'
set size 0.5,0.5
set autoscale fix
set xtics nomirror rotate by -45
#set title "RTT=300ms; Mean decoded matrix size (in packets)"
set xlabel "Encoding window size (in packets)"
set ylabel "Max burst size (in packet)"
set cblabel "Mean matrix size"
set datafile separator comma
plot '$resper/size-matrix' matrix rowheaders columnheaders using 1:2:3 notitle  with image pixels
EOF

cp $resper/overhead.svg $paper/overhead3.svg
cp $resper/decoding.svg $paper/decoding3.svg
cp $resper/size.svg $paper/size3.svg


####################
#### SVG TO PDF ####
####################

svg2pdf() {
    output=${1%%.svg}.pdf
    echo "Convert $1 to $output"
    inkscape --without-gui --file=$1 --export-pdf=$output
    pdfcrop $output $output
}

echo "Converting SVG files to PDF"
cd $paper
for file in `find . -name "*.svg"` ; do
    svg2pdf ${file##*/}
done

echo "Running delay stats"
for red in ${temparray[@]} ; do
    tmpwd=$PWD
    cd $prog
    ./waf --run "tetrys-scenario  --MaxNbFlows=1 --numNodes=2 --wireless=0 --LinkRate=\"10Mbps\" -LinkDelay=\"150ms\" --LossModel='Uniform' --ErrorRate=0.15 --RedRatio=$red --filename='./userdata/random1mo.txt' --seed=1" > $tmpwd/run$red 2>&1
    cd $tmpwd
    sendtime=`cat run$red | grep "LOG SEND PACKET" | cut -d: -f1,3 | tr -d ":" | awk '{printf("%0.3f %d\n",$1,$2)}'`
    receivetime=`cat run$red | grep "LOG RECEIVE PACKET" | cut -d: -f1,3 | tr -d ":" | awk '{printf("%0.3f %d\n",$1,$2)}'`
    dectime=`echo -e "$receivetime\n$sendtime" | awk 'BEGIN{sum=0;max=0;var=0}; {if(tab_r[$2] == 0)tab_r[$2]=$1; else tab_s[$2]=$1 ; if(max < $2) max=$2;} END {for(i=1; i<=max;i++) { tab_t[i] = tab_r[i] - tab_s[i] ; sum+=tab_t[i]; }; avg=sum/max;  for(i=1; i<=max;i++){tab_var[i]=(tab_t[i] - avg)^2; var+=tab_var[i];} var=var/max; stddev=sqrt(var); print "max="max "; mean=" avg "; var=" var "; stddev=" stddev; }'`
    time=`cat run$red | grep "Sink" | cut -d: -f2`
    echo "Transfer time for redundancy $red % : $time"
    echo "Mean decoding time for redundancy $red %  : $dectime"
done

echo "End"

