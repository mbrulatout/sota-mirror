#!/bin/bash

# Small script which create files called $prefix$n$suffix
# in current directory. $n is the file size in MB.
# $n varies between $min and $max.
min=1
max=10

prefix=random
suffix=mo.txt
echo "Creating files of $max MB max (1MB step starting from $min)"
for i in `seq $min $max` ; do
	size=$(echo "$i*1024*1024" | bc)
	dd if=/dev/urandom of=$prefix$i$suffix bs=$size count=1
done;
