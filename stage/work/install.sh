#!/bin/bash

path="ns-allinone-3.19/ns-3.19"

#echo "Creating $path/userdata directory"
#mkdir -p $path/userdata
#mkdir -p $path/userdata/hop_number

rm -rf $path/userdata/*
echo "Creating $path/userdata and its subdirectories"
for dir in `find userdata -type d`
do
	mkdir -p  $path/$dir
	#echo $path/$dir
done

for dir in  scratch userdata src 
do
	echo "Linking files from directory $dir to $path/"
	for file in `find $dir`
	do
	    if [[ -f $file && ! `echo $file | grep ".svn" ` ]] ; then
		rm -f $path/$file
			#echo $path/$file
		ln -s `pwd`/$file $path/$file
	    fi
	done
done