export TEXINPUTS="`pwd`/packages:";
rm -f *.aux *.blg *.out *.bbl *.log *.pdf *.out *.toc *.lof *.dvi *~;
pdflatex resume.tex && bibtex resume.aux && pdflatex resume.tex && pdflatex resume.tex
