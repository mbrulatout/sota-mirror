\begin{abstract}

	This paper introduces \REST, a reliable streaming protocol for lossy networks. \REST~ensures full reliability while recovering losses as soon as possible thanks to the proactive injection of redundancy packets encoded following an on-the-fly scheme. It dynamically adapts the sending of codes depending on the estimation of the packet error rate with periodic acknowledgments to limit feedback dependency and protocol overhead. Results show that data are smoothly delivered to the receiving application with minimum overhead when errors are uniform.
For systems with limited processing capacity, 
%For varying PER, the size of decoding matrices, while being reasonably low, increases when the loss rate changes too rapidly. 
we propose to use a bounded encoding window to deliver data more uniformly while limiting decoding matrices size. 
We study the performance of \REST~under different network conditions and highlight the underlying trade-offs behind each system parameter. We show that an optimal acknowledgement frequency can be estimated to minimize overhead while meeting system requirements in terms of delivery delay and computational power.
\end{abstract}


\section{Introduction}
\label{sec:Introduction}

The physical layer of today's wireless communication systems such as LTE/LTE-A and Wi-Fi focuses on lossless connectivity which are achieved by accurate network planning and link budget allocation. This paper considers wireless networks where the channel is still difficult to estimate in a stable and timely manner, leading to lossy network links. Such harsh conditions can be found in public safety and military systems, preventing accurate and stable channel estimation due to long propagation delays, fast mobility and high interference. 

%Because lossy networks suffer from a high number of error bursts, robust and efficient transport layer mechanisms should be provided to applications to meet their data delivery objectives. 
In this context, this paper targets a particular type of streaming applications which require a full reliable service. Such applications cannot tolerate any losses and should consume data in-order as soon as possible. An example of such application in surveillance systems is the streaming of audio or video flows that should be consumed in real-time by operators but also recorded in a lossless manner for later use. Our goal is then to design a reliable streaming protocol which recovers losses as soon as possible. As lossy networks often suffer from scare resources and sometimes long delays, we also seek to limit protocol overhead and processing complexity. 

To ensure a full reliable streaming service, feedback from the receiver to the source are necessary to organize the recovery of losses. However, they introduce high overhead if all packets are acknowledged, like in TCP~\cite{Jacobson88congestionavoidance}, and they can also slow down the error recovery process in case of high round trip times (RTT), as the connection may stall if no feedback is received~\cite{Padhye}.  
%\sout{Traditional transport protocols such as TCP perform poorly with high error rate.}
%TCP, the commonly-used reliable transport protocol, performs poorly with high error rate.
%\sout{A number of coding based reliability mechanisms have recently been proposed for different purposes such as CTCP~\cite{CTCP} for file transfers and Tetrys~\cite{tetrys11tom} for live multimedia streaming.The first one provides the same transport service as TCP. It acknowledges all packets to provide feedback to the sender leading to a high overhead. The second pro-actively injects redundancy to correct transmission errors but does not seek for full reliability. We fully review coding based transport solutions in Sec.~\ref{sec:Related} and motivate our design choices.}
To provide full reliability in lossy networks, there is one coded based reliability mechanism which proposes to integrate the use of an on-the-fly code (RLNC~\cite{medard08arq}) in TCP.
The resulting proposal, named CTCP~\cite{CTCP}, mitigates the retransmission of a full data block by sending redundancy packets.
%CTCP remains constrained by the TCP congestion control, it means that if there is no more data to transmit and if the additional degrees of freedom sent does not allow to recover all missing packets, timeout might occur and TCP retransmission is triggered. Furthermore, CTCP needs to operate towards a proxy buffer that can overflow and greatly slow-down the transmission due to the processing overhead.
In this paper and in the context of reliable streaming over lossy networks, CTCP is not appropriate as it generates too much overhead from acknowledging every single data packet, depends too much on RTT estimation and does not deliver data smoothly to the application as it follows standard TCP behaviour. 
%But in the mean time, the use of an on-the-fly code alone of any kind (RLNC, Tetrys~\cite{tetrys11tom}) at the application layer level is not enough to enable a full reliable service. A transport protocol must ensure that every missing packets are indeed recovered.

%CTCP implements a congestion control algorithm which backoff feature relies too much on a minimal jitter and on a complex and resource-demanding feedback channel with acknowledgement of every single received packet. 

%This paper presents \REST~(Reliable Streaming for Lossy Networks), a reliable streaming protocol which ensures full reliability while repairing losses as soon as possible thanks to the proactive injection of on-the-fly codes.

This paper aims at filling this gap by presenting \REST~(Reliable Streaming for Lossy Networks), a reliable streaming protocol which ensures full reliability while repairing losses as soon as possible thanks to the proactive injection of redundancy based on an on-the-fly coding scheme.
 It dynamically adapts the sending of redundancy codes depending on the estimation of the packet error rate (PER) using an Exponentially Weighted Moving Average (EWMA) with periodic acknowledgments which limits feedback dependency and protocol overhead. REST builds upon the on-the-fly coding scheme of Tetrys~\cite{tetrys11tom} to perform reliable streaming.


For uniform errors, results show that \REST~leads to a smooth data delivery with minimum redundancy overhead. However, for bursty errors, the size of decoding matrices still varies in an unpredicted way. For systems with limited processing capacity, we thus propose to use a bounded encoding window to deliver data more uniformly to the application while limiting decoding matrices size. We evaluate our solution under different network conditions and study the underlying trade-offs behind each system parameter such as the frequency of acknowledgements ($f_{ACK}$) and the encoding window size on the protocol overhead and the data delivery speed to application. We show that an optimal $f_{ACK}$ can be found depending on network conditions and system requirements.

This paper is structured as follows. Sec.~\ref{sec:Related} presents the state of the art on reliability mechanisms and compares on-the-fly coding versus block coding. Sec.~\ref{sec:advanced} details the design of \REST~and Sec.~\ref{sec:results} presents its performance evaluation. Sec.~\ref{sec:conclusion} concludes this paper and discusses future work.

\section{Related Work}
\label{sec:Related}

Two mechanisms have been initially proposed to ensure reliability. A first one is based on retransmission and another one based on redundancy. The former, a reactive scheme, also known as Automatic Repeat reQuest (ARQ), ensures reliability with retransmissions. This implies a feedback from the receiver notifying a lost packet. While this is quite a simple mechanism, it is also RTT-dependant, which can be a problem in long delay and lossy networks. Hence, it is not adapted to streaming applications.
%, where delay can easily exceed an application-layer threshold.

The latter, a proactive scheme known as Application-Layer Forward Error Correction (AL-FEC), introduces redundancy at the sender side. Since it is known that there will be lost packets, the sender can send more data to overcome losses. The choice of which data to send is crucial, and there is a theoretical limit, where $n$ lost packets can be recovered using $n$ redundancy packets. These codes use a block approach, where some redundancy is added to each fixed-size block of data. Decoding is no longer possible when you lose more than the added redundancy. 
%Thus there is a trade-off between the block size and the application-layer throughput desired.
%
%Another approach, Hybrid-ARQ (H-ARQ) uses both approaches to increase trade-off limitations but still requires retransmissions limited by the RTT. Receiver asks for retransmissions if the FEC code can not correct errors.
%
%Kumar et al. \cite{NCmeetsTCPimplem} and more recently Kim et al. \cite{CTCP} proposed a block coding scheme using feedback to lower encoding and decoding complexity. Their contribution uses potentially complex feedback for each sent packet. While achieving full reliability, their congestion control mechanism is RTT-dependent (huge throughput drops in high jitter networks) and also supposes symmetrical data and feedback channels.
%
%Lacan et aL \cite{Tetrys, TetrysAdaptatif} introduced inline redundancy where coded packets are linear combinations of all sent but not acknowledged packets. Both of these solutions do not ensure full reliability but are adapted to real-time video transmission where some losses are acceptables. Using an application-dependant threshold, they drop packets which are too old to be received and deliver a data subset to the application.
%
%Most of erasure codes (also called FEC) used over packet erasure channels are block codes. This means at the encoder side, a set of repair packets is built from a fixed-size set of source data packets and at the decoder side, these repair packets can only be used to recover source packets from their corresponding set. 
%If too many packets (among the source and repair packets) are lost during the transmission, the recovery of the missing source packets is then not possible. 
On the opposite, if only few packets are lost, some of the repair packets become useless. 

A solution to this problem, known as Hybrid FEC-ARQ (or H-ARQ) mechanism, is to use receiver's feedback to send additional repair packets or 
to adjust the redundancy level of the FEC to the observed packet loss rate. However, when using such mechanisms to achieve full reliability, large RTT might lead to very long delays to efficiently 
recover a packet.
% IWCMC Most of erasure codes (also called FEC) used over packet erasure channels are block codes. 

On the contrary, the principle of on-the-fly codes (such as Tetrys~\cite{tetrys11tom} or RLNC~\cite{CTCP, medard08arq}) is to build repair packets from a source packets set (the coding window) which is updated with the receiver's feedback (as seen in Fig.~\ref{fig:simple}). We use a parameter $k$ which is defined as the number of data packets sent between two repair packets. Although initially RLNC was proposed in a non-systematic version while Tetrys was in a systematic one, both codes can be enabled indifferently in both modes. The main difference leads in the way the coding coefficients are computed: RLNC uses a pseudo-random generator while Tetrys uses a deterministic function based on the sequence numbers.
The receiver's feedback update is done in a way that any source packet sent is included in the coding window as long as the sender does not receive its acknowledgement. The method used by the sender to generate a repair packet is simply a random linear combination over a finite field $\mathbb{F}_{q}$ of the data source packets belonging to the coding window. Then, the receiver performs the inverse linear combinations to recover the missing source packets 
from the received data and repair packets. 
In this approach, there is no fixed partitioning of the data stream as would be required with block codes.
%a block-by-block FEC encoding solution.

\begin{figure}[t]
\centering
\includegraphics[width=6cm]{scenario-simple.pdf}
\caption{Example of Tetrys with $k=2$.}
\label{fig:simple}
\end{figure}

%Instead, the FEC encoding window is dynamically updated, according to the feedback information returned by the receiver, hence the "elastic window" or "on-the-fly" term.
%The goal of the feedback flow is only to simplify FEC encoding, by removing source packets that have been either successfully received, or decoded, or even "seen" (as in~\cite{medard08arq}). 
%Since the feedback flow has no impact on the reliability achieved, this proposal is also robust to feedback losses.
%In its most general form, this approach is compatible with any kind of traffic and full reliability can be achieved. In the current work, we focus on bulk data transfer on top of lossy links. The main achievement is the good balance obtained between correction capability and recovering delay compared to a classic block-by-block FEC or simple ARQ schemes (like TCP). 
%A broader presentation of Tetrys is available in~\cite{tetrys11tom}.

In the following, we detail \REST~which borrows this concept of on-the-fly code to proactively inject repair packets.
