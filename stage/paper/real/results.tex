\section{Performance evaluation}
\label{sec:results}

This section presents the performance evaluation of \REST.

\subsection{Simulation setup}
\label{subsec:setup}
We used NS-3 to simulate the transmission of a 1MB file over a lossy network. 
%This network topology allows precise and simple manipulation of the PER. Albeit ran on a signe hop network, the solution was designed to work as well on any kind of topology.
Table \ref{table:parameters} presents the different parameters used. Various packet error rate models are used to simulate different network behaviours. Note that the decoding process is simulated. As an abstraction, we suppose that all repair packets are linearly independent from each other. Hence, when receiving exactly $n$ repair packets covering $n$ losses, decoding is possible. This is almost always true when the coding coefficients are chosen in a sufficiently large field.

\begin{table}[!t]
\renewcommand{\arraystretch}{1.3}
\caption{Simulation parameters.}
\label{table:parameters}
\centering
\footnotesize
\begin{tabular}{|c||c|}
\hline
\bfseries Parameter & \bfseries Value\\
\hline
Link rate & 10Mbps\\
\hline
RTT & 300ms\\
\hline
Payload size & 450bytes\\
\hline
CBR packet interval & 0.008s\\
\hline
ACK interval ($1/f_{ACK})$ & 0.2s\\
\hline
\end{tabular}
\end{table}


\subsection{Reliable version with fixed inline redundancy}
\label{subsec:restetrys1}

The goal of this simulation is to understand the underlying trade-offs in terms of overhead, decoding complexity and delivery time to the application. We define the overhead as the ratio of the sum of useless coded packets and acknowledgements, over the total amount of sent packets.

Fig.~\ref{fig:reliable} shows how \REST~behaves on a 15\% uniform PER link, but with \emph{different static inline redundancy ratios}, respectively 0\%, 15\%, 20\% and 30\%. When the redundancy is smaller or equal than the loss rate as in Fig.~\ref{fig:defaultsub0} and \ref{fig:defaultsub1}, many missing packets are recovered at the end of the transmission, ending in a single and very costly decoding process. However, transferring time is minimum. Fig.~\ref{fig:defaultsub2} shows the case with 20\% redundancy where multiple decoding of various sizes happen during transfer. The application receives a continuous data flow, despite some useless recovery packets received leading to a very small overhead.
% Some recovery packets might be useless from time to time leading to a very small overhead.
However, the application can receive a continuous data flow. Fig.~\ref{fig:defaultsub3} shows that for a high redundancy of 30\%, each lost packet is quickly recovered, through very simple decoding operations. Overhead is huge and application-layer data rate is uniform.

%\textbf{JL: ce serait bien de donner des résultats (chiffres) sur l'overhead et la temps moyen de livraison des paquets à l'application}
%\textbf{Mathias: Pour reliable et adaptive ? Une courbe en fonction du RTT (pour se rapprocher des maps mais sans l'encoding window du coup)}

\begin{figure}
\centering
\subfigure[0\% inline redundancy]{\label{fig:defaultsub0}\includegraphics[width=0.49\columnwidth]{defaultuniform15-0.pdf}}
\subfigure[15\% inline redundancy]{\label{fig:defaultsub1}\includegraphics[width=0.49\columnwidth]{defaultuniform15-15.pdf}}\\
\subfigure[20\% inline redundancy]{\label{fig:defaultsub2}\includegraphics[width=0.49\columnwidth]{defaultuniform15-20.pdf}}
\subfigure[30\% inline redundancy]{\label{fig:defaultsub3}\includegraphics[width=0.49\columnwidth]{defaultuniform15-30.pdf}}
\caption{\label{fig:reliable} Reliable version with a uniform PER of 15\%. Left y-axis : Decoded matrix size (red). Right y-axis : ACK sequence number (green).}
\end{figure}

%While this solution works well with the suited inline redundancy ratio and a uniform PER throughout the flow duration, real networks have varying PER, often in bursts. 

\begin{table}[!t]
\renewcommand{\arraystretch}{1.3}
\caption{Delivery times for the Reliable version.}
\label{table:res}
\centering
\footnotesize
\begin{tabular}{|m{\dimexpr 0.25\columnwidth-2\tabcolsep}|m{\dimexpr 0.25\columnwidth-2\tabcolsep}|m{\dimexpr 0.25\columnwidth-2\tabcolsep}|m{\dimexpr 0.25\columnwidth-2\tabcolsep}|}
\hline
\bfseries Redundancy ratio & \bfseries Streaming \newline duration & \bfseries Packet delay (mean) & \bfseries Packet delay \newline (std dev)\\
\hline
0\% & 21.96s & 2.00s & 4.91s\\
\hline
15\% & 21.96s & 0.89s & 2.12s\\
\hline
20\% & 22.53s & 0.23s & 0.32s\\
\hline
30\% & 25.01s & 0.16s & 0.06s\\
\hline
\end{tabular}
\end{table}

Table~\ref{table:res} shows the total streaming duration for 1MB and the mean packet delay for the four different runs presented in Fig.~\ref{fig:reliable}. The packet delay is the difference between the reception and the emission of a data packet. If a packet is not lost, delay is equal to the link delay. In case of a loss, the packet delay is equal to the sum between the link delay and the time until the packet is decoded at receiver. 
One can see that streaming duration remains the same when the injected redundancy is less or equal than the error rate. Beyond the error rate threshold, streaming duration increases as more and more useless redundancy packets are sent. On the opposite, the packet delay decreases (mean and standard deviation) as the inline redundancy injected grows. Notice that the mean packet delay has a lower bound, which is the link delay ($RTT/2$).

These results clearly show how the reliability and the on-the-fly decoding features work whereas a lack of adaptability from the sender side and a varying application-layer rate are obvious weaknesses to this solution. When it comes to smoothing application-layer rate (minimizing the standard deviation of packet delay), a simple solution would be to always overcome losses with a huge inline redundancy ratio hence generating a huge overhead. These results motivate the adaptive redundancy injection proposed in next section.
% to overcome overhead issues while keeping a standard deviation for packet delay.

\subsection{Proactive injection of adaptive redundancy}
\label{subsec:restetrys2}

Using the redundancy adaptation presented in Sec.~\ref{subsec:tetrys2}, we achieve a uniform application-layer rate with uniform PER and almost no overhead except for a handful of packets (between 0.5 and 1\% of useless sent packets). Fig.~\ref{fig:adaptativeuniform1}\subref{fig:adaptativesinus1} and \subref{fig:adaptativeburst1} present the acknowledged sequence numbers and the size of decoded matrices over time. Fig.~\ref{fig:adaptativeuniform2}\subref{fig:adaptativesinus2} and \subref{fig:adaptativeburst2} present the estimated error rates and the real-time inline redundancy ratio $k$.
An infinite coding window is considered here.

As lossy networks have varying behaviours, we considered more varying packet loss distribution such as a sinusoidal one (although not very representative of any channel, it models simple variations varying from 0\% to 20\% with a 10\% mean error rate) and a more realistic bursty model (a burst of one to 20 packets happens randomly after a loss which has 1\% chance of happening).
Results for these two distributions are presented in Fig.~\ref{fig:adaptative}. For implementation purpose, we assume that an infinite $k$ ratio is equal to 100.

%Fig.~\ref{fig:adaptativesinus1}  and Fig.~\ref{fig:adaptativesinus2} for the sinusoidal model
%through Fig.~\ref{fig:adaptativeburst2}. Fig.~\ref{fig:adaptativesinus2} and Fig.~\ref{fig:adaptativeburst2} represents estimated error rates, inline redundancy ratios and k adaptation for the sinusoidal and bursty error distribution respectively. 
%Fig.~\ref{fig:adaptativesinus1} and Fig.~\ref{fig:adaptativeburst1}; as in Sec.~\ref{subsec:restetrys1}; represent acknowledged sequence numbers and decoded matrices sizes over time. 

One can see that two decoding of the same size occur in the descending sinus phase (one decoding for each sinus wave. See Fig.~\ref{fig:adaptativesinus1} and Fig.~\ref{fig:adaptativesinus2}). The same behaviour can be observed (with different matrix sizes) with the burst error model. When a set of burst increases the measured error rate, decoding is not possible until a return to a lower error value.

Adaptive redundancy shows great improvements in terms of overhead and matrices size reduction. However, in realistic conditions such as bursty PER distribution, matrices size remains potentially large, leading to a non-uniform application-layer rate.
%, application-layer rate is still not uniform enough to apply our solution to streaming applications in more difficult but realistic conditions such as bursty PER distribution. Adaptive redundancy can reduce matrices sizes, but potentially large ones are still decoded.
%they cannot be controlled yet, inducing potentially huge matrices to decoded, which is still not well suited for streaming applications.

% \textbf{JL: la conclusion devrait être sur la difficulte de maitriser les taille de matrice plutot. Mais en disant qu'elles sont deja plus petites.}


\begin{figure}[t]
\centering
\subfigure[]{\label{fig:adaptativeuniform1}\includegraphics[width=0.49\columnwidth]{adaptativeuniform1.pdf}}
\subfigure[]{\label{fig:adaptativeuniform2}\includegraphics[width=0.49\columnwidth]{adaptativeuniform2.pdf}}
\subfigure[]{\label{fig:adaptativesinus1}\includegraphics[width=0.49\columnwidth]{adaptativesinus1.pdf}}
\subfigure[]{\label{fig:adaptativesinus2}\includegraphics[width=0.49\columnwidth]{adaptativesinus2.pdf}}
\subfigure[]{\label{fig:adaptativeburst1}\includegraphics[width=0.49\columnwidth]{adaptativeburst1.pdf}}
\subfigure[]{\label{fig:adaptativeburst2}\includegraphics[width=0.49\columnwidth]{adaptativeburst2.pdf}}
\caption{Adaptive version with a uniform\subref{fig:adaptativeuniform1}\subref{fig:adaptativeuniform2}, sinus\subref{fig:adaptativesinus1}\subref{fig:adaptativesinus2} and bursty\subref{fig:adaptativeburst1}\subref{fig:adaptativeburst2} PER. \subref{fig:adaptativeuniform1}\subref{fig:adaptativesinus1} and \subref{fig:adaptativeburst1} : Decoded matrix size (red) and ACK sequence number (green). \subref{fig:adaptativeuniform2}\subref{fig:adaptativesinus2} and \subref{fig:adaptativeburst2} : Left y-axis (in \%) Estimated error rate (blue), Inline redundancy (red). Right y-axis k value (green).}
\label{fig:adaptative}
\end{figure}


\subsection{Mobile window with adaptive redundancy}
\label{subsec:restetrys3}

As in section \ref{subsec:restetrys2}, error model is based on a burst model (a burst of one to 20 packets happens randomly after a loss which has 1\% chance of happening). Results in Fig.~\ref{fig:mobile} represent simulations using an encoding window maximum size $W = 100$. In theory, the size of decoded matrices  can be up to 100 packets (in case of a huge burst of 100 consecutive losses). In reality, their sizes evolve around $W * p$.

This solution achieves a more uniform rate at the application layer than the adaptive one in more realistic conditions; i.e. bursty networks. There is a slight overhead though, induced by useless repair packets sent. This is due to the fact that, when a block is decoded, feedback takes some time to reach its destination. Meanwhile, the sender still sends repair packets from the same window.
This overhead depends on the encoding window size, the ACK frequency and the RTT (see Section \ref{subsec:ack} for a thorough study).
%\textbf{JL: pas d'accord. Avec la fenetre mobile on uniformiser la livraison des données, c'est vrai. Mais au prix de la vitesse, le transfert est beaucoup plus long. On ne peut pas dire qu'on fait mieux. Ou alors il faut préciser.}
%Mathias : précision faite. C'est mieux au niveau du débit applicatif mais léger overhead induit par des paquets de redodnance inutiles (voir figure plus bas pour des chiffres)
Even with a high RTT and a bursty PER, we provide a uniform application layer rate with minimum overhead when encoding window size is well chosen. Indeed, a small window combined with a high RTT will smooth the application-layer rate, but at a substantial overhead. Many useless repair packets will be sent by the sender over the network, while waiting for an incoming ACK which takes longer than usual to reach its destination, resulting in a significant overhead.

\begin{figure}
\centering
\subfigure[]{\label{fig:mobileadaptativeburst1}\includegraphics[width=0.49\columnwidth]{mobileadaptativeburst1.pdf}}
\subfigure[]{\label{fig:mobileadaptativeburst2}\includegraphics[width=0.49\columnwidth]{mobileadaptativeburst2.pdf}}
\caption{Mobile Adaptive version with a bursty PER. \subref{fig:mobileadaptativeburst1} : Decoded matrix size (red) and ACK sequence number (green). \subref{fig:mobileadaptativeburst2} : Left y-axis (in \%) Estimated error rate (blue), Inline redundancy (red). Right y-axis k value (green).}
\label{fig:mobile}
\end{figure}

Using multiple network conditions, we study the effect of the RTT on the encoding window size.
Fig.~\ref{fig:matrix-overhead} shows that a low RTT and a large encoding window limit the overhead. A threshold can be seen beyond which, a small encoding window combined with a higher RTT induce a large overhead. This is due to the fact that many redundancy packets encoding the same data packet range are sent while the receiver's feedback takes too much time to reach the sender. Fig.~\ref{fig:matrix-decoding} represents the mean packet delay with varying RTT and encoding window size. For a given RTT, say 500ms, it shows a minimum packet delay for sufficiently large encoding windows, $120$ in this case because the maximum window size is never reached. 
 
%, hence an increased overhead.
%% \begin{figure}
%% \centering
%% \includegraphics[width=0.49\columnwidth]{overhead.pdf}
%% \caption{Overhead depending on RTT and encoding window size}
%% \label{fig:matrix-overhead}
%% \end{figure}

\begin{figure}
\centering
\subfigure[Overhead]{\label{fig:matrix-overhead}\includegraphics[width=0.49\columnwidth]{overhead.pdf}}
\subfigure[Mean packet delay]{\label{fig:matrix-decoding}\includegraphics[width=0.49\columnwidth]{decoding.pdf}}
\caption{Overhead\subref{fig:matrix-overhead} and mean packet delay\subref{fig:matrix-decoding} depending on RTT and encoding window size.}
\end{figure}



%% Fig.~\ref{fig:matrix-decoding} presents the impact of RTT and encoding window size over the mean flow duration. There is an increasing delay when the RTT increases
%% However, when the encoding window gets too small, below 150 packets in our simulations, flow duration increases too much, due to an increasing number of useless sent packets.

%% \begin{figure}
%% \centering
%% \includegraphics[width=\columnwidth]{decoding.pdf}
%% \caption{Mean flow duration (in seconds) depending on RTT and encoding window size}
%% \label{fig:matrix-decoding}
%% \end{figure}

%% \begin{figure}
%% \centering
%% \includegraphics[width=\columnwidth]{size.pdf}
%% \caption{Mean decoded matrix size depending on RTT and encoding window size}
%% \label{fig:matrix-size}
%% \end{figure}


\subsection{Feedback and loss dependence}
\label{subsec:ack}

%Although we showed that \REST~can achieve full reliability with great performance and a light-weighted feedback, it is interesting to see the limits of this system.
In this section, we study the impact of the ACK frequency and the error rate on the overhead and the mean packet delay. 
%While $f_{ACK}$ does no impact reliability, we conjectured a trade-off between coding efficiency (with an increased $f_{ACK}$) and a transmission overhead.
In the simulations, we set a moderate RTT of 300ms and use different $f_{ACK}$ from 100ms to 1s. Error model is set to the most representative one, which is the burst model, with a default mean error rate of 10\%.

Fig.~\ref{fig:matrix-overhead2} shows the mean overhead depending on the interval between two ACKs ($1/f_{ACK}$) and the encoding window size. A high $f_{ACK}$ (resp. a low ACK interval) introduces a constant overhead of around 10\%, whatever the encoding window size. This is due to the fact that some ACK do not reduce the encoding buffer size hence make the encoding window progress. The overhead is created at the receiver side in this case. 
In the opposite direction, a low $f_{ACK}$ (resp. a high ACK interval) induces a high overhead at the sender side.
Fig.~\ref{fig:matrix-overhead2} shows that an optimal value for $f_{ACK}$ exists for a given encoding window. This is clear for encoding windows larger than 30 packets. Both Fig.~\ref{fig:matrix-overhead} and Fig.~\ref{fig:matrix-overhead2} show that the optimal value for $f_{ACK}$ depends on network parameters such as the RTT.

Fig.~\ref{fig:matrix-decoding2} represents the mean packet delay with varying ACK intervals and encoding window size. Results show that reducing the ACK frequency while targeting a similar mean packet delay can be achieved by significantly increasing the coding window.


%In our simulation, sending an ACK every three RTT (hence 900ms) seems to be a good compromise.

%with ACK interval lower than 500ms whatever the encoding window size is (maximum size is never reached). Setting a low ACK frequency with a small encoding window increases the mean packet delay way more than usual due to an increasing number of useless packet emissions.

%% \begin{figure}
%% \centering
%% \includegraphics[width=0.49\columnwidth]{overhead2.pdf}
%% \caption{Overhead depending on $f_{ACK}$ and encoding window size with RTT=300ms}
%% \label{fig:matrix-overhead2}
%% \end{figure}

\begin{figure}
\centering
\subfigure[Overhead]{\label{fig:matrix-overhead2}\includegraphics[width=0.49\columnwidth]{overhead2.pdf}}
\subfigure[Mean packet delay]{\label{fig:matrix-decoding2}\includegraphics[width=0.49\columnwidth]{decoding2.pdf}}
\caption{Overhead\subref{fig:matrix-overhead2} and mean packet delay\subref{fig:matrix-decoding2} depending on $f_{ACK}$ and encoding window size with RTT=300ms.}
\end{figure}



%% \begin{figure}
%% \centering
%% \includegraphics[width=\columnwidth]{decoding2.pdf}
%% \caption{Mean flow duration (in seconds) depending on $f_{ACK}$ and encoding window size with RTT=300ms}
%% \label{fig:matrix-decoding2}
%% \end{figure}

%% \begin{figure}
%% \centering
%% \includegraphics[width=\columnwidth]{size2.pdf}
%% \caption{Mean decoded matrix size depending on RTT and encoding window size}
%% \label{fig:matrix-size}
%% \end{figure}



Fig.~\ref{fig:matrix-overhead3} shows the mean overhead depending on the maximum burst size and the encoding window size. Burst occurring probability is still the same but with varying burst size, hence a ascending PER as the max burst size grows. The same trend is shown in this figure as on Fig.~\ref{fig:matrix-overhead} and~\ref{fig:matrix-overhead2}. A lower encoding window size increases the overhead (even more when bursts can get bigger). One can notice though, that an increasing window size reduces the effect of bursts on overhead. In other words, a bigger encoding window can absorb bigger bursts in terms of overhead while increasing packet delay. 
%Depending on the application requirements and the type of networks, the encoding window must be carefully set to avoid mis-dimensioning parameters.

\begin{figure}
\centering
\subfigure[Overhead]{\label{fig:matrix-overhead3}\includegraphics[width=0.49\columnwidth]{overhead3.pdf}}
\subfigure[Mean packet delay]{\label{fig:matrix-decoding3}\includegraphics[width=0.49\columnwidth]{decoding3.pdf}}
\caption{Overhead\subref{fig:matrix-overhead3} and mean packet delay\subref{fig:matrix-decoding3} depending on max burst size and encoding window size with RTT=300ms.}
\end{figure}

%\textbf{paragraph \ref{fig:matrix-decoding3} and \ref{fig:matrix-decoding}}

Fig.~\ref{fig:matrix-decoding3} represents the mean packet delay with varying maximum burst size and encoding window size. It shows a minimum gain with a small burst size (30 packets or less) whatever the encoding window size is because below these two points, maximum encoding window size is never reached. Setting a high burst size with a small encoding window increases the mean packet delay way too much since the encoding window is full most of the time. 

To conclude on these results, there are two options depending on the system's needs. One can set a maximum delivery delay depending on the streaming application needs and figure out the minimum encoding window size (hence the minimal computational power to comply with those restrictions) and the optimal ACK frequency. One can also set a maximum window size (when computational power is limited) and seek the optimal ACK frequency to minimize the average delay to recover packets. The optimal value will depend on network conditions (RTT, losses).

%% \begin{figure}
%% \centering
%% \includegraphics[width=\columnwidth]{decoding3.pdf}
%% \caption{Mean flow duration (in seconds) depending on max burst size and encoding window size with RTT=300ms}
%% \label{fig:matrix-decoding3}
%% \end{figure}
