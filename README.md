Guide
=======

###Télécharger NS-3 et Kodo 

Créer un dossier et y accéder

    mkdir folder
    cd folder

Cette ligne télécharge la version de développement de NS-3
    
    git clone git://github.com/nsnam/ns-3-dev-git

Ces lignes permettent de télécharger Kodo et les bibliothèques dont Kodo est dépendant

    git clone git://github.com/steinwurf/fifi.git
    git clone git://github.com/steinwurf/sak.git
    git clone git://github.com/steinwurf/external-boost-light.git
    git clone git://github.com/steinwurf/kodo.git

* **fifi :** Bibliothèque d'arithmétique des corps finis.
* **sak :** Bibliothèque d'utilitaires C++ dont la gestion de l'endianness utilisée par Kodo
* **external-boost-light :** Bibliothèque contenant de nombreux utilitaires dont les smart pointers utilisés par Kodo

###Référencer Kodo comme une bibliothèque externe de NS-3

    cd ns-3-dev-git/src

Rajouter ce morceau de code à la fonction build du fichier wscript

~~~python
 bld.env['INCLUDES'] += ['path_to_folder/kodo/src',
                            'path_to_folder/external-boost-light/',
                            'path_to_folder/fifi/src',
                            'pâth_to_folder/sak/src' ]
~~~

La fonction build doit avoir cette forme à la suite de cette opération

~~~python
def build(bld):
    bld.create_ns3_module = types.MethodType(create_ns3_module, bld)
    bld.create_ns3_module_test_library = types.MethodType(create_ns3_module_test_library, bld)
    bld.create_obj = types.MethodType(create_obj, bld)
    bld.ns3_python_bindings = types.MethodType(ns3_python_bindings, bld)
    bld.env['INCLUDES'] += ['/path_to_folder/kodo/src',
                            '/path_to_folder/external-boost-light/', 
                            '/path_to_folder/fifi/src', 
                            '/path_to_folder/sak/src' ]
    # Remove these modules from the list of all modules.
    for not_built in bld.env['MODULES_NOT_BUILT']:

        # XXX Becaue these modules are located in subdirectories of
        # test, their names in the all_modules list include the extra
        # relative path "test/".  If these modules are moved into the
        # src directory, then this if block should be removed.
        if not_built == 'ns3tcp' or not_built == 'ns3wifi':
            not_built = 'test/' + not_built

        if not_built in all_modules:
            all_modules.remove(not_built)

    bld.recurse(list(all_modules))

    for module in all_modules:
        modheader = bld(features='ns3moduleheader')
        modheader.module = module.split('/')[-1]
~~~

###Configuration et compilation

    cd ../
    CXXFLAGS="-std=c++0x" ./waf --build-profile=debug --enable-examples configure

ou pour aller plus vite (ne pas compiler les tests, les exemples, ni le binding python.
     CXXFLAGS="-std=c++0x" ./waf --build-profile=debug --disable-examples --disable-tests --disable-python --enable-modules=applications,config-store,core,flow-monitor,internet,mobility,network,nix-vector-routing,olsr,point-to-point,wifi configure

	
puis pour compiler
    ./waf

Il est possible de lancer un exemple très simple via la commande `./waf --run first`
Il est maintenant possible d'inclure n'importe quel header de Kodo via un simple include.

